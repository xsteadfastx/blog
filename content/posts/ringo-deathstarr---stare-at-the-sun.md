Title: Ringo Deathstarr - Stare At The Sun
Date: 2015-10-28 10:23
Slug: ringo-deathstarr---stare-at-the-sun
Tags: music, ringodeathstarr

Ich freue mich ja auf das neue Ringo Deathstarr Album. Auch wenn die Band sich immer von My Bloody Valentine distanziert, man hört es doch ziemlich raus. Und das meine ich überhaupt nicht Negativ... im Gegenteil.

{% soundcloud https://soundcloud.com/reverberationappreciation/03-stare-at-the-sun-1 %}
