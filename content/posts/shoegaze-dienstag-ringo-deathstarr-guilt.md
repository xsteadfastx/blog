Title: Shoegaze Dienstag: Ringo Deathstarr - Guilt
Date: 2015-09-22 11:31
Slug: shoegaze-dienstag-ringo-deathstarr-guilt
Tags: shoegaze, music, ringodeathstarr
Description: Neuer Track von Ringo Deathstarr

[Ringo Deathstarr](http://www.ringodeathstarr.org/) hatte ich hier schon öfters. Sie wollen nicht mit My Bloody Valentine verglichen werden aber wenn man sie hört weiß man einfach bescheid. Dies ist ja auch garnicht schlimm. Ich mag es.

{% soundcloud https://soundcloud.com/club-ac30/ringo-deathstarr-guilt/s-Wh9ed %}
