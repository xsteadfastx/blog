Title: Gestern waren Christine und ich bei einem Konzert...
Date: 2012-06-04 10:56
Author: marvin
Category: Uncategorized
Tags: concerts, leindwand, music, nuernberg
Slug: gestern-waren-christine-und-ich-bei-einem-konzert

![Leinwand]({static}/images/7334502654_ef324ea377_b.jpg)

Gestern waren Christine und ich bei einem Konzert von der Band Leinwand.
Wollte eigentlich ein paar Fotos machen...Motivation und so...aber
irgendwie bin ich nicht ganz zufrieden. Vor allem die schwachen
Lichtverhältnisse haben mir zu schaffen gemacht. Und alles wegblitzen
wollte ich auch nicht. Also überbelichtet bis zur Versenkung. Im
Endeffekt hat hat es fast jedes Bild zerrauscht...naja...

![Leinwand]({static}/images/7334514892_9f5d4431fb_b.jpg)

![Leinwand]({static}/images/7334525272_2e2a86cb88_b.jpg)

![Leinwand]({static}/images/7334544814_a51c9fbf41_b.jpg)

![Leinwand]({static}/images/7334564750_7d8dc26a06_b.jpg)

![Leinwand]({static}/images/7334584486_c1a6753bbc_b.jpg)

![Leinwand]({static}/images/7334591832_64f99cc9d8_b.jpg)

![Leinwand]({static}/images/7334596826_549a2189c9_b.jpg)

![Leinwand]({static}/images/7334607972_043a57d633_b.jpg)

![Leinwand]({static}/images/7334618614_587a2fd975_b.jpg)

![Leinwand]({static}/images/7334611614_4649195fbd_b.jpg)

![Leinwand]({static}/images/7334625476_5139c6a22c_b.jpg)

![Leinwand]({static}/images/7334632558_ecc47c070f_b.jpg)

![Leinwand]({static}/images/7334492656_496aeb73f6_b.jpg)

