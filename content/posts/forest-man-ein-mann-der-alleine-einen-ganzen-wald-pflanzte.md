Title: Forest Man: Ein Mann der alleine einen ganzen Wald pflanzte
Date: 2014-07-18 10:47
Author: marvin
Category: Uncategorized
Tags: documentary, jadavpayeng, nature
Slug: forest-man-ein-mann-der-alleine-einen-ganzen-wald-pflanzte

In Indien hat [Jadav Payeng](http://en.wikipedia.org/wiki/Jadav_Payeng)
auf eigene Faust einen Wald auf einer Insel angelegt. Und das seit 1979.
Nun ist der Wald größer als der Central Park und die Heimat vieler
Tiere. Und er will bis zu seinem Tode weitermachen. Ich bin begeistert.

{% youtube HkZDSqyE1do   %}

([via](http://laughingsquid.com/forest-man-a-short-documentary-about-an-indian-man-who-planted-a-forest-larger-than-central-park/))

