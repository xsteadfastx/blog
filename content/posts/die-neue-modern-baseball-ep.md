Title: Die neue Modern Baseball EP
Date: 2015-10-27 09:07
Slug: die-neue-modern-baseball-ep
Tags: music, modernbaseball

![Cover]({static}/images/modern_baseball_ep.jpg)

Und ich dachte ich müsste etwas länger warten auf ein neues Release von Modern Baseball. Wie aus dem Nichts kommt da diese EP die mich wohl durch diesen Herbst boxen wird. Erst vor kurzer Zeit mussten sie die Australien Tour wegen Depressionen des Sängers absagen. Nun diese EP und vielleicht irgenwann mal eine Europa Tour. Ich bin Fan.

<iframe style="border: 0; width: 350px; height: 654px;" src="https://bandcamp.com/EmbeddedPlayer/album=1758370554/size=large/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="http://lame-orecords.bandcamp.com/album/mobo-presents-the-perfect-cast-ep-featuring-modern-baseball">MoBo Presents: The Perfect Cast EP Featuring Modern Baseball by Lame-O Records</a></iframe>
