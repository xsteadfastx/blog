Title: Slow Magic – Corvette Cassette
Date: 2012-09-22 17:54
Author: marvin
Category: Uncategorized
Tags: music, slowmagic
Slug: slow-magic-corvette-cassette-2

![slowmagicsurf]({static}/images/slowmagicsurf.jpg)

Ein Quell aktuellem Herbst-Blues ist "Slow Magic" mit diesem, abgefüllt
mit Sommer, Video zu dem Track "Corvette Cassette". Mehr kann und will
ich garnicht dazu sagen...

{% vimeo 27304056 %}

