Title: Scenekid-Robot
Date: 2012-09-11 10:59
Author: marvin
Category: Uncategorized
Tags: curiosity, mars, nasa, photography
Slug: scenekid-robot

![685361main_pia16149-full_full]({static}/images/685361main_pia16149-full_full.jpg)

Was sagen eigentlich die Robotergesetze Asimovs zur Selbstverliebtheit
von intelligenten Maschinen? Ich kann nicht beschreiben wieso, aber mein
Herz erfüllt sich mit Freude wenn ich dieses Selbstportrait des
Marsrovers Curiosity sehen.

