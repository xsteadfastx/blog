Title: Ein Blick auf die Welt
Date: 2012-07-25 08:47
Author: marvin
Category: Uncategorized
Tags: iss, nasa, space, timelapse
Slug: ein-blick-auf-die-welt

![531997]({static}/images/531997.jpg)

Was für ein Ausblick man auf der ISS haben muss. Ich weiß gar nicht ob
man da oben die Ruhe weg hat um sich auf den Ausblick einzulassen. Ganz
egal... Hauptsache jemand hat mal die Kamera rausgehalten...

{% vimeo 45878034 %}

