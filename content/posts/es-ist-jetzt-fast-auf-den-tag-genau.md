Title: Es ist jetzt fast auf den Tag genau...
Date: 2012-06-18 13:34
Author: marvin
Category: Uncategorized
Tags: germanygermany, music, nuernberg
Slug: es-ist-jetzt-fast-auf-den-tag-genau

![3663822428-1]({static}/images/3663822428-1.jpg)

Es ist jetzt, fast auf den Tag genau, ein Jahr her das ich hier in
Nürnberg wohne. Ich kann es eigentlich immer noch gar nicht fassen was
letztes Jahr alles so passiert ist. Mein komplettes Leben wurde
umgekrempelt. Und das auf die schönste Weise die man sich vorstellen
kann. Mein erster Sommer hier mit meiner Frau...im warmen Nürnberg...
Als ich heute das Haus verließ hatte ich genau wieder diesen Duft in der
Nase...Sommer.

Ein Album verbinde ich besonders mit dieser Zeit. Es ist das Album
"Adventures" von Germany Germany. Wir haben ihn eines Abends Live
gesehen...und es war einfach alles perfekt. Wir haben getanzt...die
Stimmung...der Ort...die Musik. Deswegen ist das, das Album des Tages...

{% youtube M08XptP2oa8 %}

<iframe width="300" height="410" style="position: relative; display: block; width: 300px; height: 410px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=2753982818/size=grande3/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[Adventures
by Germany
Germany](http://grmnygrmny.bandcamp.com/album/adventures)</iframe>

