Title: Petal - Heaven
Date: 2015-10-20 09:40
Slug: petal-heaven
Tags: petal, music

{% youtube JHlx7aAKSRY %}

Willkommen zu dem Ohrwurm meines gesammten Wochenendes. Und mal wieder bin ich ganz aus dem Häuschen was es für gute neue Bands und Musiker gibt. Fern von diesem Hipster-Indie Ding. Petals Album "Shame" kommt die Tage raus und ich hoffe das ich es irgendwo hier in Deutschland abgreifen kann. Die Songs haben genau den richtigen Grad an Melancholie für diese Zeit.

Ach ja... Youtube und Doitschland. Es nervt nur noch. Also werft euern Youtube Unblocker an oder nutzt den ganz wunderbaren [tor-browser](https://www.torproject.org/projects/torbrowser.html.en) um sich das Video anzuschauen.

Hier noch einmal in unplugged und auch in Deutschland anschaubar.

{% youtube IKPSy26v29s %}

Oder auch den Track "Sooner" auf Soundcloud.

{% soundcloud https://soundcloud.com/runforcoverrecords/06-petal-1/s-9z0hF %}
