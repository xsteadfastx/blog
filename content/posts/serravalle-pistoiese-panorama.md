Title: Serravalle Pistoiese Panorama
Date: 2012-09-03 12:41
Author: marvin
Category: Uncategorized
Tags: nikond60, panorama, photography
Slug: serravalle-pistoiese-panorama

![Serravalle Pistoiese]({static}/images/7920500522_3f6598f37c_b.jpg)

Aus meiner Reihe: Die paar Urlaubsfotos die ich gemacht habe. Heute ein
Panorama mit der Aussicht auf das kleine Örtchen Serravalle Pistoiese in
dem wir unseren Urlaub verbracht haben. Das ist die direkte Aussicht von
unserem Ferienhaus in dem wir eine Woche gewohnt habe. Da bekomme ich
gleich wieder Fernweh...
