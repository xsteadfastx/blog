Title: Dockercraft - Minecraft Docker client
Date: 2015-11-18 09:37
Slug: dockercraft-minecraft-docker-client
Tags: docker, minecraft

[![Dockercraft]({static}/images/dockercraft.gif)](https://github.com/docker/dockercraft)

Endlich kann man dem Chef einfach sagen das man sich nur um die Administration der [Docker-Container](https://de.wikipedia.org/wiki/Docker_%28Software%29) kümmert. Darauf habe ich gewartet... Docker Container aus Minecraft heraus steuern. Zweimal heißen Scheiß kombinieren und voilá: [Dockercraft](https://github.com/docker/dockercraft). Man fühlt sich ein wenig in einen Hackerfilm der 90er versetzt. Und alleine deswegen kann man das schon feiern. Erinnert sehr start an den [Doom-Prozess-Killer](https://www.cs.unm.edu/~dlchao/flake/doom/).

{% youtube eZDlJgJf55o %}

([via](https://news.ycombinator.com/item?id=10584956))
