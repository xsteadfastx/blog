Title: Die Technik mit der das klassische Doctor Who Intro gebaut wurde
Date: 2013-11-21 12:55
Author: marvin
Category: Uncategorized
Tags: doctorwho
Slug: die-technik-mit-der-das-klassische-doctor-who-intro-gebaut-wurde

![doctorwho_intro]({static}/images/doctorwho_intro.jpg)

Zwei Tage vor dem großen Jubiläum und der Strom an Doctor-Who-Content
reißt einfach nicht ab. In diesem Video wird erklärt mit welcher Technik
damals das klassische Intro gebaut wurde. Gänsehaut und so.

{% youtube kOB5W_4kZTI %}

{% youtube sjDFvoRNpOM %}

