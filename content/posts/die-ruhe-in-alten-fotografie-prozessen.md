Title: Die Ruhe in alten Fotografie-Prozessen
Date: 2012-11-15 11:08
Author: marvin
Category: Uncategorized
Tags: mattmorris, photography
Slug: die-ruhe-in-alten-fotografie-prozessen

![tintypeprocess]({static}/images/tintypeprocess.jpg)

Was eine wunderbare Ruhe in dem Prozess des Fotografierens liegt. Ich
rede nicht von digitalem, schnellem Knipsen. Sich Zeit zunehmen für die
Materialien, das Motiv, den Prozess. [Matt
Morris](http://www.mattmorrisfilms.com/) hat eine kurze Dokumentation
über einen Fotografen, mit dem Namen Harry Taylor, gemacht. Großartig...

{% vimeo 53077087 %}

([via](http://www.petapixel.com/2012/11/13/american-tintype-a-portrait-of-an-tintype-portrait-photographer/))

