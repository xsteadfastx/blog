Title: XKCD und John Cage
Date: 2013-04-15 10:10
Author: marvin
Category: Uncategorized
Tags: art, johncage, xkcd
Slug: xkcd-und-john-cage


![cc-by-nc [XKCD](http://xkcd.com/1199/)]({static}/images/silence.png)

XKCD widmetseinen heutigen Strip einer meiner liebsten Kunstarbeiten.
[4′33″](https://de.wikipedia.org/wiki/4%E2%80%B233%E2%80%B3) von John
Cage. Einer der Startschüsse des Umdenken über Musik. Und wie kann Musik
mit Diensten wie Shazam definiert werden? Alles was eine ein Tonfolge
hat? Oder schränkt uns das doch zu sehr ein. Wunderbar wie diese
Beispiel in das heutige Internetz-Zeitalter geholt wurde. Ich habe mich
gefreut.

