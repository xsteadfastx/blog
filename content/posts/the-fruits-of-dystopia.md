Title: The Fruits Of Dystopia
Date: 2012-08-21 09:57
Author: marvin
Category: Uncategorized
Tags: 1984, abravenewworld, fahrenheit451, georgeorwell, politics, surfing
Slug: the-fruits-of-dystopia

![639910]({static}/images/639910.jpg)

Dieser Kurzfilm soll den Spaß zeigen in einer nicht perfekten Welt...
Neben den tollen Aufnahmen ist der Film unterlegt mit Zitaten aus
Büchern wie "1984", "A Brave New World" oder "Fahrenheit 451". Schönes
Ding...

{% vimeo 46762315   %}
([via](http://www.korduroy.tv/))

