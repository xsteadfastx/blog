Title: Dokumentation über den Drum Doctor Ross Garfield
Date: 2016-03-10 14:21
Slug: dokumentation-ueber-den-drum-doctor-ross-garfield
Tags: drums, documentary, rossgarfield

Ross Garfield ist für sehr viele ikonische Drumsounds verantwortlich. Neben seiner [unfassbaren Sammlung](http://www.drumdoctors.com/rental.htm), ist seine Art zu stimmen berühmt und einzigartig. Wie sehr ich oft verzweifel am stimmen meines Drumsets. Man stelle sich vor seinen Beruf zu haben. Unfassbar.

> From Frank Sinatra to Johnny Cash, Michael Jackson to Metallica, master drum craftsman and technician, Ross Garfield, is responsible for some of the world’s most iconic drum sounds. This is the story of the man behind the drums.

{% vimeo 156509577 %}

([via](https://www.reddit.com/r/drums/comments/49ojy5/the_drum_doctor/))
