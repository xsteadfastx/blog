Title: Daheim
Date: 2012-09-18 17:32
Author: marvin
Category: Uncategorized
Tags: instagram, photography, wolfsburg
Slug: daheim

![Besuch Daheim]({static}/images/7995708630_39c8b4d043_b.jpg)

Wir hatten vor Montag nach Wolfsburg zu fahren. Das letzte mal war ich
kurz vor dem Sommer in der Stadt in der ich aufgewachsen bin. Dort
befindet sich neben meiner Familie, meinem offiziellem Arbeitsplatz auch
meine komplette Vergangenheit. In jeder Straße, jedem Baum und jeder
Ampel. In meinen Gedanken sind die verdrängten Verbindungen die man,
glaube ich, "Daheim" nennt. Ein Spaziergang durch dieses "Daheim".

![Besuch Daheim]({static}/images/7995708202_98b0024549_b.jpg)

![Besuch Daheim]({static}/images/7995707870_9eaa2c474a_b.jpg)

![Besuch Daheim]({static}/images/7999206545_3f0b8fe872_b.jpg)

