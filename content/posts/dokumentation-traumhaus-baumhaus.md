Title: Dokumentation: Traumhaus Baumhaus
Date: 2016-04-18 10:27
Slug: dokumentation-traumhaus-baumhaus
Tags: arte, documentary

Auf Arte ist immer verlass. Sonntag hatte ich lust auf eine Dokumentation und scrollte durch die Arte App nach etwas passendem. Beim Titel [Traumhaus Baumhaus](http://www.arte.tv/guide/de/058360-000-A/traumhaus-baumhaus) blieb ich natürlich hängen. Baumhäuse sind hier immer Ontopic. Was fehlt ist [The Conder Cone](http://thecindercone.com/).

> In der Auvergne im Wald von La Stèle haben Thomas und Mathieu ihren Traum wahr werden lassen: Sie haben ein ganzes Baumhaus-Dorf angelegt. Die Baumhäuser sind so konzipiert, dass die Bewohner von ihrer Unterkunft in bis zu 13 Metern Höhe keine Sicht auf die anderen Häuser haben. So können sie sich ganz allein im Tannenwald fühlen.
> -- <cite>[Arte](http://www.arte.tv/guide/de/058360-000-A/traumhaus-baumhaus)</cite>

<iframe src="http://www.arte.tv/guide/de/embed/058360-000-A/medium" allowfullscreen="true" style="width: 600px; height: 344px;" frameborder="0"></iframe>
