Title: Brothertiger - Wake
Date: 2015-09-30 08:59
Slug: brothertiger-wake
Tags: brothertiger, music
Description: Neuer Brothertiger Track

Es gibt kaum einen Blogpost den ich öfters aufsuche als den mit einem [Radio Liveset von Brothertiger]({static}/posts/brothertiger-liveset.md). Alleine der erste Track haut mich immer wieder um. Nun kommt bald sein neues Album, welches man [hier](https://brothertiger.bandcamp.com/album/out-of-touch-lp) vorbestellen kann. Hier nun der erste Track aus dem Album. Yiha.

{% soundcloud https://soundcloud.com/brothertiger/wake/s-Q1a4F %}

([via](http://www.stereogum.com/1833556/brothertiger-wake-stereogum-premiere/mp3s/))
