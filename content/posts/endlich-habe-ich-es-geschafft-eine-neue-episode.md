Title: Endlich habe ich es geschafft eine neue Episode...
Date: 2012-02-17 11:56
Author: marvin
Category: status
Tags: bbfc, football, listedercoolenleute, podcast, politics
Slug: endlich-habe-ich-es-geschafft-eine-neue-episode

Endlich habe ich es geschafft eine neue Episode für die "Liste der
coolen Leute" hochzuladen. Eigentlich sollte das ja ein regelmäßiger
Podcast werden...klappt natürlich nicht. Vor allem wegen der Entfernung.
Aber ab und zu können wir sogar Skype weglassen und uns treffen für eine
kurze Aufnahme. Ich war krank und der gute Michael hatte seine Stimme
durch eine Sportveranstaltung verloren...trotzdem gute Zeit gewesen :-)

[Hier](http://listedercoolenleute.de/?p=79) klicken zum hören...

