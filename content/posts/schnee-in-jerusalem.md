Title: Schnee in Jerusalem
Date: 2013-12-18 11:55
Author: marvin
Category: Uncategorized
Tags: fernweh, israel, jerusalem
Slug: schnee-in-jerusalem

Die Abstände zu meiner letzten Israelreise werden immer länger. Gerade
in dieser Jahreszeit war ich oft im heiligen Land. Ich hatte in der Zeit
Wetter jeglicher Art mitgenommen. Von Sonnenbrand bis
Winterjackenpflicht. Schnee ist dann doch was besonders welches nur alle
paar Jahre vorkommt. Gerade ist so eine Zeit in Jerusalem. Schnee kommt
vom Himmel und bedeckt diese ganz besondere Stadt.

Ich merke wie hoch mein Fernweh steigt. Es gab sogar einen Notfall-Plan
ganz kurzfristig einen Flug zu buchen um dort den Winterurlaub zu
verbringen. Diese Spontanität wird mit sehr hohen Flugpreisen bestraft.
Und so wurde dann doch nichts daraus. So versinke ich im Fernweh und
schaue dieses Video über den Schnee auf den Dächern und Straßen von
Jerusalem.

{% youtube ovZ2DGqdzyI %}

