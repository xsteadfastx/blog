Title: Die konsequente Entwicklung des Urheberrechts unter digitalen Produktions...
Date: 2012-05-07 09:22
Author: marvin
Category: Uncategorized
Tags: art, berlin, copyright, piraten
Slug: die-konsequente-entwicklung-des-urheberrechts-unter-digitalen-produktions

![0250_9d32_480]({static}/images/0250_9d32_480.jpeg)

> Die konsequente Entwicklung des Urheberrechts unter digitalen
> Produktions- und Verbreitungsbedingungen wird vom zuständigen
> Bundesministerium der Justiz in unvertretbarer Weise verzögert. Dies
> führt zu zunehmender Rechtsunsicherheit in Bezug auf den Umgang mit
> geschützten Werken. In dieser Situation werden die Urheber nicht
> zögern, das Verfassungsgericht aufzufordern, Klarheit darüber
> schaffen, dass der Staat verpflichtet ist, mit zeitgemäßen Gesetzen
> das Recht auf Schutz des geistigen Eigentums seiner Bürger
> durchzusetzen. Die Akademie der Künste wird sie dabei unterstützen.

- [AdK-Blog](http://www.adk.de/de/blog/index.htm?we_objectID=30927)

Was soll man nur zu einer Kunsteinrichtung wie die Akademie der Künste
in Berlin sagen die versuchen wollen das Urheberrecht mit einem
Menschenrecht gleichsetzen? Ich kann es mir nicht anderes erklären als
das irgendwelche Leute aus der der oberen Verwaltungsetage zu solchen
Aussagen kommen. Menschen die noch nie kreativ gearbeitet haben. Wo wird
in den Künsten 100% aus sich selber erschaffen? Ist Kunst nicht ein
Reaktor aus Zitaten?

Also liebe Akademie der Künste...ich hoffe wirklich das diese Aussagen
von nicht von den Künstlern ihrer Akademie stammen...sondern nur von den
Anzugträgern und Kreativbremsen.

Danke für das bisschen Kotze die mir diesen Morgen wegen ihnen
hochgekommen ist...

