Title: There is nothing to writing All you do...
Date: 2012-03-06 10:34
Author: marvin
Category: quote
Slug: there-is-nothing-to-writing-all-you-do

There is nothing to writing. All you do is sit down at a typewriter and
bleed.

<cite>Ernest Hemingway</cite>

