Title: small black - despicable dogs
Date: 2011-12-08 12:03
Author: marvin
Category: post
Tags: music, smallblack
Slug: small-black-despicable-dogs

Will man nicht eigentlich so leben wir die Hauptperson in diesem
großartigen Musikvideo? Ich schreibe es zwar so oft aber ich sehne mich
so sehr nach dem Frühling...nach Sonne und dem Badesee. Vielleicht liegt
es auch daran das das letzte Jahr das schönste meines Lebens war. Dieser
Song erinnert mich an dieses ganz bestimmte Gefühl was ich damit
verbinde. Vielleicht gebe ich auch zuviel auf
Song-Gefühl-Verbindungen...ich bin aber glücklich damit.

{% youtube Zq_KgUT8gLc %}

