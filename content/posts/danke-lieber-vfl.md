Title: Danke lieber VfL
Date: 2015-12-15 10:36
Slug: danke-lieber-vfl
Tags: vflwolfsburg, wolfsburg, football

![Volkswagen Arena]({static}/images/23697352576_cd49ef6a78_b.jpg)

Was ein Jahr. Ok am Samstag wäre ein Sieg noch besser gewesen... geschenkt. Ein Berg und Talfahrt der Gefühle. Ich erinnere mich an Berlin und den Pokalsieg und auch die Nächte in der Sommerpause in der mich der bevorstehende Wechsel von Kevin De Bruyne wach hielt. Danke.
