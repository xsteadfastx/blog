Title: Isa Leshko fotografiert alte tiere
Date: 2011-12-16 15:41
Author: marvin
Category: post
Tags: animals, photography
Slug: isa-leshko-fotografiert-alte-tiere

Ein Video über Isa Leshko die ganz wunderbare Fotos von alten Tieren
macht die auf "sanctuarie farms" leben. Verrückt das man alte Tiere so
selten sieht...ok gerade die gezeigten Tiere werden sehr früh
geschlachtet oder gemordet. So hat man ihr Älterwerden nicht vor Augen.
Was ein schönes Projekt.

{% vimeo 29632448 %}

