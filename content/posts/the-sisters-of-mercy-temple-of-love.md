Title: The Sisters of Mercy - Temple of Love
Date: 2012-11-21 12:09
Author: marvin
Category: Uncategorized
Tags: mtv, music, thesistersofmercy
Slug: the-sisters-of-mercy-temple-of-love

![templeoflove]({static}/images/templeoflove.jpg)

1992! Wahnsinn! "Temple of Love" war eins der ersten Musikvideos an die
ich mich erinnere. Zu dem Zeitpunkt war ich 8 Jahre alt. Ich fing an
nach der Schule MTV zu konsumieren. Stundenlang. Zum Glück gibt es
Großeltern die ihren Enkeln den Fernseher bedingungslos zur Verfügung
stellen. Ich weiß noch genau, dass ich mit diesem Sound nicht viel
Anfangen konnte. Doch der Beat und die schnell wechselnden Schnitte
beeindruckten mich. Irgendwie genau das was MTV damals für mich
ausmachte. Ein paar Jahre später entdeckte ich die Sisters wieder und
lege sie regelmäßig wieder auf. Und ich liebe es.

{% youtube ROnXv7Z7v28 %}

