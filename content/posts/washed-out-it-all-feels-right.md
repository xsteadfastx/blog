Title: Washed Out - It All Feels Right
Date: 2013-06-12 08:21
Author: marvin
Category: Uncategorized
Tags: music, washedout
Slug: washed-out-it-all-feels-right

![washedout_itallfeelsright]({static}/images/washedout_itallfeelsright.jpg)

Endlich hört man mal was vom neuen Washed Out Album. Der Teaser war
super und jetzt wurde der komplette Track "It All Feels Right"
veröffentlicht.

{% youtube qJILRWBKnQ4 %}

