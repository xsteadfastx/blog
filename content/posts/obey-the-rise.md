Title: obey - the rise
Date: 2011-08-29 08:58
Author: marvin
Category: post
Tags: art, banksy, obey, shepardfairey
Slug: obey-the-rise

[Shepard Fairey](http://de.wikipedia.org/wiki/Shepard_Fairey) ist ja
neben [Banksy](http://de.wikipedia.org/wiki/Banksy) der Künstler der
einem sofort einfällt...denkt man doch an Streetart. Das Obama HOPE
Poster oder seine Klamottenlinie...mehr braucht es nicht zu sagen. Er
ist mitten im Mainstream angekommen. In letzter Zeit fallen mir immer
mehr Leute hier in Nürnberg auf die seine T-Shirts tragen.

Was ihn neben seiner Kunst so interessant macht sind die Szenen aus
denen er seine Inspiration zieht...aber darüber erzählt er in diesem
Video mehr.

{% vimeo 28161343 %}

