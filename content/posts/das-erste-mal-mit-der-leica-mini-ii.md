Title: Das erste mal mit der Leica Mini II
Date: 2014-01-17 12:17
Author: marvin
Category: Uncategorized
Tags: leicamini2, photography
Slug: das-erste-mal-mit-der-leica-mini-ii

![Christine II]({static}/images/11993402594_6775bd65a5_b.jpg)

Ich hatte ja geschrieben das ich meine Olympus Mju-II (wegen Lightleaks)
gegen eine Leica Mini II getauscht habe. Nach der Trauer diese
wunderbare Kamera zurückzugeben, musste ich wieder einen Film voll
schießen um sie zu testen. Vor allem musste wieder mal Christine dafür
herhalten. Ich habe auch meinen Entwicklungsprozess ein wenig [angepasst](https://github.com/xsteadfastx/filmentwicklung/blob/master/Rodinal.md).

![Aufseßplatz]({static}/images/11993836316_e9b00fc88e_b.jpg)

![Christine I]({static}/images/11993034545_a4d785f898_b.jpg)

![Christine III]({static}/images/11993848316_d2682b02e7_b.jpg)
