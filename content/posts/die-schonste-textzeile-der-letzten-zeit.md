Title: Die schönste Textzeile der letzten Zeit
Date: 2012-10-22 14:48
Author: marvin
Category: Uncategorized
Tags: kraftklub, liebe, music
Slug: die-schonste-textzeile-der-letzten-zeit

Es gibt Textzeilen die sind so romantisch das vielleicht einigen
Personen der Sinn darin sich nicht erschließt. Manche Textzeilen lassen
einen von perfekten Momenten träumen. Perfekte Momente, beschrieben in
der Sprache der Popkultur. So schreibt Kraftklub in ihrem Song "Songs
für Liam":

> Wenn du mich küsst schreibt Noel wieder Songs für Liam!  
>  <cite>Kraftklub</cite>

Wenn du mich küsst ist die Welt wieder perfekt... mehr gibt es dazu wohl
nicht zu sagen. Danke Kraftklub...

{% youtube 9VyrI-Bml2Q %}

