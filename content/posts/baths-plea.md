Title: Baths - Plea
Date: 2012-09-28 16:40
Author: marvin
Category: Uncategorized
Tags: baths, music
Slug: baths-plea

![bathsplea]({static}/images/bathsplea.jpg)

Der Arbeitstag ist fast zu Ende. Eigentlich sollten sich ein "Weekend
Feeling" einstellen. Ich weiß nicht. Vielleicht sind meine Gedanken
gerade ein wenig zu belegt. Ich entlasse euch ins Wochenende mit diesem
tollen Song von Will Wiesenfeld, der als
[BATHS](http://www.bathsmusic.com/) ganz großartige Musik macht. Ich mag
den Namen. Er hat in einem Interview erzählt das die Badewanne immer
sowas wie ein Rückzogsort für ihn war. Als kleinen Bonus gibt es ein
HOWTO-Video zu dem Song.

{% youtube gSoScYz_4eQ %}

{% youtube KpZScE07x8o %}

