Title: Nightwing: The Series
Date: 2015-09-02 09:28
Slug: nightwing-the-series
Tags: nightwing, batman, comics
Description: Es gibt eine kleine fanmade Nightwing Serie

![nightwing]({static}/images/nightwing_theseries.jpg)

Gerade durch Zufall über [thetvdb](http://thetvdb.org) entdeckt: [Nightwing: The Series](http://www.imdb.com/title/tt4113712/). Eine über [Kickstarter](https://www.kickstarter.com/projects/ismahawk/nightwing-the-series?lang=de) finanzierte Serie über den ehemaligen Robin [Nightwing](http://de.batman.wikia.com/wiki/Nightwing). Das ganze natürlich inoffiziell und fanmade. Selbst die Qualität kann man ertragen. Habe bis jetzt nur zwei Episoden gesehen. Werde es aber bis zum Ende durchziehen.

{% youtube o297A1wmys0 %}

{% youtube Adv8OASsZu0 %}

{% youtube kfYoS0YkAtY %}

{% youtube 43GNeN8qHwQ %}

{% youtube k-DMUJTWrf8 %}
