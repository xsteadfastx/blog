Title: Durch die Nacht mit Ai Weiwei und Wim Delvoye
Date: 2012-07-21 10:16
Author: marvin
Category: Uncategorized
Tags: aiweiwei, art, arte, documenta, durchdienachtmit, wimdelvoye
Slug: durch-die-nacht-mit-ai-weiwei-und-wim-delvoye

![517136]({static}/images/517136.jpg)

Es gibt durchaus sehr sympatische Künstler. Zwei aus dieser Abteilung
haben eine Nacht für die Sendung "Durch die Nacht mit" verbracht... und
das auf der dOCUMENTA (12). Absolut Sehenswert...

{% youtube ry8jpTzx7l8 %}

{% youtube w-Ixq_ML3EY %}

