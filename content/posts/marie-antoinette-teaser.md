Title: marie antoinette teaser
Date: 2011-08-23 07:18
Author: marvin
Category: post
Tags: kirstendunst, marieantoinette, movies, neworder, sofiacoppola
Slug: marie-antoinette-teaser

the second Marie Antoinette content for today. but this teaser is just
way too good. im a big fan of all Sofia Coppola movies but Marie
Antoinette is one of my favourites. i like the atmosphere of the
movie...the colours...the acting...and mostly the soundtrack. just
everything. and to speak for this trailer...new order...what else to
say?

{% vimeo 98887975 %}
