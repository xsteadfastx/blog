Title: Man nehme ein 25.000 $ Digiback und klebe...
Date: 2012-04-17 11:13
Author: marvin
Category: Uncategorized
Tags: holga, photography
Slug: man-nehme-ein-25-000-digiback-und-klebe

![4a11901176a38220c6f4a8bb7963f8b55df747]({static}/images/4a11901176a38220c6f4a8bb7963f8b55df747.jpg)

Man nehme ein 25.000 \$ Digiback und klebe mit Ductape eine 25 \$ Holga
dran...fertig ist die [teuerste Holga der Welt](http://goo.gl/ywdDR).
Oder wie er sie nennt: Die "Holga-cam of the Apocalypse". Nimmt das ihr
Instagram-Hipster...

Die
[Ergebnisse](http://www.flickr.com/photos/rofimike/sets/72157603026422991/with/1926585145/)
können sich sehen lassen. Ich merke immer wieder wie mich die Holga
fasziniert. Sie ist mit Abstand meine Lieblingskamera...

![22fd0ae54eed537fc67b16fc4f6736a8635744]({static}/images/22fd0ae54eed537fc67b16fc4f6736a8635744.jpg)

