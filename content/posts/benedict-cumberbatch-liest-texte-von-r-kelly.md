Title: Benedict Cumberbatch liest Texte von R. Kelly
Date: 2013-12-06 10:19
Author: marvin
Category: Uncategorized
Tags: benedictcumberbatch, sherlock
Slug: benedict-cumberbatch-liest-texte-von-r-kelly

Wir warten alle auf die dritte Sherlock Staffel. Ich bin schon ganz
aufgeregt. Und das fast mehr als auf das Doctor Who Weihnachts-Special.
Um uns allen die Zeit ein wenig zu versüßen gibt es hier Benedict
Cumberbatch, der R. Kelly Liedtexte vorliest.

{% youtube sNPp74zh8lM %}

([via](http://boingboing.net/2013/12/05/cumberbatch-reads-r-kelly.html))

