Title: Die letzten Monate in Bildern
Date: 2017-02-06 13:05
Slug: die-letzten-monate-in-bildern
Tags: photography

![eisbach surfen]({static}/images/31638023304_ea0b0d49ee_b.jpg)
Es war mal wieder Zeit eine Rolle Film zu entwickeln. Ein wenig München und ein wenig Rügen.

![isar]({static}/images/32103159270_aea572c22a_b.jpg)

![sassnitz]({static}/images/32441115586_d4e0b56f88_b.jpg)

![sassnitz]({static}/images/32481700365_c31ab5af3c_b.jpg)

![sassnitz]({static}/images/32360070841_4a8d8ea878_b.jpg)

![putbus]({static}/images/32481729795_5de4e19ba7_b.jpg)

![putbus]({static}/images/32103257700_2c4739f3ec_b.jpg)

![putbus]({static}/images/31638105354_22f7e97e00_b.jpg)

![putbus]({static}/images/31638115394_82a63afc68_b.jpg)

![binz]({static}/images/32481776915_742476bfca_b.jpg)

![binz]({static}/images/31638135354_0ed57162cc_b.jpg)

![binz]({static}/images/31670214223_c81c8cd5d5_b.jpg)

![neujahr in binz]({static}/images/32103318370_631cc55aa6_b.jpg)

![münchen]({static}/images/32359950051_d2b0c295b7_b.jpg)

![eisbach surfen]({static}/images/32441034736_d6430de76e_b.jpg)

![think coffee]({static}/images/31637983014_51566a8a9a_b.jpg)
