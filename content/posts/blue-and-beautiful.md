Title: Blue and Beautiful
Date: 2013-06-12 13:17
Author: marvin
Category: Uncategorized
Tags: melodysheep, music, nasa, neilarmstrong, science, space
Slug: blue-and-beautiful

![blueandbeautiful]({static}/images/blueandbeautiful.jpg)

"What does the sky acutally look like?". Diese Frage stellte die BBC
Neil Armstrong und aus der damals entstandenen Antwort hat
[melodysheep](https://www.youtube.com/user/melodysheep) wieder mal eine
großartige Sound und Video Collage gestrickt und sie mit seiner Musik
unterlegt. Gänsehaut... wie immer bei den Sachen die melodysheep so
produziert.

{% youtube DqC7H7_Noi8 %}

