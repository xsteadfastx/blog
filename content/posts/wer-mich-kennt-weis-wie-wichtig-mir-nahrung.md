Title: Wer mich kennt weiß wie wichtig mir Nahrung...
Date: 2012-05-30 13:38
Author: marvin
Category: Uncategorized
Tags: brittariley, diy, nature, republica, windowfarms
Slug: wer-mich-kennt-weis-wie-wichtig-mir-nahrung

![5122491825_8be955bcc0_z]({static}/images/5122491825_8be955bcc0_z.jpg)

Wer mich kennt weiß wie wichtig mir Nahrung, die Welt und Mutter Natur
wichtig ist. Was mich nervt ist der
Bioladen-Yuppie-Scheiß...zumindestens möchte ich das gerne so nennen.
Wenn man ehrlich ist hat man als Normalverdiener kaum die Chance nur
Bio-Produkte zu konsumieren. Und genau da ist meine Kritik. Es definiert
sich nur über den Konsum an sich. Wo ist die Konsumkritik? Brauchen wir
wirklich diese ganzen Produkte? Wie lange kann ich meine Kleidung
tragen? Kann ich Lebensmittel selber anbauen?

Und in genau diese Kerbe schlägt Britta Riley mit ihren
[Windowfarms](http://www.windowfarms.org/). Sie hat ein Verfahren
entwickelt wie man selbst in kleinen Wohnungen ohne Balkon Gemüse
anbauen kann. Ihre Erfahrungen und Pläne hat sie Open Source gemacht und
läd ein nachzubauen. Auf der re:publica hat sie dazu einen Talk
gehalten. Super interessant...

{% youtube 73c_qkuOLIY %}

(pic cc [Ars Electronica](http://www.flickr.com/photos/arselectronica/5122491825/))

