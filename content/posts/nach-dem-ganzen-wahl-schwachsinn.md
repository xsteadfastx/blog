Title: Nach dem ganzen Wahl-Schwachsinn
Date: 2013-09-23 10:34
Author: marvin
Category: Uncategorized
Tags: air, politics
Slug: nach-dem-ganzen-wahl-schwachsinn

Nach dem ganzen Wahl-Schwachsinn und der Post-Wahl-Melancholie versuche
ich mich mit den wichtigen und schönen Dingen dieser Welt auseinander zu
setzen. Irgendwie war es auch klar das der Mehrheit in diesem Land alles
egal zu sein scheint. Hauptsache man geht wählen. Na dann gute Nacht.
Ich höre AIR's "All I Need" auf Repeat und versuche mich in andere
Spheren zu träumen. Klappt nicht. Dem Internetz sei dank.

{% vimeo 2613582 %}

