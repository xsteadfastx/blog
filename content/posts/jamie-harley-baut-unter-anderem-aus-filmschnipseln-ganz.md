Title: Jamie Harley baut unter anderem aus Filmschnipseln ganz...
Date: 2012-05-29 13:16
Author: marvin
Category: Uncategorized
Tags: jamieharley, music, twobicycles
Slug: jamie-harley-baut-unter-anderem-aus-filmschnipseln-ganz

Jamie Harley baut, unter anderem, aus Filmschnipseln ganz tolle Musik
Videos. Ich bin gerade auf sein
[Tumblr-Blog](http://jamieharley.tumblr.com/) gestossen und kann gar
nicht aufhören seine Videos anzuschauen.

Hier ein Beispiel für sein Werk... "Moon Colors" von Two Bicycles...

{% vimeo 22165079 %}

