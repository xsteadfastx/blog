Title: Wuat? Es kommt ein neues Desaparecidos Album?
Date: 2015-04-07 14:28
Author: marvin
Category: Uncategorized
Tags: brighteyes, desaparecidos, music
Slug: wuat-es-kommt-ein-neues-desaparecidos-album

Anscheinend kommt ein neues [Desaparecidos
Album](http://pitchfork.com/news/59116-conor-obersts-band-desaparecidos-announce-new-album-payola-share-city-on-the-hill/).
Conor Oberst's emo-punk-dingens Band. Das erste Album der Band lief auf
meinen ersten Road Trips rauf und runter. Ich hatte gerade erst meinen
Führerschein und wollte nach Belgien fahren. Alles änderte mit einem
Autounfall... es lief auf einem leiernden Tape die Desaparecidos...

{% youtube FDSaq-Vwb50 %}

