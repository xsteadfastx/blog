Title: Kees van Dijkhuizen macht ganz bezaubernde Collagen zu...
Date: 2012-05-15 10:59
Author: marvin
Category: Uncategorized
Tags: movies, sofiacoppola
Slug: kees-van-dijkhuizen-macht-ganz-bezaubernde-collagen-zu

[Kees van Dijkhuizen](http://www.youtube.com/user/keesvdijkhuizen) macht
ganz bezaubernde Collagen zu bestimmten Regisseuren, aus Schnippseln
ihrer Filme. Hier ein ganz besonders gelungenes von Sofia Coppola... ich
könnte es stundenlang anschauen...

{% youtube rtieZvF-LUM %}

