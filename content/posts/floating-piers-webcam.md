Title: Floating Piers Webcam
Date: 2016-06-27 14:19
Slug: floating-piers-webcam
Tags: christo, art, italy

Christo hat es mal wieder getan. Sein neues Kunstwerk [The Floating Piers](http://www.thefloatingpiers.com/) ist zum Pilgerort geworden. Zehntausende Besucher tummeln sich auf dem See [Iseosee](https://de.wikipedia.org/wiki/Iseosee) in Italien. So viele Menschen das sie es teilweise sogar schließen mussten. Ich schaffe es leider nicht das Werk zu besuchen. Ein kleiner Trost ist diese Webcam...

{% youtube CdBKNCPKBE8 %}
