Title: Die Briten, der Guardian, Festplatten und Zoolander
Date: 2013-08-23 22:48
Author: marvin
Category: Uncategorized
Tags: edwardsnowden, guardian, movies, politics, wikileaks, zoolander
Slug: die-briten-der-guardian-festplatten-und-zoolander

![zoolanderdaten]({static}/images/zoolanderdaten.jpg)

Es hat wohl jeder mitbekommen als der britische Offizielle ins Büro des
Guardians kamen und Hardware zerstört haben die mit den Leaks von Edward
Snowden in Verbindung stehen soll. Das dies ein Armutszeugnis für eine
Demokratie ist, darüber muss ich gar nicht sprechen. Pressefreiheit
scheint ein Luxusgut zu werden. Unfassbare Szenen. Neben diesem riesen
Haufen dampfender Scheiße fragt man sich noch wieso die Kollegen nicht
nur die Festplatten zerstört haben sondern gleich mal die dazugehörigen
Laptops über den Jordan geschickt haben. Bin ich eigentlich der Einzige
der an diese Szene aus Zoolander denken musste? Wo sind eigentlich diese
Daten?

{% youtube ZkwrIZQDt50 %}

