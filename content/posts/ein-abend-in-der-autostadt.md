Title: Ein Abend in der Autostadt
Date: 2014-06-11 13:57
Author: marvin
Category: Uncategorized
Tags: autostadt, photography, wolfsburg
Slug: ein-abend-in-der-autostadt

![autostadt]({static}/images/14393478671_dc562d920b_b.jpg)

Ich war schon lange nicht mehr in der Autostadt. Das persönliche
Disneyland meiner Heimat. Weniger Mäuse, mehr Autos. Es war mal wieder
Zeit sich die Veränderungen dieses Stadt anzuschauen.

![autostadt]({static}/images/14395471882_b6089149fe_b.jpg)

![autostadt]({static}/images/14210187559_327233e137_b.jpg)

![autostadt]({static}/images/14210248890_021823cf66_b.jpg)

![autostadt]({static}/images/14393390681_2506a5c451_b.jpg)

![autostadt]({static}/images/14395669664_18ee002016_b.jpg)

![autostadt]({static}/images/14210102789_0750779ff1_b.jpg)

