Title: Immer wieder beeindruckend und bewegend wenn am Holocaust...
Date: 2012-04-19 11:19
Author: marvin
Category: Uncategorized
Tags: hashoah, holocaust, israel
Slug: immer-wieder-beeindruckend-und-bewegend-wenn-am-holocaust

Immer wieder beeindruckend und bewegend wenn am Holocaust Gedenktag,
HaShoah, in ganz Israel die Sirenen ertönen und das ganze Land stehen
bleibt um den Opfern des Holocausts zu gedenken...

{% youtube OeozUSWdoQA %}

