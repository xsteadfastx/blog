Title: Jodorowskys Dune
Date: 2014-03-14 11:43
Author: marvin
Category: Uncategorized
Tags: davidlynch, dune, lsd, movies
Slug: jodorowskys-dune

[![cc-by Sam Howzit]({static}/images/3456998345_503c0130ea_b.jpg)](https://secure.flickr.com/photos/aloha75/3456998345/)

An der DUNE-Verfilmung von David Lynch scheiden sich ja die Geister.
Inklusive David Lynch selber der teilweise noch nicht mal in den Credits
auftauchen wollte. Dabei war es nicht die erste geplante Verfilmung. Die
stammte von dem Regisseur Jodorowsky. Der hatte zwar das Buch nie
gelesen, hatte aber die Vision einen LSD-Trip auf die Leinwand zu
bringen. Unter anderem dabei: Mick Jagger, Orson Welles, H.G. Giger und
Salvador Dali. Was ein Lineup. Das Projekt fuhr legendär an die Wand
trotz sehr weit vorangeschrittenen Vorbereitungen. Es gibt ja eine
Tradition von gescheiterten Filmprojekten. Über diesen gibt es eine
Dokumentation. Die ist noch nicht draußen und ich habe sie noch nicht
gesehen, aber der Trailer sagt mir, dass er großartig wird. Yeah.

{% youtube 4WWu1kclNDA %}

