Title: Ich wollte schon monatelang über diesen Kurzfilm dieses...
Date: 2012-05-09 13:25
Author: marvin
Category: Uncategorized
Tags: coconutrecords, giacoppola, jasonschwartzman, kirstendunst, movies, music
Slug: ich-wollte-schon-monatelang-uber-diesen-kurzfilm-dieses

![dunstschwartz1]({static}/images/dunstschwartz1.png)

Ich wollte schon monatelang über diesen Kurzfilm / dieses Musikvideo zu
Coconut Records "Is This Sound Ok?" schreiben. Mehr hoffnungslose
Träumerei kann man nicht in ein paar wunderbare Minuten pressen. Jason
Schwartzman hat einfach ein paar Leute aus seiner Familie
zusammengerufen um dieses Video zu machen, allen voran seine Cousine Gia
Coppola. Sie hat geschrieben und gedreht. Mit dabei...Kirsten Dunst...

{% youtube OATV6kuehBE %}

