Title: radioseven
Date: 2011-09-29 10:19
Author: marvin
Category: post
Tags: music, radioseven
Slug: radioseven

http://www.vimeo.com/20767887

Mal wieder ein kleine Musiktipp von mir. Mal wieder in die Richtung
Elektro-Ambient-Gaze. Aufmerksam auf Radioseven bin ich durch ihr
wunderschönes Video zu "Explorer" geworden. Da dreht sich alles um die
Raumfahrt...sehr schön verträumt. Da muss man gleich mal wieder
[archive.org](http://archive.org) danken für ihre public domain Videos.
Das ganze Musikvideo aus aus NASA Footage von archive.org geschnitten.

Das letzte Release gibt es als freien Download auf bandcamp.com

<iframe width="300" height="410" style="position: relative; display: block; width: 300px; height: 410px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=678814943/size=grande3/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[An
Escape by
Radioseven](http://radioseven.bandcamp.com/album/an-escape)</iframe>

