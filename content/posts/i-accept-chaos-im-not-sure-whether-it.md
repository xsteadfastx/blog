Title: I accept chaos I'm not sure whether it...
Date: 2012-02-21 15:09
Author: marvin
Category: quote
Tags: bobdylan
Slug: i-accept-chaos-im-not-sure-whether-it

I accept chaos,  
I'm not sure whether  
it accepts me.

<cite>Bob Dylan</cite>

