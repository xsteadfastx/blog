Title: Arcade Fire, Greta Gerwig, Spinke Jonze und überhaupt
Date: 2016-09-22 10:19
Slug: arcade-fire-greta-gerwig-spinke-jonze-und-uberhaupt
Tags: arcadefire, gretagerwig, spikejonze, music

Ich habe dieses Video jetzt vielleicht 30 mal angeschaut. Auch wenn ich einige Arcade Fire Platten besitze, war mir der Hype immer zu groß. Aber wahrscheinlich bin ich wieder einmal auf dem Holzweg und schiebe super Bands vor mich hin aus unergründlichen Gründen.

Dies ist eine Aufnahme von irgendeinem Youtube Event. Arcade Fire spielen live auf der Bühne und es tut sich ein ganzes Musikvideo auf. Gedreht von Spike Jonze... mit Greta Gerwig... unfassbar.

{% youtube tBTTd0gfkn0 %}
