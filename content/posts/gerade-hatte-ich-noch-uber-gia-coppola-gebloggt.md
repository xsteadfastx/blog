Title: Gerade hatte ich noch über Gia Coppola gebloggt...
Date: 2012-05-09 13:45
Author: marvin
Category: Uncategorized
Tags: giacoppola, movies, robertschwartzman
Slug: gerade-hatte-ich-noch-uber-gia-coppola-gebloggt

Gerade hatte ich noch über Gia Coppola gebloggt...da hatte ich noch ein
wenig rum geschaut was sie bis jetzt noch so gemacht hat und bin auf
einen weiteren Kurzfilm gestossen den sie zusammen mit einem Modelabel
gemacht hat. Nicht ganz so vegan...aber trotzdem wirklich nett gemacht.
Untermalt von Robert Schwartzman's Musik...

{% vimeo 28938294 %}

