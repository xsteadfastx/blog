Title: The Last Roll of Kodachrome
Date: 2013-01-14 15:52
Author: marvin
Category: Uncategorized
Tags: kodachrome, kodak, nationalgeographic, photography, stevemccurry
Slug: the-last-roll-of-kodachrome

[![cc-by-sa Metroplex]({static}/images/1024px-KRPKR.jpg)](https://de.wikipedia.org/w/index.php?title=Datei:KRPKR.jpg&filetimestamp=20090613163228)

[Steve McCurry](https://en.wikipedia.org/wiki/Steve_McCurry) hat den
aller letzten [Kodachrome](https://en.wikipedia.org/wiki/Kodachrome)
Film beschossen und National Geographic hat darüber eine Dokumentation
gemacht. Den ganzen Beitrag durchzieht ein Hauch von Melancholie. Etwas
was man mit einer digitalen Kamera vielleicht gar nicht spüren kann.
Good Night Sweet Princess...Kodachrome...

{% youtube DUL6MBVKVLI %}

([via](http://www.lomography.com/magazine/news/2013/01/14/national-geographic-the-last-roll-of-kodachrome))

