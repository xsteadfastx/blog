Title: hochzeitsvorbereitungen
Date: 2011-09-19 12:20
Author: marvin
Category: post
Tags: canonae1, dresden, photography, rodinal, wedding
Slug: hochzeitsvorbereitungen

Hier sind einige Fotos von unseren Hochzeitsvorbereitungen. Mittlerweile
ist ja alles wieder vorbei und so schwelge ich in Erinnerungen an diese
sehr aufregende Woche meines Lebens...

![img423]({static}/images/6162509576_cfa6e11037_b.jpg)

![img424]({static}/images/6162510062_13e1800cfa_b.jpg)

![img425]({static}/images/6162510954_c17515e943_b.jpg)

![img426]({static}/images/6162511532_7f77d8cd4d_b.jpg)

![img428]({static}/images/6162512076_b85e16e039_b.jpg)

![img433]({static}/images/6162512612_6ca937eae8_b.jpg)

![img434]({static}/images/6162513184_0003b86c1c_b.jpg)

![img436]({static}/images/6162514026_e50db64afb_b.jpg)

![img439]({static}/images/6162514926_279899340a_b.jpg)

![img441]({static}/images/6162515478_17271d7bf9_b.jpg)

![img442]({static}/images/6162516080_59147e32fc_b.jpg)

![img444]({static}/images/6162516724_503a8fde7a_b.jpg)

![img443]({static}/images/6162517224_5b5512aeed_b.jpg)

![img437]({static}/images/6161982979_74fc81defd_b.jpg)

