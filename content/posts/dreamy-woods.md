Title: dreamy woods
Date: 2011-08-05 14:35
Author: marvin
Category: post
Tags: holga, photography, rodinal, wolfsburg
Slug: dreamy-woods

i dont know...i love the look of trees and woods through a holga lens.
maybe its this dreamy look i love so much. my beloved plastic box called
the holga is my favourite camera to use. when i look at holga picture
there is this soundtrack in my head...all the time...and im dreaming
away...

![Holga]({static}/images/4872858135_fd2755bd40_b.jpg)

![Holga]({static}/images/4873454798_facdfd8bf3_b.jpg)

![Holga]({static}/images/4873456826_4434964a64_b.jpg)

![Holga]({static}/images/4873473354_706e37cc28_b.jpg)

