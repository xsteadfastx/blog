Title: SEA + AIR - Do Animals Cry?
Date: 2012-10-23 12:13
Author: marvin
Category: Uncategorized
Tags: animals, music, seaandair
Slug: sea-air-do-animals-cry

![seaandairanimals]({static}/images/seaandairanimals.jpg)

Und schon hätte ich fast vergessen dieses wunderschöne, von [Benedikt
Toniolo](http://www.benedikttoniolo.de/) animierte, Musikvideo von SEA +
AIR zu posten. Ich freue mich gerade sehr über das viele Echo und die
öffentliche Begeisterung für die beiden. Verdient haben sie es...

{% youtube bxHNbP5hRAI %}

