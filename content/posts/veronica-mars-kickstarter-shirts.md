Title: Veronica Mars Kickstarter Shirts
Date: 2013-04-08 10:13
Author: marvin
Category: Uncategorized
Tags: kickstarter, veronicamars
Slug: veronica-mars-kickstarter-shirts

![image-236212-full]({static}/images/image-236212-full.jpg)

Wir sind bei fast 4.8 Millionen Dollar angekommen! Und fast täglich gibt
es News wie es voran geht. Die letzte Meldung ist besonders interessant
für Leute die die 25\$ oder mehr in das Projekt gesteckt haben: Die
Shirt Designs sind da!

![image-236213-full]({static}/images/image-236213-full.jpg)

![image-236214-full]({static}/images/image-236214-full.jpg)

![image-236215-full]({static}/images/image-236215-full.jpg)

Also... zuschlagen und den Film
[supporten](http://www.kickstarter.com/projects/559914737/the-veronica-mars-movie-project/)...
solange es noch geht...

