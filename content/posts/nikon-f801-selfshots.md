Title: nikon f801 selfshots
Date: 2011-10-18 12:40
Author: marvin
Category: post
Tags: nikonf801, photography, rodinal
Slug: nikon-f801-selfshots

Ich hoffe das es normal ist wenn man eine neue Kamera testet das
Scene-Girl-Ding macht. Ich finde das Ergebnis eigentlich ganz gut.
Deswegen habe ich sie auch hochgeladen.

![selfshot series]({static}/images/6256816375_cd0341640f_b.jpg)

![selfshot series]({static}/images/6256815691_84c8ae76b0_b.jpg)

![selfshot series]({static}/images/6257344866_bab6183c18_b.jpg)

