Title: Observer Drift - Corridors
Date: 2012-05-07 10:22
Author: marvin
Category: Uncategorized
Tags: music, observerdrift
Slug: observer-drift-corridors

![794734590-1]({static}/images/794734590-1.jpg)

Aus der Abteilung: Jemand schraubt ein wunderschönes Album bei sich im
Kinderzimmer zusammen. Observer Drift macht verträumten Dreampop (die
doppelte Verträumtheit ist Absicht). Die paar Euros bei Bandcamp lohnen
sich wirklich falls man noch nichts hat was die warmen Temperaturen der
letzten Tage wiederspiegelt. Ok gut...die sind leider schon vorbei.
Trotzdem darf man den Glauben an den Frühlich nicht aufgeben.

<iframe width="300" height="410" style="position: relative; display: block; width: 300px; height: 410px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=987835706/size=grande3/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[Corridors
by Observer
Drift](http://observerdrift.bandcamp.com/album/corridors)</iframe>

