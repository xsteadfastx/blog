Title: Die Inspiration zu "Before Sunrise"
Date: 2016-09-14 12:15
Slug: die-inspiration-zu-before-sunrise
Tags: richardlinklater, beforesunrise, amylehrhaupt

Dieser Film. Jeder der mich und meine Biographie kennt weiß über die Verbindung zu diesem Film. Ich hatte mal gehört das es eine echte Nacht gab die Richard Linklater inspirierte. Es war Amy Lehrhaupt mit der er durch die Nacht zog. Aber ich will nicht spoilern...

{% youtube zb0Q7QyvrHM %}
