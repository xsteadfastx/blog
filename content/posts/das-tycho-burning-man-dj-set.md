Title: Das Tycho Burning Man DJ Set
Date: 2015-09-15 15:27
Slug: das-tycho-burning-man-dj-set
Tags: tycho, music, burningman
Description: Wunderbares DJ Set von Tycho

![Elsewhere]({static}/images/tycho_burning_man_2015.jpg)

So das Burning Man 2015 ist vorbei und langsam trudeln die ganzen DJ Sets auf Soundcloud ein. Dieses ist von Tycho. Könnte kaum besser sein.

{% soundcloud https://soundcloud.com/tycho/elsewhere-burning-man-sunrise-set-2015 %}
