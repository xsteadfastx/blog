Title: Ach ich stehe auf diese Umbruchstimmung die gerade...
Date: 2012-05-02 09:22
Author: marvin
Category: Uncategorized
Tags: amandapalmer, kickstarter, music
Slug: ach-ich-stehe-auf-diese-umbruchstimmung-die-gerade

![sign_sq.large_]({static}/images/sign_sq.large_.jpg)

Ach ich stehe auf diese Umbruchstimmung die gerade in Musik herrscht.
Weg von den geldfressenden, großen Labels die meist eh, abgelenkt durch
ihre Profitmaximierung, nicht verstehen worum es bei der Kunst geht.

Eine ganz tolle Aktion hat Amanda Palmer gerade auf
[Kickstarter](http://www.kickstarter.com/projects/amandapalmer/amanda-palmer-the-new-record-art-book-and-tour)
gestartet. Sie braucht Geld für die Promo ihrer neuen Albums. Aber seht
selber was sie in Bob Dylan-Style zu erzählen hat...

<iframe frameborder="0" height="360px" src="http://www.kickstarter.com/projects/amandapalmer/amanda-palmer-the-new-record-art-book-and-tour/widget/video.html" width="480px"></iframe>

