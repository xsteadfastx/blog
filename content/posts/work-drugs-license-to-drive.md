Title: work drugs - license to drive
Date: 2012-02-27 08:53
Author: marvin
Category: post
Tags: johnhughes, movies, music, phoenix, workdrugs
Slug: work-drugs-license-to-drive

Was passt schon besser zusammen als Chillwave und 80er Teenager Filme?
Einer der besten Kombinationen der Popkultur. Ok ist kein Chillwave aber
das Prinzip wurde bei dem Fanvideo zu Phoenix Lisztomania zur Perfektion
gebracht. Ein Zusammenschnitt von Tanz Szenen aus [John
Hughes](http://de.wikipedia.org/wiki/John_Hughes_(Regisseur)) Filmen.
Phoenix selber bezeichnete dann dieses Fanvideo zum besten Musikvideo
was sie sich hätten vorstellen können für diesen Song. Da stimme ich nur
zu. Ich verlinke das ganze mal unter dem eigentlichen Video über das ich
hier schreibe. Dabei geht es zwar nicht um Szenen aus einem John Hughes
Film sondern aus "[License to
Drive](http://www.imdb.com/title/tt0095519/)". Oder im deutschen
"Daddy's Caddilac". Alles kommt drin vor was man sich so wünscht:
Gefälschter Führerschein bis hin zum "Knutschfelsen". Eine Ort der,
glaube ich, nur in amerikanischen Teenie-Filmen exisitert. Ganz
großartig. Vor allem kann man den Track auch noch einfach
[runterladen](http://soundcloud.com/work-drugs/license-to-drive/s-Dqt4r).
Gefällt mir und versüßt mir den Montag.

{% vimeo 37401727 %}

{% vimeo 23274394 %}

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F37524211&amp;show_artwork=true"></iframe>

