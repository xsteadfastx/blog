Title: The Making Of The Blues Brothers
Date: 2013-01-11 13:57
Author: marvin
Category: Uncategorized
Tags: bluesbrothers, documentary, movies
Slug: the-making-of-the-blues-brothers

[![cc by Rovdyr]({static}/images/851px-Stencil_Graffiti_at_Staromiejska_Street_in_Szczecin_Poland.jpg)](https://de.wikipedia.org/w/index.php?title=Datei:Stencil_Graffiti_at_Staromiejska_Street_in_Szczecin_Poland.jpg&filetimestamp=20071124213505)

Das ist der Film an den ich mich am meisten erinnern kann. Seit meiner
frühen Kindheit habe ich den Film immer und immer wieder gesehen. Viele
Zitate sind in meinen Alltagssprachgebrauch eingeflossen und die Blues
Brothers kann ich einfach immer wieder ansehen.

Vor ein paar Tagen habe ich einen Artikel zu der Entstehung des Films
auf der Website der [Vanity
Fair](http://www.vanityfair.com/hollywood/2013/01/making-of-blues-brothers-budget-for-cocaine)
gelesen. Wirklich lesenswert. Hier ist eine Dokumentation die zum 25
jährigen Jubiläum entstanden ist:

{% youtube rUTh-etxGW0 %}

{% youtube 077LqwhfzNw %}

{% youtube TPqPuSGxLtE %}

([via](http://www.openculture.com/2013/01/the_making_of_ithe_blues_brothersi_when_belushi_and_aykroyd_went_on_a_mission_for_comedy_music.html))

