Title: Der Hippie in mir und der existiert wirklich...
Date: 2012-02-21 14:39
Author: marvin
Category: status
Tags: archiveorg, gratefuldead, music
Slug: der-hippie-in-mir-und-der-existiert-wirklich

Der Hippie in mir (und der existiert wirklich) will immer wieder mal
befriedigt und unterhalten werden. Deswegen habe ich nichts dagegen ab
und zu mal die guten Grateful Dead rauszuholen, mich mit einer guten
Cola Light zurück zu lehnen und durch mein erträumtes imaginäres San
Francisco im Frühling zu spazieren. Ich sag nur [Sugar
Magnolia](http://www.archive.org/details/gd1975-06-17.aud.unknown.87560.flac16)...

<iframe src="http://www.archive.org/embed/gd1975-06-17.aud.unknown.87560.flac16" width="640" height="480" frameborder="0"></iframe>

