Title: Es sollte eigentlich Sommer sein Zumindest behauptet der...
Date: 2012-06-11 12:27
Author: marvin
Category: Uncategorized
Tags: beachhouse, joolsholland, music
Slug: es-sollte-eigentlich-sommer-sein-zumindest-behauptet-der

![361285]({static}/images/361285.png)

Es sollte eigentlich Sommer sein. Zumindest behauptet der Kalender das.
Es regnet wie aus Eimern. Es scheint auch nicht gerade mein Tag zu sein.
Wobei das wohl eine Entscheidung dazu ist. Ich sitze hier im Büro und
höre Beach House. Dies ist von ihrem Auftritt bei Jools Holland...

{% youtube Y-kGrIN3Pb4 %}

