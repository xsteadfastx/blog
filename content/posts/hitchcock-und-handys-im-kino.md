Title: Hitchcock und Handys im Kino
Date: 2012-10-29 10:50
Author: marvin
Category: Uncategorized
Tags: alfredhitchcock, movies
Slug: hitchcock-und-handys-im-kino

![hitchcockcellphone]({static}/images/hitchcockcellphone.jpg)

Der "Master of Suspense" scheint es wohl nicht gerne zu sehen, wenn man
sein Handy im Kino anlässt. Ich bin ja wirklich mal gespannt auf den
Hitchcock-Film mit Anthony Hopkins...

{% youtube rY-59UgAs8M %}

Und als kleine Zugabe gibt es den Trailer zu dem Film. Ach ja, ich freue
mich drauf...

{% youtube _Uw6fweoB8E %}

