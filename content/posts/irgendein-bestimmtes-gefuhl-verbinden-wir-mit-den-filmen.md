Title: Irgendein bestimmtes Gefühl verbinden wir mit den Filmen...
Date: 2012-05-09 09:05
Author: marvin
Category: Uncategorized
Tags: johnhughes, revolver, sixteencandles
Slug: irgendein-bestimmtes-gefuhl-verbinden-wir-mit-den-filmen

Irgendein bestimmtes Gefühl verbinden wir mit den Filmen von John
Hughes. Er erfand ein Universum in denen seine Filme spielen in denen
die ewige Jugend zu leben scheint. Gerne wäre man Teil davon...

Ich feier es ja ab das es viele Verbindungen zwischen aktueller Musik
und seinen Filmen gibt. Revolver hat das Video zu seinem Song "Still"
mit Ausschnitten aus "Sixteen Candles" online gestellt. Ach ja...

<iframe src="http://player.vimeo.com/video/36008795" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

([via](http://knicolai.com/2012/02/03/revolver-still/))

