Title: Superheroes - Kampf der Wahrheit
Date: 2015-04-19 08:09
Tags: comics, documentary, arte
Slug: superheroes-kampf-der-wahrheit


Gestern durch Zufall beim Durchzappen gesehen. Eine Dreiteilige Dokumentation der Anfänge von Comics. Inklusive mit vielen Interviews mit Legenden wie Stan Lee. Dazu die Ursprünge und Inspirationen der Superhelden. Achja, und die Rolle von Comics im zweiten Weltkrieg. Wirklich gelungen.

<iframe src="http://www.arte.tv/guide/de/embed/048390-001/medium" allowfullscreen="true" style="width: 600px; height: 344px;" frameborder="0"></iframe>

<iframe src="http://www.arte.tv/guide/de/embed/048390-002/medium" allowfullscreen="true" style="width: 600px; height: 344px;" frameborder="0"></iframe>

<iframe src="http://www.arte.tv/guide/de/embed/048390-003/medium" allowfullscreen="true" style="width: 600px; height: 344px;" frameborder="0"></iframe>
