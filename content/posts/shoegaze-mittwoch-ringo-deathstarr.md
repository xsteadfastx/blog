Title: Shoegaze Mittwoch: Ringo Deathstarr
Date: 2014-03-05 09:54
Author: marvin
Category: Uncategorized
Tags: music, ringodeathstarr
Slug: shoegaze-mittwoch-ringo-deathstarr

![artworks-000070398290-agt5t6-t500x500]({static}/images/artworks-000070398290-agt5t6-t500x500.jpg)

Auch wenn Soundwände fast wieder so salonfähig geworden sind wie einst
Anfang der Achtziger, mit so Sachen wie Chillwave und unzähligen
Reunions, versuchen wirklich wenig Bands Shoegaze zu machen. Die
Ausnahme sind Ringo Deathstarr. Sie klingen so als ob My Bloody
Valentine nie aufgehört haben. Und um ehrlich zu sein, die neue Platte
von Ringo Deathstarr habe ich jetzt schon öfters gehört als die letztes
Jahr erschienene Platte von My Bloody Valentine. Shoegaze as it best.
Bin mal kurz träumen...

<iframe frameborder="0" height="450" scrolling="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/22555415%3Fsecret_token%3Ds-HgDuT&amp;auto_play=false&amp;hide_related=false&amp;visual=true" width="640"></iframe>

([via](http://noisey.vice.com/blog/ringo-deathstarrs-new-album-is-the-soundtrack-to-gods-dreams))

