Title: Dinosaur Jr. covert Phoenix
Date: 2013-03-19 16:46
Author: marvin
Category: Uncategorized
Tags: dinosaurjr, music, phoenix
Slug: dinosaur-jr-covert-phoenix

[![cc-by Johannes Scherman]({static}/images/1024px-Dinosaur_Jr._at_WTAI_in_Stockholm.jpg)](https://en.wikipedia.org/wiki/File:Dinosaur_Jr._at_WTAI_in_Stockholm.jpg)

Es gibt so Tage da passieren ganz wunderbare Sachen. Wie wenn Dinosaur
Jr. auf einmal einen Phoenix Song covern.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/83226180&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>

([via](http://www.spin.com/articles/phoenix-dinosaur-jr-entertainment-cover-remix-bankrupt))

