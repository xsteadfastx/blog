Title: Happy Birthday Andy Warhol
Date: 2013-08-06 16:23
Author: marvin
Category: Uncategorized
Tags: andywarhol, art, webcam
Slug: happy-birthday-andy-warhol

![927px-Warhol_grave-RZ]({static}/images/927px-Warhol_grave-RZ.jpg)

Viele halten dich für überschätzt. Ich liebe deine Sachen. Heute wäre
dein Geburtstag und zur Feier des Tages gibt es hier Testaufnahmen die
du von einigen großartigen Künstlern gemacht hast. Nebenbei wurde gerade
an deinem Grab eine
[Webcam](http://earthcam.com/usa/pennsylvania/pittsburgh/warhol/?cam=warhol_figmentstream)
installiert. Geburtstagsluftballons im Wind...

{% youtube exFokKr-WEg %}

{% youtube MYrPJozmSdg %}

{% vimeo 10410439 %}

([via](http://www.openculture.com/2013/08/andy-warhol-shoots-screen-tests-of-bob-dylan-nico-salvador-dali.html))

