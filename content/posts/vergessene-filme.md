Title: Vergessene Filme
Date: 2012-09-11 13:54
Author: marvin
Category: Uncategorized
Tags: berlin, photography, rodinal, yashicat3
Slug: vergessene-filme

![Berlin]({static}/images/7970270340_20d3fc0317_b.jpg)

Es passiert nicht selten das ich Filme in Kameras vergesse. Vor allem
wenn es Kameras sind die ich selten benutze. Viel zu selten. Irgendwann
ist die Rolle voll. Ich entwickel und die Gedanken fangen nur so an zu
fliegen. Oft hatte ich die abgelichteten Momente schon vergessen. Sie
sind nur noch schwache, neblige Gestalten. Das Negativ bringt sie wieder
zum Vorschein. Und ich frage mich wieso.

Es war Berlin im Frühjahr 2012... -20 Grad.

![Berlin]({static}/images/7970269182_00f86106bb_b.jpg)

![Berlin]({static}/images/7970271524_218d53c5e8_b.jpg)

