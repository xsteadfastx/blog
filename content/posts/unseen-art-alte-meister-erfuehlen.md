Title: Unseen Art: Alte Meister erfühlen
Date: 2015-12-03 12:45
Slug: unseen-art-alte-meister-erfuehlen
Tags: art

[![Die 3D gedruckte Mona Lisa]({static}/images/unseen_art.jpg)](http://www.unseenart.org/)

Es geht um ein Crowdfunding-Projekt das klassische Gemälde erfühlbar machen lässt. [Unseen Art](http://www.unseenart.org/). Dies soll die Werke für Blinde erlebbar machen. Ich frage mich wieso erst jetzt? Es geht vor allem auch darum die 3D-Daten freizugänglich ins Internet zu stellen. So kann jeder mit einem 3D-Drucker die Gemälde ausdrucken. Ein wunderbares Projekt. Es sind oft diese Sachen die wirklich einen Unterschied machen. Ich denke ich werde da mal ein paar Euros [drauf werfen](https://www.indiegogo.com/projects/unseen-art-experiencing-art-for-the-first-time#/). Es ist wichtig das Kunst mehr und mehr zugänglich wird. Für jederman. Eine steile Parole und doch ist es sehr wichtig.

{% youtube GOF4Ipvjp8A %}
