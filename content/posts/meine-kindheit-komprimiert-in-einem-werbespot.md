Title: Meine Kindheit komprimiert in einem Werbespot
Date: 2013-01-24 11:34
Author: marvin
Category: Uncategorized
Tags: commercial, internetexplorer, microsoft, youth
Slug: meine-kindheit-komprimiert-in-einem-werbespot

![ie90s]({static}/images/ie90s.jpg)

Und das ausgerechnet in einem Werbespot von meinen alten Freunden
Microsoft. Eigentlich müsste es die Möglichkeit geben Microsoft dafür
abzumahnen das sie die schönsten meiner Kindheitserinnerungen mit ihrem
reudigen Browser zusammenbasteln. Auch wenn ich gerade in Erinnerungen
schwelge... bitte bitte benutzt einen vernünftigen Browser!

{% youtube qkM6RJf15cg %}

([via](http://www.doobybrain.com/2013/01/23/internet-explorer-child-of-the-90s/))

