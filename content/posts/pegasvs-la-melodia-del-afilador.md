Title: Pegasvs - La melodía del afilador
Date: 2013-01-08 13:24
Author: marvin
Category: Uncategorized
Tags: music, pegasvs
Slug: pegasvs-la-melodia-del-afilador

![pegasvs]({static}/images/pegasvs.jpg)

Optik und Sound sind bei diesem Video, der Band
[PEGASVS](http://pegasvs.tumblr.com/), wirklich aller erste Sahne.
Irgendwie Krautrock aber doch sehr eingängig und dreamy.

{% vimeo 54842290 %}

([via](http://www.thefoxisblack.com/2012/12/10/la-melodia-del-afilador-by-pegasvs/))

