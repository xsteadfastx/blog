Title: Musik als Waffe
Date: 2012-11-20 14:14
Author: marvin
Category: Uncategorized
Tags: documentary, music, torture, war
Slug: musik-als-waffe

![musikalswaffe]({static}/images/musikalswaffe.jpg)

Heute morgen kam die Nachricht das die Dokumentation "Musik als Waffe"
von Tristan Chytroschek einen Emmy bekommen hat. Gratulation! In der
Dokumentation begleitet ein Filmteam den Komponisten Christopher Cerf,
der über 200 Lieder für die Sesamstraße geschrieben hat. Als er erfuhr
das seine Lieder zum Foltern benutzt werden war er geschockt und wollte
mehr darüber raus finden. Ich hatte schon öfters gelesen das Musik zur
Folter eingesetzt wird. Um ehrlich zu sein bin ich vor allem schockiert
wie Musik als Soundtrack des Krieges benutzt wird. Das sich auch noch
stumpfe "Musiker" vor die Kamera setzen, nachdem sie auf einem
Militärstützpunkt aufgetreten sind, um zu sagen "wir foltern unsere
Konzertgäste jeden Abend". Ich habe ein flaues Gefühl im Magen.

{% youtube t7T0oH6PWLU %}

