Title: Erster deutscher Trailer für "Palo Alto"
Date: 2015-08-24 13:49
Slug: erster-deutscher-trailer-fuer-palo-alto
Tags: giacoppola, movies, paloalto, jamesfranco
Description: Erster deutscher Trailer für den Gia Coppola Film "Palo Alto"

![Poster]({static}/images/paloalto.jpg)

Da ist man mal drei Tage nicht im Internet und verpasst die veröffentlichung des deutschen Trailers zu "Palo Alto". Ich hatte ja immer gedacht das ich schon über den Film gebloggt habe. Pustekuchen. Gia Coppola bekannt aus einigen Werbungen und Kurzfilmen und vor allem als Cousine von Sofia Coppola hat nun ihren ersten Film fertig. Die Verfilmung der Kurzgeschichten von James Franco unter dem gleichen Namen. Ich hatte ihn mir kurz nach der amerikanischen Veröffentlichung angeschaut und war ziemlich begeistert. So sehr das ich mir sofort die beiden Soundtracks kaufte. Wie bei ihrer Cousine passiert nicht so viel in dem Film selber. Es ist eher die Athmosphere die einen völlig einnimmt. Es ist die gelebte Langeweile vom Erwachsenwerden in Palo Alto. Eine Stadt die für ihre Internet-Startups bekannt ist sowie für ihre hohe Selbstmordrate von Teenagern. Natürlich muss sich Gia Coppola den Vergleich zu ihrer Cousine stellen. Aber was solls... es ist ein wahnsinnig hypnotischer Film geworden.

{% youtube -P-XV_3HYxU %}

Hier noch ein Track vom Soundtrack von Blood Orange:

{% vimeo 40906281 %}
