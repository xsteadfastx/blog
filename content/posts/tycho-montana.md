Title: Tycho - Montana
Date: 2014-01-22 11:21
Author: marvin
Category: Uncategorized
Tags: music, tycho
Slug: tycho-montana

![artworks-000068571467-ctyent-t500x500]({static}/images/artworks-000068571467-ctyent-t500x500.jpg)

Tycho alias Scott Hansen schafft ein digitales Gesamtkunstwerk.
Angefangen bei seinen grandiosen [Blog](http://blog.iso50.com/) (eins
der wenigen die ich nicht nur um RSS-Reader lesen, sondern auch ab und
zu direkt ansurfe), seinen Artworks bis hin zu dem wunderbaren
melancholischen, elektrischen Dreampop.

Dies hier ist einer der ersten Tracks von seinem neuen Album "Awake".
Hammer.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/130572064&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
