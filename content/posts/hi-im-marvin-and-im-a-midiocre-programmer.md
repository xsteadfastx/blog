Title: Hi, I am Marvin and I am a midiocre programmer
Date: 2015-04-16 15:25
Tags: python, pycon
Slug: hi-im-marvin-and-im-a-midiocre-programmer


[Jacob Kaplan-Moss](http://jacobian.org/) hat eine Keynote auf der PyCon2015 gehalten. Es lohnt sich wirklich sie sich einmal anzuschauen. Nehmt euch die knappen 30 Minuten. Für jeden der sich noch nicht einmal traut sich selber als Programmierer zu sehen.

{% youtube hIJdFxYlEKE %}
