Title: Manche Ideen sind so großartig Man nehme Farbe...
Date: 2012-06-12 09:36
Author: marvin
Category: Uncategorized
Tags: art, edwindeen
Slug: manche-ideen-sind-so-grosartig-man-nehme-farbe

![liquid-rainbow]({static}/images/liquid-rainbow.jpg)

Manche Ideen sind so großartig. Man nehme Farbe, eine white Box und
einen Rasensprenger... fertig ist die Installation von Edwin Deen...
Liquid Rainbow...

{% vimeo 21197032   %}

([via](http://www.doobybrain.com/2012/06/11/liquid-rainbow-by-edwin-deen/))

