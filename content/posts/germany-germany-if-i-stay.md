Title: Germany Germany - If I Stay
Date: 2015-09-25 10:21
Slug: germany-germany-if-i-stay
Tags: music, germanygermany
Description: Neuer Track von Germany Germany

Wie schon so oft geschrieben verbinde ich ziemlich viel mit der Musik von [Germany Germany](http://grmnygrmny.com/). Es war mein erster Sommer in Nürnberg und eines Abends war dieses Konzert von Germany Germany. Und seit dem befällt mich eine Gänsehaut sobald ich Sachen von ihm höre. So wie bei diesem Neuen Track von ihm.

<iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=459605937/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/transparent=true/" seamless><a href="http://grmnygrmny.bandcamp.com/album/if-i-stay">If I Stay by Germany Germany</a></iframe>
