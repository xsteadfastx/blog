Title: Die Ästhetik der Kaffeezubereitung
Date: 2012-10-25 16:30
Author: marvin
Category: Uncategorized
Tags: coffee
Slug: die-asthetik-der-kaffeezubereitung

![coffeelove]({static}/images/coffeelove.jpg)

Das schöne an dem Kaffeezubereitungsprozess sind die Einzelheiten die
man beachten muss oder beachten kann. Geht es nach den Kaffee-Nerds kann
jeder einzelne Schritt den Geschmack des Endproduktes beeinflussen.
Glaubt man der Werbung braucht man von Kindern handgepflückten Nespresso
Kaffee für 80 Euro das Kilo, von George Clooney unter Wasser
handgeklöppelt und in umweltvernichtenen kleinen Aluminiumkapseln. Und
es gibt wirklich viele Menschen die das glauben. Sehe ich mir hier die
beiden Videos von [hufort]({% vimeo user578485) an, merkt man, %}
dass Kaffee doch mehr ist: Ein ästhetischer Genuss.

{% vimeo 18524628 %}

{% vimeo 48920310   %}

([via](http://www.thefoxisblack.com/2012/10/12/how-to-make-coffee-in-a-chemex-video/))

