Title: Heute habe ich mich mal hingesetzt und ein...
Date: 2012-05-30 15:21
Author: marvin
Category: Uncategorized
Tags: israel, jerusalem, panorama, photography
Slug: heute-habe-ich-mich-mal-hingesetzt-und-ein

![Ausblick Johanniter Hospitz]({static}/images/7302196918_d5d7458286_b.jpg)

Heute habe ich mich mal hingesetzt und ein Panorama aus den Fotos
gemacht, die ich 2010 in Jerusalem geschossen habe. Dies ist der
Ausblick von der Terrasse des Johanniter Hospitzes in der Jerusalemer
Altstadt.
