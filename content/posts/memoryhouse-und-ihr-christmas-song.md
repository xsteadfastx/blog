Title: Memoryhouse und ihr Christmas Song
Date: 2013-12-19 10:07
Author: marvin
Category: Uncategorized
Tags: christmas, memoryhouse, music
Slug: memoryhouse-und-ihr-christmas-song

![a3205418858_10]({static}/images/a3205418858_10.jpg)

Memoryhouse hat "Christmas Island" gecovert. Dreampop-Weihnachten. Da
habe ich ja mal gar nichts dagegen. Ich als Halbtags-Grinch. Ist ja bald
auch vorbei...

<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/track=2247335581/size=medium/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless>[Christmas
Island (The Andrew Sisters Cover) by
Memoryhouse](http://memoryhouse.bandcamp.com/track/christmas-island-the-andrew-sisters-cover)</iframe>

