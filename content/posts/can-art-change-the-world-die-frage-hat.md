Title: Can Art Change The World Die Frage hat...
Date: 2012-05-22 09:51
Author: marvin
Category: Uncategorized
Tags: art, jr, ted
Slug: can-art-change-the-world-die-frage-hat

Can Art Change The World? Die Frage hat JR gestellt als er mit seinem
TED-Talk 100.000\$ gewann. JR ist ein Streetartist der für seine großen
Portrait-Fotos bekannt ist die er in der ganzen Welt auf Wände klebt. Er
hat das Geld in das [Inside Out
Projekt](http://www.insideoutproject.net/) gesteckt. Eine Website auf
der man Portraits uploaden kann...diese werden als Poster ausgedruckt
und an einen zurück geschickt. Diese soll man dann verkleben.

{% youtube 4T1sEMhwEJA %}

Über die Ergebnisse hat er nun in einem weiteren TED-Talk geredet.

{% youtube Gn2W3X_pGh4 %}
