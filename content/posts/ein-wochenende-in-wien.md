Title: Ein Wochenende in Wien
Date: 2014-01-07 10:49
Author: marvin
Category: Uncategorized
Tags: beforesunrise, hp5, photography, wien, yashicat3
Slug: ein-wochenende-in-wien

![Hausverzierungen]({static}/images/11685094483_21bc1595d7_b.jpg)

Wir sind zwischen den Jahren nach Wien geflohen. Einfach raus und nicht
zurück schauen. Natürlich habe ich viel zu wenig Fotos gemacht. Kennt
man ja.

![Hotel Kummer]({static}/images/11685121896_62f3816fde_b.jpg)

Der Film "Before Sunrise" spielt ja in Wien. Der Film war der erste den
Christine und ich gemeinsam gesehen haben. So romantisch, verträumt und
toll ich ihn fand, so schlecht fand ihn Christine. Die Liebe und die
Abscheu zu diesem Film brachten uns auf die Idee ein paar Filmkulissen
in Wien zu suchen.

{% youtube WL8_wQ9LdvU %}

Als erstes war das Cafe Sperl dran. Ich würde sagen, die Mutter aller
Cafehäuser. Ein Ort an dem man eigentlich den ganzen Tag verstreichen
lassen will.

![Notizen]({static}/images/11684648523_f0887a7efc_b.jpg)

![Cafe Sperl]({static}/images/11684431955_db97512cc3_b.jpg)

Den Plattenladen aus dem Film konnten wir leider nicht finden. Trotzdem
ist es einer meiner liebsten Szenen im Film.

{% youtube nQpYHiB0k6k %}

![What Would Walter White Do?]({static}/images/11685250466_8d319889f8_b.jpg)

Im Film besuchen die beiden den Friedhof der Namenslosen. Ein sehr
kleiner Friedhof an der Donau auf dem Leichen begraben wurden die aus
dem Fluss gezogen wurden. So morbide es auch war. Er hatte etwas
besonderes.

![Friedhof der Namenlosen I]({static}/images/11684752213_3caf6e527e_b.jpg)

![Friedhof der Namenlosen II]({static}/images/11684783903_cb272b5e1b_b.jpg)

![Friedhof der Namenlosen III]({static}/images/11684849603_da5eb4e65f_b.jpg)

![Friedhof der Namenlosen IV]({static}/images/11684865113_c30a9cb7a6_b.jpg)

![Friedhof der Namenlosen V]({static}/images/11684995054_46e96a9626_b.jpg)

![Friedhof der Namenlosen VI]({static}/images/11684698815_b308a1d16c_b.jpg)

Mit der Freude den Friedhof gefunden zu haben, zogen wir weiter in die
Stadt.

![Kaffeehaus]({static}/images/11685047184_57a3ec1359_b.jpg)

![Franziskanerplatz]({static}/images/11685082174_ab3ef90fcc_b.jpg)

Auf dem Franziskanerplatz steht das "Kleine Cafe". Und es ist wirklich
sehr klein. So klein das wir keinen Platz drin bekommen haben.

![Kleines Cafe]({static}/images/11685189544_804bf3cc80_b.jpg)

Die letzten beiden Bilder bevor wir ins Auto nach Nürnberg gestiegen
sind.

![Christine]({static}/images/11685277164_cfe133cc82_b.jpg)

![Die letzten Minuten in Wien]({static}/images/11685643206_43d958b3b3_b.jpg)

Wien, ich habe mich verliebt.

