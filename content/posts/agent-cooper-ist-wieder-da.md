Title: Agent Cooper ist wieder da
Date: 2013-02-25 12:15
Author: marvin
Category: Uncategorized
Tags: art, michellelevy, twinpeaks
Slug: agent-cooper-ist-wieder-da

[![c by Michelle Levy]({static}/images/168462842283670351_pcOnR5R0_c.jpg)](http://www.michelle-levy.com/)

Michelle Levy hat eine
[Kunstaustellung](http://www.michelle-levy.com/agentc.html), über die
vermeintliche Rückkehr Agent Dale Coopers, gemacht. Ich glaube es ist
das Joch was wir tragen nach dem unendlich fordernden Cliffhanger am
Ende der zweiten Staffel. Kaum eine andere Serie hatte diesen
populturellen Impact auf die Kreativität von Künstlern, wie Twin Peaks.
Und wenn wir Glück haben kommt ja doch irgendwann mal die dritte
Staffel... wo bist du Agent Cooper?

{% vimeo 34109390 %}

([via](http://welcometotwinpeaks.com/photos/dale-cooper-sightings/))

