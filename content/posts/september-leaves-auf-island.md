Title: September Leaves auf Island
Date: 2012-07-09 12:04
Author: marvin
Category: Uncategorized
Tags: fernweh, island, music, septemberleaves
Slug: september-leaves-auf-island

![467163]({static}/images/467163.jpg)

Wie lange will ich eigentlich schon nach Island? Und wenn ich diese
Bilder sehe, gepaart mit dieser Musik, weiß ich wieso mich dieses
tiefsitzende Fernweh antreibt... Könnt ihr das Rauschen des Wassers
hören?

Danke September Leaves...

{% vimeo 45351562 %}

