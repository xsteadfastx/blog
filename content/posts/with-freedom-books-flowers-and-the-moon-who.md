Title: With freedom books flowers and the moon who...
Date: 2012-03-02 11:48
Author: marvin
Category: quote
Tags: oscarwilde
Slug: with-freedom-books-flowers-and-the-moon-who

With freedom, books, flowers, and the moon, who could not be happy?

<cite>Oscar Wilde</cite>

