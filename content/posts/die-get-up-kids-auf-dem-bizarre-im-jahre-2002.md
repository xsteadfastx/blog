Title: Die Get Up Kids auf dem Bizarre im Jahre 2002
Date: 2013-04-30 09:09
Author: marvin
Category: Uncategorized
Tags: music, thegetupkids
Slug: die-get-up-kids-auf-dem-bizarre-im-jahre-2002

Es war das Jahr 2000 als wir das Album "Something to Write Home About"
entdeckten. Wir hörten es den ganzen Sommer. Wir tanzten und fuhren
durch die warmen Nächte in die Stadt, nur um die Songs so laut wie
möglich aufdrehen zu können. Die ganze Stadt zu beschallen. In einem
alle Probleme die so ein Teenager hat. Vor allem aber Liebeskummer.
Immer eine riesen Portion davon. Die Songs haben immer gepasst. Ach
ja...

{% youtube 1vQTNL-0nWc %}

