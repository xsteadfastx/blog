Title: Was bedeutet schon Geld Ein Mensch ist erfolgreich...
Date: 2012-02-17 16:10
Author: marvin
Category: quote
Tags: bobdylan, music
Slug: was-bedeutet-schon-geld-ein-mensch-ist-erfolgreich

Was bedeutet schon Geld? Ein Mensch ist erfolgreich, wenn er zwischen
Aufstehen und Schlafengehen das tut, was ihm gefällt.

<cite>Bob Dylan</cite>

