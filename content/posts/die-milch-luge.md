Title: Die Milch-Lüge
Date: 2012-08-07 08:01
Author: marvin
Category: Uncategorized
Tags: graffiti, milch, vegan
Slug: die-milch-luge

![582155_426744460701123_1097707471_n]({static}/images/582155_426744460701123_1097707471_n.jpg)

Es steht sogar an den Wänden... der ultimative TV-Tipp! Die angepriesene
Doku ist aber wirklich gut. Sie enthält für mich keine neuen
Informationen, aber mein Umfeld glaubt eher einer Doku auf NDR als dem
paranoiden Vegan-Fantast. Also...anschauen und verteilen...

{% youtube PTf9F7nTYg4 %}

