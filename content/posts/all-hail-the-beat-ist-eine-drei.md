Title:  All Hail The Beat ist eine drei...
Date: 2012-05-16 07:54
Author: marvin
Category: Uncategorized
Tags: music, nelsongeorge, rolandtr808
Slug: all-hail-the-beat-ist-eine-drei

"All Hail The Beat" ist eine drei minütige Dokumentation über die
berühmte Drummachine Roland TR-808. Das Ding ist von dem Journalisten
Nelson George. Wahnsinn in wie vielen Songs diese Drumsounds stecken.

{% vimeo 40094608   %}

([via](http://idealistpropaganda.blogspot.de/2012/05/all-hail-beat-short-history-of-roland.html))

