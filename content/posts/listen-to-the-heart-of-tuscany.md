Title: Listen To The Heart Of Tuscany
Date: 2012-09-12 10:47
Author: marvin
Category: Uncategorized
Tags: fernweh, fieldrecordings, toskana
Slug: listen-to-the-heart-of-tuscany

![737302]({static}/images/737302.jpg)

Das hat mir gerade noch gefehlt. Die ersten zwei Tage am Arbeitsplatz
trennen mich von meinem Urlaub. Unsere Zeit in der Toskana. Jeden morgen
kommt die Sehnsucht in mir hoch wieder weg zu sein.

Dies wird durch dieses Video auch nicht wirklich besser. Aber danke
[Gunther Machu]({% vimeo gunthermachu) für dieses wunderschöne %}
Fernweh.

> One of my favorite places on earth - the heart of the Tuscany in
> Italy, from a macro perspective. No music, just the sounds of little
> life in a fantastic atmosphere.  
>  <cite>Gunther Machu</cite>

{% vimeo 48890521   %}
([via](http://www.kraftfuttermischwerk.de/blogg/?p=41092))

