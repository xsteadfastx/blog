Title: Uns Gregor
Date: 2013-09-05 12:12
Author: marvin
Category: Uncategorized
Tags: gregorgysi, politics
Slug: uns-gregor

Die Linke sind ein Beispiel für das Dilemma mit der Politiklandschaft.
Die Parteien sind meist voll mit komischen Gesocks mit den beknacktesten
Ansichten. Doch immer mal wieder gibt es Lichtblicke. Einer davon ist
und bleibt Gregor Gysi. Einer der wenigen Politiker bei denen ich nicht
permanent im Strahl kotzen möchte. Dies ist seine letzte Rede im
Bundestag. Ich bin Fan...

{% youtube XVYDs_apGvk %}

Und als kleine Zugabe: Wenn die Union keine Argumente akzeptiert muss
man halt mal die Bibel zitieren:

{% youtube HF7FZPJGaKI %}

Volker Kauder disqualifiziert sich dann auch noch gleich selber mit
seinem Einwurf nach Gysi's Rede. Damit konnte ja niemand rechnen...

