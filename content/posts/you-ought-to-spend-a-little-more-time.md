Title: You ought to spend a little more time...
Date: 2012-02-15 08:22
Author: marvin
Category: quote
Tags: johnhughes, movies, thebreakfastclub
Slug: you-ought-to-spend-a-little-more-time

You ought to spend a little more time trying to make something of
yourself and a little less time trying to impress people

<cite>The Breakfast Club</cite>

