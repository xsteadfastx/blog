Title: Live And Let Live
Date: 2013-08-12 15:08
Author: marvin
Category: Uncategorized
Tags: crowdfunding, documentary, marcpirschel, vegan
Slug: live-and-let-live

![liveandletlive]({static}/images/liveandletlive.jpg)

> Live and Let Live ist ein Dokumentarfilm über unser Verhältnis zu
> Tieren, die Geschichte des Veganismus und die ethischen, ökologischen
> und gesundheitlichen Hintergründe, die Menschen dazu bewegen, vegan zu
> leben.

Wir haben ja kaum den neoliberalen Shitstorm zum Veggietag überstanden.
Eine Forderung sich keine Gedanken über die Zukunft dieser Welt machen
zu müssen. Um es mit Olli Schulz zu sagen: "Es gibt einfach mehr
Arschlöcher als gute Menschen auf dieser Welt". Kommen wir zu tollen
Sachen. Der Dokumentarfilm "Live And Let Live" von Marc Pirschel ist
wohl mit dem Dreh und Rohschnitt fertig. Er hat unter anderem ein super
Grundlagenbuch zum Thema Veganismus geschrieben und die super Straight
Edge Dokumentation "EDGE" gedreht. Jetzt wird per Crowdfunding Geld für
die finalen Schritte des Films gesammelt. Also...
[supporten](http://www.startnext.de/liveandletlive)!!!

{% youtube 10MK7v6Mhjc %}

{% youtube hepwlJD_xz8 %}

