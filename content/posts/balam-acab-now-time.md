Title: Balam Acab - Now Time
Date: 2012-10-02 14:49
Author: marvin
Category: Uncategorized
Tags: balamacab, jamieharley, music
Slug: balam-acab-now-time

![balamacabnowtime]({static}/images/balamacabnowtime.jpg)

[Jamie Harley](http://jamieharley.tumblr.com/) hat ein ganz tolles,
verträumtes Video zu Balam Acab's Track "Now Time" gemacht. Balam Acab
spielt ja bald in Nürnberg. Ich bin wirklich am überlegen ob ich da
hingehe.

{% vimeo 28661162 %}

