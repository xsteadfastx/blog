Title: The XX - Angels
Date: 2012-07-24 07:28
Author: marvin
Category: Uncategorized
Tags: gema, music, proxtube, psychedelic, thexx, youtube
Slug: the-xx-angels

![526941]({static}/images/526941.jpg)

Ich weiß ja nie wie ich damit umgehen soll in Deutschland gesperrte
Musikvideos hier zu posten. Eigentlich lasse ich es immer. Ich weiß halt
nicht wie viele Menschen von dem wunderbaren Plugin
[Proxtube](https://proxtube.com/) gehört haben. Eins ist klar... die
GEMA scheint Musik zu hassen... so kommt es mir zu mindestens vor.

Zurück zur Musik von The XX. Ist schon ein paar Jahre her als das erste
Album erschienen ist. Erst war ich skeptisch... Hype und so. Eine Woche
nach Release-Datum tauchte das Album schon bei Rolling Stone Lesern in
den "Top 10 meines Lebens" Hitlisten auf. So wie eigentlich alle Hype
Alben... und die sind oft austauschbar. Ich hörte auf den Plattendealer
meines Vertrauens und er hatte Recht. Ungehört kaufte die Vinyl... ich
habe es nie bereut.

Hier nun der erste Einblick in ihr neues Album "Coexist". Es lohnt sich
schon wegen dem Video...

{% youtube _nW5AF0m9Zw %}

