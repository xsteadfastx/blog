Title: Die letzte Rolle von gestern
Date: 2012-01-19 11:06
Author: marvin
Category: post
Tags: nuernberg, photography, rodinal, yashicat3
Slug: die-letzte-rolle-von-gestern

Ich habe mal den Film in meiner Yashica T3 voll gemacht. Ich brauchte
sie für Konzert Fotos. Spontan bin ich bei frostigem Sonnenschein raus
um noch ein paar Bilder zumachen bevor es ans Entwickeln ging.

![logo]({static}/images/6719561699_cd8fa75773_b.jpg)

![img722]({static}/images/6719562557_b584c9404a_b.jpg)

![img724]({static}/images/6719563523_75bd43983d_b.jpg)

![img725]({static}/images/6719564403_87e1877f83_b.jpg)

![img726]({static}/images/6719565199_a36b925f88_b.jpg)

![img731]({static}/images/6719566613_55e863f3d5_b.jpg)

![img732]({static}/images/6719567379_c4d30f4e0b_b.jpg)

![img733]({static}/images/6719568283_9c053e3c38_b.jpg)

![img739]({static}/images/6719569355_819113aa60_b.jpg)

![img740]({static}/images/6719570237_904fcefd26_b.jpg)

![img741]({static}/images/6719571265_cfe0febbf3_b.jpg)

![img742]({static}/images/6719572075_1fb1ba3d1c_b.jpg)

![img743]({static}/images/6719572967_a14bccd3b6_b.jpg)

![img746]({static}/images/6719574133_344c85a820_b.jpg)

