Title: Man könnte es Frühling nennen also fast zumindestens...
Date: 2012-04-26 14:27
Author: marvin
Category: Uncategorized
Tags: music, summerheart
Slug: man-konnte-es-fruhling-nennen-also-fast-zumindestens

Man könnte es Frühling nennen...also fast zumindestens. Der passende
Soundtrack flatterte gerade per Bandcamp rein: [Summer
Heart](http://summerheart.bandcamp.com/album/about-a-feeling). Ganz
wunderbarer Dreampop / Chillwave. Was auch sonst sollte ich hören? Ich
weiß es nicht... es ist bald Freitag...die Sonne scheint. Das macht wohl
was mit mir...

<iframe width="300" height="410" style="position: relative; display: block; width: 300px; height: 410px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=1202109290/size=grande3/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[About
A Feeling by Summer
Heart](http://summerheart.bandcamp.com/album/about-a-feeling)</iframe>

