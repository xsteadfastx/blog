Title: V Motion Project
Date: 2012-07-24 11:11
Author: marvin
Category: Uncategorized
Tags: hacking, kinect, music
Slug: v-motion-project

![527760]({static}/images/527760.jpg)

Man nehme einen Kinect-Controller...baut ein nettes optisches Interface
und verbindet das ganze mit Ableton Live... und schon kann man seine
Musik tanzen. Akustisch haut mich das jetzt nicht vom Hocker... nett
anzusehen ist es aber wirklich...

{% youtube YERtJ-5wlhM %}

Mehr technische Infos gibt es
[hier](http://www.custom-logic.com/blog/v-motion-project-the-instrument/)...

