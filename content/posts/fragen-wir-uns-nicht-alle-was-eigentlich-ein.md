Title: Fragen wir uns nicht alle was eigentlich ein...
Date: 2012-05-11 12:42
Author: marvin
Category: Uncategorized
Tags: hipster, juliepercha
Slug: fragen-wir-uns-nicht-alle-was-eigentlich-ein

Fragen wir uns nicht alle was eigentlich ein Hipster ist? Wir gebrauchen
das Wort so oft, doch gibt es eine passende Umschreibung für das Wort?
Was sind die Eigenschaften von Hipstern?

Dieser Frage ist Julie Percha nachgegangen. Sie kommt aus Paris und
kannte das Wort nicht als sie nach New York ist um herauszufinden was es
mit dem Hipstertum auf sich hat.

{% vimeo 41264640 %}

([via](http://www.thefoxisblack.com/2012/05/10/whats-a-hipster-i-think-everyones-a-hipster/))

