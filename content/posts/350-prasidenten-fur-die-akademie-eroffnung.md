Title: 350 Präsidenten für die Akademie: Eröffnung
Date: 2012-07-13 08:48
Author: marvin
Category: Uncategorized
Tags: art, nikond60, nuernberg, photography, verblonshen
Slug: 350-prasidenten-fur-die-akademie-eroffnung

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560607154_b02b416a6a_b.jpg)

Da ist sie... unsere eigene Terrakottaarmee. Am Mittwoch war die
Eröffnung der Jahresausstellung der Akademie in Nürnberg. Verblonshen
ist mit einem Stand vertreten an dem wir 350 Präsidenten der Akademie,
gegossen aus Gips, verkaufen. Wir hatten zwar einiges zu tun mit dem
verkaufen aber trotzdem habe ich ein paar Fotos machen können.

Die nächsten drei Tage ist der Stand geöffnet und es kann fleißig
eingekauft werden... El Presidente und so...

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560605418_1999005817_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560604556_50ed5cd0cc_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560603416_2059fda2d6_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560601370_068ff4ff46_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560602196_f7d9dc7b43_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560599378_205583d9f6_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560600472_d86c1fe9d6_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560597372_60768590f5_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560598454_5927531ccf_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560596282_d9790d7549_b.jpg)

![350 Präsidenten für die Akademie: Eröffnung]({static}/images/7560595326_49bfd02309_b.jpg)

