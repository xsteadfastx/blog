Title: lost notebooks of hank williams
Date: 2011-09-26 07:48
Author: marvin
Category: post
Tags: hankwilliams, music
Slug: lost-notebooks-of-hank-williams

{% vimeo 28206614 %}

Bob Dylan arbeitet zur Zeit an einer Compilation von Songs die auf
ungenutze Liedtexte aus Hank Williams hinterlassenden Notizbüchern
zurück geht. Mit dabei sind unter anderem Jack White, sein Sohn Jakob
Dylan und andere Musikgrößen. Der Aufbau erinnert mich sehr stark an die
"Mermaid Avenue" Platten von Wilco und Billy Bragg. Dort wurden Lieder
um die hinterlassenden Texte von Woody Guthrie geschrieben und vertont.
Die beiden Platten die daraus entstanden sind zählen zu meinen
Lieblingswerken von Wilco.

Ich verehre Hank Williams sehr und deswegen freue ich mich auf das Werk.
Alleine schon das Robert Zimmermann seine Finger mit dem Spiel hat. Das
ganze wird auf seiner Plattenfirma Egyptian Records erscheinen.

Mehr Infos gibt es in
[diesem](http://www.nytimes.com/2011/09/25/arts/music/bob-dylan-assembles-the-lost-notebooks-of-hank-williams.html?_r=1)
NY Times Artikel.

