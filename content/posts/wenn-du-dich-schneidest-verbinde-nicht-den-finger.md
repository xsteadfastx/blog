Title: Wenn du dich schneidest verbinde nicht den Finger...
Date: 2012-05-30 07:01
Author: marvin
Category: Uncategorized
Tags: josephbeuys
Slug: wenn-du-dich-schneidest-verbinde-nicht-den-finger

Wenn du dich schneidest, verbinde nicht den Finger, sondern das Messer.

<cite>Joseph Beuys</cite>

