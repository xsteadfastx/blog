Title: Touché Amoré - Palm Dreams
Date: 2016-07-26 16:28
Slug: touche-amore-palm-dreams
Tags: toucheamore, music, davidlynch

Touché Amoré bringen am 19.09. ihr neues Album raus. Nun ist schon das erste Video online. Eine Hommage an einen meiner Lieblingsfilme "Mulholland Drive".

{% youtube aCj2VhmnluE %}
