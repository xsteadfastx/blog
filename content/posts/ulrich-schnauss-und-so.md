Title: Ulrich Schnauss und so
Date: 2014-07-07 10:33
Author: marvin
Category: Uncategorized
Tags: elizabethtown, movies, music, ulrichschnauss
Slug: ulrich-schnauss-und-so

Das erste mal ist mit die Musik von Urlich Schnauss aufgefallen in dem
Film
[Elizabethtown](https://de.wikipedia.org/wiki/Elizabethtown_%28Film%29).
In der Szene in der Kirsten Dunst und Orlando Bloom nach einer durch
telefonierten Nacht den Sonnenaufgang entgegen fahren, um sich in der
Mitte zu treffen. Eine Szene in die ich mich sofort verliebt hatte.
Ulrich Schnauss hat sie perfekt vertont. Danach habe ich mich mal durch
seine Diskographie gehört und wurde nicht enttäuscht. Elektronischer
Shoegaze. Das trifft es eigentlich ganz recht. Sommernächte und lange
Fahrten mit runtergekurbelten Fenstern.

{% youtube JfNNlkaeTtg?t=1m39s %}

[Hier](http://www.theransomnote.co.uk/music/mixes/ulrich-schnauss-the-ransom-note-mix-talk/)
ist der Mix den Mr. Ulrich Schnauss für The Ransom Note gemacht hat.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/155800638&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
