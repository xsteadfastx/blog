Title: Die letzten Monate in Holga Fotos
Date: 2012-01-05 12:11
Author: marvin
Category: post
Tags: holga, muenchen, nuernberg, photography, rodinal, wolfsburg
Slug: die-letzten-monate-in-holga-fotos

Worte können kaum beschreiben wie verzweifelt ich heute morgen um ein
Uhr Nachts auf dem Büroboden saß und vor Wut meinen Scanner aussernander
nahm. Das immer wiederkommende Problem von komischen Streifen auf den
Negativ-Scans. Ich habe alles probiert was das Zerlegen des Scanners
hinfällig machen sollte: Reinigen der Kalibrierungsfläche und das
Netzteil für längere Zeit abstecken. Nichts hat geholfen. Im Kopf
googelte ich schon nach einem neuen Scanner. Gar nicht so einfach wenn
man auch 120mm Negative einscannen möchte. Denkt man daran das man davon
abhängig ist läuft einem der Schauer den Rücken runter. Was wäre wenn?
Gott bewahre! Wahrscheinlich war es nur ein Staubkorn der die schlieren
verursachte. Scanner aufgemacht, mit Isopropanol die Scheibe gereinigt,
alles wieder ok. Und so konnte ich die Bilder dann doch noch einscannen
und hochladen.

Im November waren Christine, Ellen und ich in München zwecks einer
Kunstkalendarverleihung. Ich hatte mir wie immer viel mehr vorgenommen.
Am Ende habe ich zwei Fotos gemacht...

![img688]({static}/images/6637936433_6df8a1d8b7_b.jpg)

![img689]({static}/images/6637939319_f62830c155_b.jpg)

Kurz darauf waren wir in Wolfsburg und neben meiner Arbeit dort haben
wir das Kunstmuseum und den Kunstverein Wolfsburg besucht. Im
Kunstmuseum zeigt man gerade eine Ausstellung zum Thema
"Entschleunigung". Highlight sind zwei Autos die permanent pneumatisch
wenige Zentimenter pro Tag aufeinander "zurasen". Das ganze heißt "The
Slow Inevitable Death of American Muscle" und ist von Jonathan Schipper.

![img691]({static}/images/6637945429_8eaac42d5c_b.jpg)

![img690]({static}/images/6637941953_dbb8583473_b.jpg)

Ich hatte die Idee viele Fotos vom den Christkindlesmarkt in Nürnberg zu
machen. Was ist rausgekommen? Ein paar wenige Fotos die ich an einem
Nachmittag gemacht habe. Das Wetter hatte nicht wirklich mitgespielt und
es war sehr früh zu dunkel für den ISO 400 Tri-X Film.

![img692]({static}/images/6637948541_957bd2c521_b.jpg)

![img693]({static}/images/6637951131_27cba961ae_b.jpg)

![img694]({static}/images/6637954767_00e959de5b_b.jpg)

![img695]({static}/images/6637957475_87a11defd9_b.jpg)

![img696]({static}/images/6637960137_a6e24d6d18_b.jpg)

![img697]({static}/images/6637964813_5d321863f5_b.jpg)

