Title: Marietta - As It Were
Date: 2015-09-24 11:30
Slug: marietta-as-it-were
Tags: emo, music, marietta
Description: EMO YEAH

Wie großartig ist es eigentlich das es in letzter Zeit wieder richtig gute Emo-Bands gibt? Ich rede jetzt nicht von den hochprodizierten-Schmink-Bands. Neuste Entdeckung von mir [Marietta](https://www.facebook.com/whereismarietta). Da schaut man das neue [Modern Baseball Video]({static}/posts/modern-baseball-rock-bottom.md) und googelt mal dem Band-Shirt des Sängers hinterher. Auf jeden Fall super und ich freue mich noch mehr von neuen Bands zu hören.

{% youtube wpAmooW8FLs %}

<iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=1663129501/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/transparent=true/" seamless><a href="http://whereismarietta.bandcamp.com/album/as-it-were">As It Were by Marietta</a></iframe>
