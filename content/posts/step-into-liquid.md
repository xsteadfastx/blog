Title: Step Into Liquid
Date: 2012-06-29 10:06
Author: marvin
Category: Uncategorized
Tags: documentary, movies, stepintoliquid, surfing
Slug: step-into-liquid

![432171]({static}/images/432171.jpg)

Auch wenn ich leider nie surfen konnte, bin ich fasziniert von der Kunst
des Wellenreitens. Manchmal wünschte ich mir das ich am Meer leben
könnte...jeden Tag surfen und so. Ach ja...wahrscheinlich würde ich
ertrinken.

Ein guter Freund von mir hatte vor ein Jahr lang durch die Welt zu
reisen um zu surfen. Ein Traum! Er zeigte mir diesen Film...Step Into
Liquid...und ich verstand ihn sofort wieso er das vor hatte...

Den vollen Film gibt es [hier]({% youtube Mt0yOFKIOic)... %}

