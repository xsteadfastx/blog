Title: Wildhoney - Laura
Date: 2015-09-14 08:34
Slug: wildhoney-laura
Tags: music
Description: Neuer Track von dem nächsten Wildhoney Release

![Wildhoney - Your Face Sideways]({static}/images/wildhoney_your_face_sideways.jpg)

Es ist Montag. Zeit für etwas schönen Shoegaze von Wildhoney. Die bringen bald eine neue EP raus. Dies ist der Track "Laura".

{% soundcloud https://soundcloud.com/topshelfrecords/wildhoney-laura/s-fv6lI %}

([via](http://www.stereogum.com/1829262/wildhoney-laura-stereogum-premiere/mp3s/))
