Title: FAQ für besorgte Bürger
Date: 2015-10-16 11:55
Slug: faq-fuer-besorgte-buerger
Tags: politics, besorgtebuerger

[Enno Lenze](https://ennolenze.de) hat ein [FAQ](https://ennolenze.de/faq-fuer-besorgte-buerger/2073/) für "besorgte Bürger" geschrieben. Ich finde den Ansatz nicht schlecht. Ich stimmte mit allem nicht überein und auch die Ausseinandersetzung mit diesen Menschen liegt mir ein wenig Quer im Magen. Aber nur weil mir sowas schwer fällt muss es nicht immer falsch sein. Es ist auf jeden Fall eine gute Idee.
