Title: Ich hatte ja schon von dem unfassbaren Nikon...
Date: 2012-05-10 11:35
Author: marvin
Category: Uncategorized
Tags: nikon, photography
Slug: ich-hatte-ja-schon-von-dem-unfassbaren-nikon

![nikon-fisheye_03]({static}/images/nikon-fisheye_03.jpg)

Ich hatte ja schon von dem unfassbaren Nikon 6mm f/2.8 fishey
[berichtet](http://xsteadfastx.org/2012/04/26/das-nennt-ihr-ein-fisheye-das-ist-ein/).
Jetzt hat jemand sich das Objektiv geschnappt und es auf eine Nikon D800
geschraubt. Einfach um sich mal vorzustellen welchen Bereich man damit
erfassen kann. Alleine die Aufmachung des Objektives und der Koffer dazu
ist weltklasse...

![nikkor-6mm-lens-2]({static}/images/nikkor-6mm-lens-2.jpg)

{% youtube zIDbw4gjunY   %}

([via](http://www.doobybrain.com/2012/05/09/nikkor-6mm-f2-8-fisheye-lens/))

