Title: Die Overhead-Einstellungen des Wes Anderson
Date: 2012-07-06 09:01
Author: marvin
Category: Uncategorized
Tags: movies, wesanderson
Slug: die-overhead-einstellungen-des-wes-anderson

![457859]({static}/images/457859.jpg)

Es gibt so manche Kameraeinstellungen die charakteristisch für einen
Regisseur sind. Bei Wes Anderson sind es, neben seiner wunderbaren
Farben, auch die Overhead-Shots. Hier hat jemand diese einfach mal
zusammen geschnitten... Träumerei...

{% youtube PNpVURAgG5g %}

