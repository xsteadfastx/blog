Title: Künstler, um ein Versager sein zu können
Date: 2013-01-10 17:12
Author: marvin
Category: Uncategorized
Tags: art, mikekelley, sonicyouth
Slug: kunstler-um-ein-versager-sein-zu-konnen

> "Ich wurde Künstler, um ein Versager sein zu können"  
>  <cite>Mike Kelley</cite>

Eine äußerst großartige Aussage von, dem leider viel zu früh gestorbene,
Mike Kelley. Wie froh ich bin das es ok ist ein Versager zu sein. Erfolg
und Gewinn ist doch eh nur was für Loser...oder so etwas.

Und als kleine Zugabe gibt es eine Ausgabe von "Whats in my bag" im
Amoeba Plattenladen mit dem guten Mike Kelley. Ich will ihn für das
Sonic YOuth "Dirty"-Cover immer noch die Füße küssen.

{% youtube lsd1bWZ1Xbo %}

