Title: Gestern Mittag war unsere Reise in mein Zuhause...
Date: 2012-06-18 09:34
Author: marvin
Category: Uncategorized
Tags: art, nikond60, photography, wolfsburg
Slug: gestern-mittag-war-unsere-reise-in-mein-zuhause

![Family]({static}/images/7392846566_47c91a2cd2_b.jpg)

Gestern Mittag war unsere Reise in mein Zuhause wieder vorbei. Noch
schnell habe ich ein Abschiedsfoto gemacht und dann ging es auf die viel
zu überfüllte Autobahn. Es war zwar sehr kurz...aber wie immer eine sehr
schöne Zeit bei meinen Eltern.

Wir durften keine Zeit verlieren und sind gleich in Christines Atelier
gefahren. Bald ist Jahresaustellung und es wird Zeit Sachen zu
produzieren. Aus dem kalten Norden ins das sonnige und warme Franken.

![Wenzelschloss.]({static}/images/7392847270_5a2e8bf3e0_b.jpg)

![Wenzelschloss.]({static}/images/7392848570_7f6232e2f3_b.jpg)

![Wenzelschloss.]({static}/images/7392845756_a490776162_b.jpg)

