Title: Deerhoof gegen den Large Hadron Collider
Date: 2015-09-22 14:38
Slug: deerhoof-gegen-den-large-hadron-collider
Tags: cern, lhc, music, deerhoof
Description: Deerhoof sind im CERN aufgetreten

{% giphy eTEI6QUKclNRe %}

Es gibt ein Musikprojekt des CERN mit dem namen [EX/NOISE/CERN](http://exnoisecern.ch/film). Es geht darum experimentelle Musik zu den Geräuschen des Large Hadron Colliders zu spielen. In der jetzigen Ausgabe tut das die Lieblingsband von Matt Groening: [Deerhoof](http://home.web.cern.ch/about/updates/2015/09/indie-band-deerhoof-experiment-sound-cern). Das ganze für mich mit einem lachenden und einem weinenden Auge. Ich habe es durch unseren Umzug nicht geschafft sie in Nürnberg Live zu sehen. Kann ja noch kommen... hoffentlich.

{% youtube tZqQGbSnSTA %}
