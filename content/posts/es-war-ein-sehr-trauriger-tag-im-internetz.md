Title: Es war ein sehr trauriger Tag im Internetz...
Date: 2012-06-06 13:03
Author: marvin
Category: Uncategorized
Tags: music, trololo
Slug: es-war-ein-sehr-trauriger-tag-im-internetz

![344331]({static}/images/344331.png)

Es war ein sehr trauriger Tag im Internetz...der Trololo-Guy, oder auch
[Eduard Anatoljewitsch
Chil](http://de.wikipedia.org/wiki/Eduard_Anatoljewitsch_Chil) im
"real-life", ist gestorben. Zu seinem Andenken kommt jetzt eine zu 800%
verlangsamte Version seines Liedes, was durch diesen Effekt wie ein
Ambient Meisterwerk klingt.

{% youtube 2HZAReul9IQ %}

