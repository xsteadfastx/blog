Title: Wie mich das Fernweh packt Weit weg in...
Date: 2012-01-12 14:07
Author: marvin
Category: post
Tags: dreams, fernweh, nature, travel
Slug: das-ewige-fernweh

Wie mich das Fernweh packt. Weit weg in die Natur möchte ich. Auch wenn
Natur nur alte Backsteine bedeutet. Ich will jeden Stein umdrehen und
Menschen auf der ganzen Welt nur fast kennenlernen. Ich bin doch so
schüchtern und vielleicht als weltfremd und unsozial zu bezeichnen. Ich
will hunderte Fotos schießen. Fotos die nur in meinen Träumen
stattfinden. Ich würde doch nie jemanden nach seinen Portrait bitten.
Meine Erlebnisse würde ich in Notizbüchern festhalten, die ich mich nie
trauen würde zu schreiben.

