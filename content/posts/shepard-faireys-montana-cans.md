Title: Shepard Fairey's Montana Cans
Date: 2013-02-27 14:47
Author: marvin
Category: Uncategorized
Tags: art, graffiti, montana, shepardfairey
Slug: shepard-faireys-montana-cans

![Spay-cans-group-shots-500x333]({static}/images/Spay-cans-group-shots-500x333.jpg)

Montana hat Shepard Fairey seine Signature-Kannen spendiert. Egal. Aber
einfach nur schön mal in sein Atelier zu schauen...

{% youtube G7jeveFE9Ko %}

([via](http://www.doobybrain.com/2013/02/27/shepard-fairey-x-mtn-colors/))

