Title: Blackout Poems 
Slug: blackout-poems
Date: 2014-01-29 13:00
Tags: art, bild, random


![Post von Wagner]({static}/images/wagnerblackout.jpg)

So sieht es aus. Zumindestens eine erste Version mit grundlegenden Funktionen. Eine Idee die ich schon lange mal umsetzen wollte. Ist schon ein paar Jahre her, als ich von den [Newspaper Blackout Poems](http://austinkleon.com/newspaperblackout/) las. Da werden Zeitungen, oder andere Texte, genommen und durch das schwärzen von Teilen des Artikels entstehen sowas wie "Gedichte". Soweit würde ich garnicht gehen. Lustig finde ich die Idee trotzdem. Hat viel mit Remixen und dem kreativen Umgang mit Content zu tun. Und da ich ein wenig mit [Flask](http://flask.pocoo.org/) rumspiele, habe ich mich gefragt ob ich sowas ähnliches auch hinbekomme. Ich überlasse die Auswahl der Wörter dem Pseudo-Zufallsgenerator von Python. Ich glaube ehrlich das es kein Unterschied der Qualität wäre, wenn ich das ganze machen müsste ;-). Interessanterweise sind viele Ergebnisse wirklich witzig. Das Problem was ich noch habe: Auf welche Texte lasse ich das ganze los? Am liebsten wäre mir Pseudojournalismus ala BILD. Am besten wären noch die Werke des "Betroffenheitspoeten" Franz Josef Wagner. Diese habe ich mal als Beispiel genommen. Gar nicht so schlecht Herr Pseudozufallszahlengenerator. 
