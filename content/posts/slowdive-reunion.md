Title: Slowdive Reunion
Date: 2014-05-23 13:59
Author: marvin
Category: Uncategorized
Tags: music, reunions, slowdive
Slug: slowdive-reunion

![cc-by [Greg Neate](https://secure.flickr.com/photos/neate_photos/12528849325)]({static}/images/slowdive.jpg)

Slowdive spielen gerade Reunion-Shows??? Und so sehr wünsche ich mir
eine von diesen zu besuchen. Slowdive sind für mich ja immer noch die
Könige des Dreampop/Shoegaze. Unangefochten. Hier gibt es ein Liveset
einer ihrer Shows. Ach ja...

{% youtube xIcpnY1kImA %}

