Title: Seit fast einer Woche habe ich sie nun...
Date: 2012-06-19 09:08
Author: marvin
Category: Uncategorized
Tags: music, sigurros
Slug: seit-fast-einer-woche-habe-ich-sie-nun

![Sigur_Ros_-_Valtari]({static}/images/Sigur_Ros_-_Valtari.jpg)

Seit fast einer Woche habe ich sie nun. Meine Frau hat mir vorzeitig zu
meinem Geburtstag "Valtari" von Sigur Ros, auf wunderschönem, schweren
Doppenvinyl, geschenkt. Sie ist ja schon ein paar Wochen draussen. Und
um ehrlich zu sein hatte ich ein wenig Angst davor. Das letzte Album
"Með suð í eyrum við spilum endalaust" hatte mich mal gar nicht von den
Socken gehauen. Zu sehr hinge ich an "Takk". Etwas was man fast als das
perfekte Album bezeichnen könnte. Seit fast 10 Jahren höre ich es...und
die Liebe wächst immer noch...von Lied zu Lied. "Með suð í eyrum við
spilum endalaust" ist, bis auf ein oder zwei Songs, einfach nicht in
meinem Ohr geblieben. Ok "Gobbledigook" ist ein großartiger Song. Vor
allem in der Kombination mit den Musikvideo ein Traum. Aber dann nimmt
die Platte rasant ab. Jetzt halte ich das neue Werk von Sigur Ros in der
Hand. Ich legte es auf meinen Plattenteller...

...und weg war ich. Meine Ängste waren weg. "Valtari" zog mich in seinen
Bann. Eigentlich müsste man das letzte Album vergessen und nach "Takk"
"Valtari" ansetzen. Es schwebt und wummert nur so dahin. Ohne Komma und
Absatz. Man hätte die Songs eigentlich so aneinander schneiden sollen.
Es wirkt fast sakral. Keine Versuche Popmusik zu machen. Ganz reduziert.
Teilweise hat man das Gefühl das es sich nicht mal mehr um Instrumente
handelt...es könnte auch nur Jonsi's Gesang sein. Ach ich bin so froh...

Zum neuen Album hat Sigur Ros mehrere Künstler gefragt ob sie nicht das
Album hören und dazu
[Videos](http://www.sigur-ros.co.uk/valtari/videos/) machen könnten.
Alle haben das gleiche Budget dafür. Bis jetzt sind es drei Videos die
daraus entstanden sind. Das zweite Video gefällt mir besonders. Es ist
eine animierte Postkarte aus Island von der Künstlerin Inga
Birgisdóttir.

{% youtube NUysiCX-XZQ %}

