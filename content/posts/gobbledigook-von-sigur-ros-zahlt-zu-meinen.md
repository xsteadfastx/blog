Title:  Gobbledigook von Sigur Ros zählt zu meinen...
Date: 2012-06-19 09:17
Author: marvin
Category: Uncategorized
Tags: music, sigurros
Slug: gobbledigook-von-sigur-ros-zahlt-zu-meinen

![391488]({static}/images/391488.png)

"Gobbledigook" von Sigur Ros zählt zu meinen liebsten Musikvideos. Nicht
ganz "not safe for work" aber es ist ein Traum. Und wisst ihr wieso?
Weil mein innerer Hippie gerade ganz schön aus dem Häuschen ist...

{% vimeo 3987450 %}

