Title: DIIV - Druun / Past Lives
Date: 2012-10-30 16:29
Author: marvin
Category: Uncategorized
Tags: diiv, music
Slug: diiv-druun-past-lives

![diiv]({static}/images/diiv.jpg)

Mal kurz den Ärger des Tages vergessen. Die Kälte. Den Regen. Den
anstehenden Winter. Das alles mit schmozzy, divende Musik die einen
träumerisch an den kalifornischen Strand setzt um die Sonne, das Meer
einzufangen.

{% youtube TzYwjhLx04s %}

