Title: The Velvet Underground and Nico - A Symphony of Sound
Date: 2012-09-19 12:28
Author: marvin
Category: Uncategorized
Tags: andywarhol, music, nico, velvetunderground
Slug: the-velvet-underground-and-nico-a-symphony-of-sound

![771243]({static}/images/771243.jpg)

67 Minuten improvisieren sich hier die Velvets, zusammen mit Nico, durch
das Video. Gefilmt von Andy Warhol, auf 16mm Schwarzweißfilm. Kaum
hörbar und ansehbar. Trotzdem ein wahnsinniges Footage und ich freue
mich das sowas im Internetz auftaucht. Danke...

{% youtube rCQFWu1TQWE   %}

([via](http://www.openculture.com/2012/09/ia_symphony_of_soundi_1966_the_velvet_underground_improvises_warhol_films_it_until_the_cops_turn_up.html))

