Title: If Youre Feeling Sinister
Date: 2013-02-18 16:16
Author: marvin
Category: Uncategorized
Tags: belleandsebastian, documentary, music, pitchfork
Slug: if-youre-feeling-sinister

[![cc-by-sa aurélien.]({static}/images/5781830785_48cfcec5ca_b.jpg)](https://secure.flickr.com/photos/aguichard/5781830785/)

Eigentlich sollte das Leben sich ja so anfühlen wie Belle & Sebastian
klingen.
[Pitchfork](http://pitchfork.com/news/49599-pitchforktv-presents-a-documentary-on-belle-and-sebastians-if-youre-feeling-sinister/)
hat jetzt eine Dokumentation über ihr Album "If Your're Feeling
Sinister" bei Youtube hochgeladen.

> As part of the "Pitchfork Classic" series, which explores the making
> of important albums, Pitchfork.tv today presents an hour-long
> documentary on Belle and Sebastian's 1996 album If You're Feeling
> Sinister.  
>  For the film, Pitchfork.tv interviewed every member of the band that
> played on the album. They tell the story of how it all came together,
> from Stuart Murdoch's debilitating illness, to plucking Stevie Jackson
> out of another local band, to Murdoch meeting Isobel Campbell while
> she was drunk at a party. The film features archival photos and videos
> from the band's early days.

{% youtube JaR3DK43T74 %}

