Title: Pink Floyd - Live At Pompeii
Date: 2012-07-11 13:57
Author: marvin
Category: Uncategorized
Tags: beastieboys, music, pinkfloyd, pompeii, psychedelic
Slug: pink-floyd-live-at-pompeii

![477016]({static}/images/477016.jpg)

Eine Stunde völliges psychedelisches Feuerwerk... Pink Floyd Live in dem
Amphitheater in Pompeii. Ich muss ja ehrlich zugeben das ich mich an
Pink Floyd bis jetzt nicht wirklich rangetraut war. Aber nach diesem
Video wird es jetzt wirklich Zeit...

{% youtube BNPuvOuDmKg %}

Kleiner Bonus... Die Optik des Video hat mich doch stark an das Video zu
dem Song "Gratitude" von den Beastie Boys erinnert... kurz gegoogelt...
es ist wirklich eine Hommage an Pink Floyd. War ja auch irgendwie
logisch wenn man die "Pink Floyd"-Schriftzüge auf ihren Boxen sieht.
Großartig...

{% youtube eNipQPpxsZo %}

