Title: memoryhouse - the years
Date: 2011-12-09 09:59
Author: marvin
Category: post
Tags: memoryhouse, music, subpop
Slug: memoryhouse-the-years-2

{% vimeo 28985813 %}

Die neu aufgenommene Version von der "The Years EP" gibt es nun,
wunderschön optisch untermalt, auf vimeo. Besser kann es gar nicht
werden. Ich könnte den ganzen Tag diese Platte hören. Eins der schönsten
Sachen die das Shoegaze/Dreampop Revival hervorgebracht haben.

