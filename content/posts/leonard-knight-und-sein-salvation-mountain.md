Title: Leonard Knight und sein Salvation Mountain
Date: 2013-05-02 10:41
Author: marvin
Category: Uncategorized
Tags: documentary, leonardknight, salvationmountain, vice
Slug: leonard-knight-und-sein-salvation-mountain

[![cc-by-sa Joe Decruyenaere]({static}/images/800px-Salvation_Mountain_001.jpg)](http://www.flickr.com/people/38213125@N00)

Leonard Knight fasziniert mich. Er ist einer dieser Menschen die einen
so inspirieren, dass es ganz egal ist welchen Nutzen oder Erfolg
Kreativität bringt, solange es einen selbst und seinen Sinn erfüllt.
Viele kennen den [Salvation
Mountain](https://de.wikipedia.org/wiki/Salvation_Mountain) bestimmt aus
Filmen wie "Into The Wild" oder auch von "Germany's Next
Topmodel"-Fotoshootings. Das künstlerische Lebenswerk Leonard Knights.
VICE hat eine kleine Dokumentation über ihn gemacht...

{% youtube gonpqxlLIa0 %}

