Title: Das einzig spannende an dem Slomka-Gabriel Duell
Date: 2013-12-03 10:59
Author: marvin
Category: Uncategorized
Tags: csu, politics, spd, zdf
Slug: das-einzig-spannende-an-dem-slomka-gabriel-duell

![gabrielslomka]({static}/images/gabrielslomka.jpg)

Als ob Gabriel's Empörung und Slomkas Fragen nicht schon peinlich genug
wären. Ok es war ganz nett. Man ist ja immer dankbar wenn mal etwas
anderes im GroKo-Sumpf passiert. Da geben wir uns sogar mit diesem
Interview zufrieden in dem Sigmar Gabriel und Marietta Slomka ein wenig
aneinander geraten. Mehr ist es auch nicht. In der Twittertimeline sah
dies viel spektakulärer aus, als ich es empfunden habe. Ich dachte das
Thema sei damit abgehakt und wir warten alle gemeinsam auf den Untergang
des Abendlandes. Falsch. Genau in diesem Moment meldet sich Horst
Seehofer und kritisiert die kritische Presse in einer angeblichen
kritischen Pressefreiheit. Dies ist der einzige Skandal an dieser
Geschichte. Wenn das ZDF nun staatlich unabhängig wäre. Ist es aber
nicht. Die Parteien sitzen in den Sendern und treffen
Personalentscheidungen. Da bekommt man schon ein wenig Gänsehaut. Die
ganz schlimme.

{% youtube Ow-36rH-nY8 %}

