Title: Mein Herz gehört ja der Analog Fotografie Vor...
Date: 2012-06-07 09:58
Author: marvin
Category: Uncategorized
Tags: afghanistan, photography
Slug: mein-herz-gehort-ja-der-analog-fotografie-vor

![347986]({static}/images/347986.png)

Mein Herz gehört ja der Analog-Fotografie. Vor allem auch die mit alter
oder außergewöhnlichker Technik. Heute habe ich ein Video gefunden von
einem Fotografen aus Afghanistan der mit einer selbstgebauten Kamera
Portraits schießt. Er entwickelt das Fotopapier gleich in der Kamera und
fotografiert das Negativ wiederum ab um daraus ein Positiv zu bekommen.
[Hier](http://www.afghanboxcamera.com/abcp_camera_howtobuild.htm) gibt
es eine Anleitung wie man so eine Kamera selber bauen kann.

{% vimeo 32748604   %}
([via](http://fstoppers.com/bts-video-how-to-use-an-afghan-box-camera))

