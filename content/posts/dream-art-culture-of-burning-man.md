Title: DREAM - Art & Culture of Burning Man
Date: 2013-05-23 16:49
Author: marvin
Category: Uncategorized
Tags: art, burningman, documentary
Slug: dream-art-culture-of-burning-man

[![cc by Kyle Harmon]({static}/images/4968347602_b7508b8c47_b.jpg)](https://secure.flickr.com/photos/wkharmon/4968347602/in/photostream/)

Das Burning Man Festival ist etwas was meinen inneren Hippie völlig aus
dem Häuschen sein lässt. Ein Festival in einem Teil der Wüste Nevadas
die zu einer riesigen Spielwiese für Kunst verwandelt wird. [Rich Van
Every](http://lightworkscreative.tv/) hat versucht die Atmosphäre in
einem Kurzfilm zusammen zu schneiden. Es lässt sich nur erahnen ob ihm
die fast 14 Minuten nicht gereicht haben und er deshalb einen längeren
Dokumentarfilm angekündigt hat. Ich will ihn auf jeden Fall sehen.

{% vimeo 62369954 %}

{% youtube JbSUyYXH8hs %}

([via](http://www.kaputtmutterfischwerk.de/?p=70))

