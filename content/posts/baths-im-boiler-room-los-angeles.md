Title: Baths im Boiler Room Los Angeles
Date: 2013-05-27 15:14
Author: marvin
Category: Uncategorized
Tags: baths, boilerroom, music
Slug: baths-im-boiler-room-los-angeles

![baths_boilerroom]({static}/images/baths_boilerroom.jpg)

Das Rolling Stone Magazin hat letztens über Baths geschrieben, dass es
nicht einer der bärtigen Hipster-Möchtegern-Folk-Bands ist sondern ein
Nerd mit ein wenig Übergewicht der aktuell die schönsten Songs schreibt.
Ich übertreibe vielleicht gerade ein wenig, aber das ganze spricht mir
aus dem Herzen. Und Baths hat einfach gerade wieder ein super schönes
Album draussen. Hier eine ganze Performance aus dem Boiler Room in Los
Angeles.

{% youtube nSc2mlIyuqs %}

