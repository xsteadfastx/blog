Title: Einfach wunderbar wenn ich...
Date: 2011-11-25 14:42
Author: marvin
Category: post
Tags: blackbirdblackbird, fernweh, music
Slug: blackbird-blackbird

{% vimeo 15091837 %}

Einfach wunderbar wenn ich last.fm dazu benutzt neue Bands und Musiker
aufzuspüren. Oft reicht einfach mal das Genreradio nach "chillwave"
durchsuchen zulassen und schon hat man viel neues und auch meist gutes
Material.

So wie gerade "blackbird blackbird". Wunderschöner Chillwave/Newgaze. So
wie immer erinnert es mich an die wunderbaren Frühlings und Sommertage.
Eigentlich will ich nicht viel mehr als zu tanzen und in Fernweh zu
versinken. Die Vögel in dem Video erinnern mich sehr an die Fotos aus
[Jonathan Safran
Foers](http://de.wikipedia.org/wiki/Jonathan_Safran_Foer) Buch "Extrem
laut und unglaublich nah".

Das Album gibt es für 5\$ bei
[Bandcamp](http://blackbirdblackbird.bandcamp.com/album/summer-heart).
Das werde ich gleich mal von meiner Kreditkarte abbuchen lassen. Nur so
am Rande: Bandcamp bietet seine Downloads auch in
[FLAC](http://de.wikipedia.org/wiki/Free_Lossless_Audio_Codec) an. Kann
nicht besser sein...

<iframe style="position: relative; display: block; width: 300px; height: 410px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=2655275274/size=grande3/bgcol=FFFFFF/linkcol=4285BB/" frameborder="0" width="300" height="410"></iframe>

