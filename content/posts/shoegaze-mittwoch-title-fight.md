Title: Shoegaze Mittwoch: Title Fight
Date: 2015-12-16 09:30
Slug: shoegaze-mittwoch-title-fight
Tags: titlefight, music, shoegaze

Nach dem letzten Album von Ceremony hätte ich nicht gedacht das es mehr Hardcore Bands gibt die soweit über ihren Tellerrand schauen um sich bei Bands wie Slowdive und My Bloody Valentine inspirieren zu lassen. Soundwände sind ihnen nie fremd gewesen. Alles wird noch ein wenig runtergeschraubt und in die breite gezogen. Schon auf [Floral Green](https://en.wikipedia.org/wiki/Floral_Green) gab es Tracks die das angekündigt haben. Doch auf [Hyperview](https://en.wikipedia.org/wiki/Hyperview) wird das volle Programm abgefeuert. Mehr Reverb und Distortion geht kaum.

{% youtube Tu9KgGqXDyw %}

{% youtube f89NbJOcu9A %}
