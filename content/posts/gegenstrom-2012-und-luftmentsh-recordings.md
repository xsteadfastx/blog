Title: Gegenström 2012 und Luftmentsh Recordings
Date: 2012-05-02 10:53
Author: marvin
Category: Uncategorized
Tags: art, gegenstroem, jerusalem, luftmentsh
Slug: gegenstrom-2012-und-luftmentsh-recordings

![logo_1335191546]({static}/images/logo_1335191546.gif)

Christine und ich waren am Samstag in der DESI zum [Gegenström
Festival](http://xn--gegenstrm-77a.de/). Ein kleines Festival für
experimentelle Musik und Soundinstallationen. Und ich war besonders
aufgeregt weil es das erste mal sein wird das ich etwas ausstelle und
das der Start für eine Website sein sollte: [Luftmentsh
Recordings](http://recordings.luftmentsh.org)... Eine Website mit
Fieldrecordings auf der Basis von OpenStreetMap und Soundcloud. Bis
jetzt kann man Aufnahmen hören die Christine und ich in Jerusalem
gemacht haben. Da uns das ganze soviel Spaß gemacht hat, wollen wir es
erweitern auf alle möglichen Orte an denen wir Fieldrecordings machen
wollen. Hier sind die Fotos von unserem Ausstellungsteil...

![Christine & Marvin Preuss - jerusalem-recordings]({static}/images/6985018154_dac4835095_b.jpg)

![Christine & Marvin Preuss - jerusalem-recordings]({static}/images/6985018544_af732a35ae_b.jpg)

![Christine & Marvin Preuss - jerusalem-recordings]({static}/images/7131102839_c0d91a40dc_b.jpg)

Ich habe noch ein paar andere Fotos von dem Festival gemacht.

![DESI]({static}/images/7131096439_be7e3359b7_b.jpg)

Jonas Tröger - o.T.

![Jonas Tröger - o.T.]({static}/images/6985024690_80744d0c59_b.jpg)

![Jonas Tröger - o.T.]({static}/images/7131108465_0a3dc1a05c_b.jpg)

Carmen Loch - nd2

![Carmen Loch - nd2]({static}/images/7131096619_ce14d59e04_b.jpg)

Ines Klaue / Christian Kröber - Stück für vier Hände

![Ines Klaue / Christian Kröber - Stück für vier Hände]({static}/images/7131101263_f431398b2f_b.jpg)

Sebastian Tröger - Candlelight Concert

![Sebastian Tröger - Candlelight Concert]({static}/images/6985050374_3fcfdd5f6e_b.jpg)

![Sebastian Tröger - Candlelight Concert]({static}/images/7131134269_0d66a399fc_b.jpg)

![Sebastian Tröger - Candlelight Concert]({static}/images/7131134637_4cff3a8ac0_b.jpg)

ICHIIGAI Artists - landbúnaði taminn

![ICHIIGAI Artists - landbúnaði taminn]({static}/images/6985034896_dbc234d1be_b.jpg)

![ICHIIGAI Artists - landbúnaði taminn]({static}/images/7131118823_e8164b8012_b.jpg)

![ICHIIGAI Artists - landbúnaði taminn]({static}/images/6985035624_363f837c63_b.jpg)

PIN TE RAULT

![PIN TE RAULT]({static}/images/6985031360_3fd0ace8fc_b.jpg)

![PIN TE RAULT]({static}/images/7131113827_010e67e20e_b.jpg)

![PIN TE RAULT]({static}/images/6985030994_01bbc3224d_b.jpg)

OY

![OY]({static}/images/6985029204_3b6ea12a11_b.jpg)

![OY]({static}/images/7131113251_6377b8ed8f_b.jpg)

ANTIFUN ARKESTRA

![ANTIFUN ARKESTRA]({static}/images/6985006134_7c32225888_b.jpg)

![ANTIFUN ARKESTRA]({static}/images/7131090185_acfbc7b7e1_b.jpg)

![ANTIFUN ARKESTRA]({static}/images/7131090645_0331da8422_b.jpg)

![ANTIFUN ARKESTRA]({static}/images/6985007614_3c3915a073_b.jpg)

