Title: Memoryhouse - UNTITLED
Date: 2013-01-24 16:52
Author: marvin
Category: Uncategorized
Tags: memoryhouse, music
Slug: memoryhouse-untitled

![untitledmemoryhouse]({static}/images/untitledmemoryhouse.jpg)

Memoryhouse haben als kleines Dankeschön für das letzte Jahr ein
Musikvideo veröffentlicht. Es untermalt einen, bis jetzt, unreleasten
Track. Vor ein paar Monaten wurde in den Tour-Van der Band eingebrochen
und die Musikinstrumente geklaut. Was für viele kleine Indie-Musiker
fast das Ende bedeuten kann. Oder wer hat schon mehrere tausend Euro auf
der Kante für neue Instrumente? Sie baten damals um Spenden um sich
wieder der Musik hingeben zu können. Dies soll ihr Dank für den Support
sein.

Das Memoryhouse Konzert letztes Jahr in München war eins meiner
Highlights. Absolut.

{% vimeo 57957774 %}

([via](http://www.memoryhou.se/day/2013/01/22))

