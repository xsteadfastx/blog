Title: Best Coast - The Only Place
Date: 2012-06-26 12:39
Author: marvin
Category: Uncategorized
Tags: bestcoast, california, fernweh, music
Slug: best-coast-the-only-place

![419276]({static}/images/419276.png)

Kalifornien und mich verbindet nicht nur der Teil meiner Familie der
dort lebt. Ich war 2002 dort. Drei Wochen haben wir unsere Familie
besucht und ich hatte mich sofort verliebt. Mit 17 ging mein Leben nicht
wirklich voran...Schule fand ich scheiße. Nachdem ich die kalifornische
Sonne und den Wind des Meeres in meinem Gesicht gespürt habe wollte ich
genau dorthin. Ich wollte auswandern. Ich liebte alles dort. Die Palmen,
die Menschen, die Wellen. Seit dem prangte eine California Flagge über
meinem Bett. In meinem Kopf malte ich mir aus das mein Leben wohl viel
besser ausgehen hätte wäre ich doch in Kalifornien groß geworden. Ob das
stimmt weiß ich nicht...

Und sehe ich mir das neue Video von Best Coast an falle ich in ein Meer
aus Fernweh...

{% youtube AJW00gx4wvE %}

