Title: Es gibt nicht viel gute Live Aufnahmen von...
Date: 2012-05-11 07:25
Author: marvin
Category: Uncategorized
Tags: music, mybloodyvalentine
Slug: es-gibt-nicht-viel-gute-live-aufnahmen-von

Es gibt nicht viel gute Live-Aufnahmen von My Bloody Valentine im
Internetz. Vielleicht liegt es an der unfassbaren Lautstärke auf ihren
Konzerten. Heute habe ich Aufnahmen von 1989 auf Youtube gefunden.
Großartig! Neben dieser Soundwand feier ich am meisten das Schlagzeug
ab. Was ein unfassbarer Sound...

{% youtube OWhvyvt347k %}

{% youtube 2mwe22CK2U8 %}

{% youtube iR1Au_ozhBI %}

{% youtube 0UimWjOmx2E %}

