Title: Jack White und Vinyl-Pr0n
Date: 2014-05-15 13:39
Author: marvin
Category: Uncategorized
Tags: jackwhite, music, vinyl
Slug: jack-white-und-vinyl-pr0n

Das neue Album von Jack White heißt "Lazaretto". Und die Vinyl Version
ist vielleicht die aufwendigste Platte aller Zeiten. Er hat einen Haufen
Gimmicks eingebaut. Von sich drehenden Hologrammen, Hiddentracks bis hin
zu zwei verschiedenen Intros zu einem Track (durch zwei verschiedene
Einstiegsrillen). Die Liebe zu Vinyl sieht man Jack White immer wieder
an. Nicht zuletzt durch sein Label [Third Man
Records](https://en.wikipedia.org/wiki/Third_Man_Records). Ich erinnere
mich daran wie er in einem Interview erzählte wie er die Singles noch
selber mit eintütet.

{% vimeo 95280913 %}

