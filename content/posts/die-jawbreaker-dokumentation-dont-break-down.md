Title: Die Jawbreaker Dokumentation "Dont break down"
Date: 2017-07-13 11:02
Slug: die-jawbreaker-dokumentation-dont-break-down
Tags: music, documentary, jawbreaker

![dont break down]({static}/images/jawbreaker-dont-break-down.jpg)

Es kommt wohl bald eine Dokumentation zu Jawbreaker. Eine Band die ich immer wieder und sehr gerne höre. Sie spielen ja bald ein paar Reunion Shows. Na mal schauen...

{% vimeo 223189941 %}

([via](http://pitchfork.com/news/jawbreaker-documentary-dont-break-down-gets-new-trailer-premiere-date-watch/))
