Title: Fotografie Prozesse
Date: 2012-08-08 13:25
Author: marvin
Category: Uncategorized
Tags: photography
Slug: fotografie-prozesse

![588434]({static}/images/588434.jpg)

Das [George Eastman House](http://www.eastmanhouse.org/) hat eine
6-teilige Minidokumentation über die verschiedenen Fotoprozesse gemacht.
Eigentlich will ich alle ausprobieren... wenn ich mich nur besser in
Chemie auskennen würde...

[Daguerreotypie](http://de.wikipedia.org/wiki/Daguerreotypie)  
{% youtube cmm90yhhuJM %}

[Kollodium-Nassplatte](http://de.wikipedia.org/wiki/Kollodium-Nassplatte)  
{% youtube --PAAJZRbn8 %}

[Albuminpapier](http://de.wikipedia.org/wiki/Albuminpapier)  
{% youtube Cq1RvahEPSk %}

[Woodburytypie](http://de.wikipedia.org/wiki/Woodburytypie)  
{% youtube yWAE2uBLRz4 %}

[Platindruck](http://de.wikipedia.org/wiki/Platindruck)  
{% youtube 6DFZ3wfft44 %}

[Trockenes
Gelatineverfahren](http://de.wikipedia.org/wiki/Trockenes_Gelatineverfahren)  
{% youtube iSBFrPWPS80 %}

([via](http://www.petapixel.com/2012/08/06/fascinating-videos-about-6-photographic-processes-used-through-history/))

