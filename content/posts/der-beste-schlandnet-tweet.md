Title: Der beste #schlandnet-Tweet
Date: 2013-11-20 12:35
Author: marvin
Category: Uncategorized
Tags: internetz, netzpolitik, politics, twitter
Slug: der-beste-schlandnet-tweet

Danke [tante](https://twitter.com/tante) für einen Mund ausgeprusteter
Cola-Light auf dem Schreibtisch. Ein
[Tweet](https://twitter.com/tante/statuses/399830284671385600) der die
traurige netzpolitische Realität in Deutschland wiederspiegelt. Danke.

![tante_schlandnet]({static}/images/tante_schlandnet.png)

