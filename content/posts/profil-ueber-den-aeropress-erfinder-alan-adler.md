Title: Profil über den Aeropress Erfinder Alan Adler
Date: 2015-09-02 11:20
Slug: profil-ueber-den-aeropress-erfinder-alan-adler
Tags: aeropress, coffee
Description: Kurze Dokumentation über Alan Adler

Wieviel Zeit ich schon verbracht habe um Aeropress Videos auf Youtube zu schauen. Es ist immer noch meine liebste Art Kaffee zu zubereiten. Vor allem gibt es endlose Möglichkeiten und Methoden für den Brühprozess. Hier eine kleine Dokumentation über den Erfinder Alan Adler. Danke Mr. Adler <3.

{% vimeo 137090060 %}
([via](http://worldaeropresschampionship.com/2015/09/02/inventor-portrait-alan-adler/))
