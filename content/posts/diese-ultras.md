Title: Diese Ultras
Date: 2013-04-09 12:35
Author: marvin
Category: Uncategorized
Tags: documentary, football, sky
Slug: diese-ultras

[![cc-by MJ Who]({static}/images/800px-NapoliUltras1.jpg)](https://commons.wikimedia.org/wiki/File:NapoliUltras1.jpg)

Damit konnte diesmal wirklich niemand rechnen. Eine sehr gute
Dokumentation über Ultras in Deutschland. Und das auf dem Sender Sky.
Äußerst reflektiert und ruhig. Ich muss immer noch extrems facepalmieren
wenn Kerner den Bengalo an die Kinderpuppe hält. Wo war da eigentlich
das Gremium des deutschen Fernsehpreises wenn man es mal braucht?

{% youtube x74tpuVBqoY %}

Ein guter Artikel der 11 Freunde zu der Dokumentation findet man
[hier](http://www.11freunde.de/artikel/sky-sendet-fussball-ultras-und-ueberrascht-positiv).

