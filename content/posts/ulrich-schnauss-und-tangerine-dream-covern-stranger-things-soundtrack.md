Title: Ulrich Schnauss und Tangerine Dream covern Stranger Things Soundtrack
Date: 2016-09-13 09:57
Slug: ulrich-schnauss-und-tangerine-dream-covern-stranger-things-soundtrack
Tags: ulrichschnauss, tangerinedream, strangerthings

Ich vertrete mal eine unpopuläre Meinung. Ich mochte Stranger Things eigentlich fast nur wegen dem Soundtrack. Inhaltlich hat es mich nicht so überzeugt, auch wenn ich die jungen Schauspieler wirklich gut fand. Geschaut habe ich die Serie dann doch bis zum Ende. Man hofft das es noch irgendeinen Twist gibt der einen doch umhaut.

Fast ging es an mir vorbei: Ulrich Schnauss ist ja jetzt festes Mitglied bei Tangerine Dream. Und die haben nun zusammen einen Coversong eines Tracks aus Stranger Things aufgenommen. So gut der richtige Soundtrack ist, ich glaube ich würde sofort Ulrich Schnauss nehmen um den Soundtrack zu schreiben. Egal für welchen Film...

**Update:** Scheint nicht mehr online zu sein... meh.
