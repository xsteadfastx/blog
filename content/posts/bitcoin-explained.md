Title: Bitcoin Explained
Date: 2013-04-09 09:57
Author: marvin
Category: Uncategorized
Tags: bitcoin, internetz, money
Slug: bitcoin-explained

![Bitcoin]({static}/images/Bitcoin.png)

Gerade ist ein [Bitcoin](https://de.wikipedia.org/wiki/Bitcoin) ca.
\$193 [wert](https://mtgox.com/). Zwischenzeitlich sah es schon mal
richtig finster aus. Ich erinnere mich wie das Thema Bitcoin vor ca. 2
Jahren hoch kam und relativ schnell wieder aus meinerm Sichtfeld
verschwand. Nun legt ja mal der Euro mal wieder die Ohren an und Bitcoin
erlebt, vielleicht auch deswegen, einen Hochflug. Eine Währung die unter
keiner Regierung steht und über das Internetz dezentral funktioniert.
Sind es die Heuschrecken die den Preis der Bitcoins nach oben treibt?
Duncan Elms hat zu Bitcoin einen kleinen Film gemacht, der die Technik
dahinter versucht zu erklären.

{% vimeo 63502573 %}

