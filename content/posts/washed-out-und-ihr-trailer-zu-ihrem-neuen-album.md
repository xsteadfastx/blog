Title: Washed Out und ihr Trailer zu ihrem neuen Album
Date: 2013-05-28 09:46
Author: marvin
Category: Uncategorized
Tags: music, washedout
Slug: washed-out-und-ihr-trailer-zu-ihrem-neuen-album

![washedout_paracosm]({static}/images/washedout_paracosm.jpg)

Das kommende Werk, mit dem Namen Paracosm, hat nun einen tollen Trailer
bekommen. Alles was fehlt... das passende Wetter.

{% youtube 3KhHHp5L5QQ %}

