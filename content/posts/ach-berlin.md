Title: Ach Berlin
Date: 2015-10-26 14:41
Slug: ach-berlin
Tags: berlin, photography

![IMG_20151024_192201]({static}/images/21864572164_2c1cd05f74_b.jpg)

Es war nur ein kleiner Wochendendurlaub der sehr nötig war. Ich sage nur eins: Die [Boticelli Ausstellung](http://www.botticelli-renaissance.de/) in der Gemäldegalerie ist großartig.

![IMG_20151024_145523]({static}/images/22498305781_c4ed4baf94_b.jpg)

![IMG_20151023_200853]({static}/images/22300462159_b8a007824a_b.jpg)

![IMG_20151024_192009]({static}/images/22461280076_6e8de456a3_b.jpg)

![IMG_20151024_192131]({static}/images/22498299721_164cd6ffd0_b.jpg)

![IMG_20151026_065107]({static}/images/22461288946_66101d9ca4_b.jpg)
