Title: New York in Wasserfarben
Date: 2012-03-22 10:41
Author: marvin
Category: post
Tags: art, newyork, openstreetmap
Slug: new-york-in-wasserfarben

![newyork]({static}/images/newyork.png)

Wie schön ist das [gemalte New
York](http://maps.stamen.com/watercolor/#12/40.7917/-73.9118)?

Langsam wird vielen Leuten klar was für tolle Sachen man mit freien
Kartendaten machen kann. Die Zivilisationswege und Landschaften als
Kunst. Danke OpenStreetMap das du das alles möglich gemacht hast. Hier
waren die Jungs von [Stamen.com](http://maps.stamen.com/) am Werk. Sie
bieten einige sehr schöne Karten an die aus den freien OSM-Daten erzeugt
wurden sind. Unter anderen auch die "Watercolor Map". Ein Traum. Das
erinnert mich daran das ich mal wieder was für OpenStreetMap machen
möchte. Lange ist es her als ich mit meinem GPS durch Wolfsburg gelaufen
bin. Wird mal wieder Zeit...

