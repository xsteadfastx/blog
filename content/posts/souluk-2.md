Title: souluk
Date: 2011-08-05 09:42
Author: marvin
Category: post
Tags: holga, photography, rodinal, souluk
Slug: souluk-2

my uncle writes beautiful german pop songs. he asked me to do some
pictures for his new album. this is the first roll i developed so far.
it was a really fast shooting...i didnt had much time. it was the day i
moved away from wolfsburg. so it had to be very quickly and i just took
a few of my cameras and made a few portraits of him.

![Souluk]({static}/images/6001589500_50dbe4ffe5_b.jpg)

![Souluk]({static}/images/6001589396_cd65351345_b.jpg)

![Souluk]({static}/images/6001589224_b68c54823f_b.jpg)

![Souluk]({static}/images/6001589068_0029dea55e_b.jpg)

![Souluk]({static}/images/6001588934_f0a4d24216_b.jpg)

