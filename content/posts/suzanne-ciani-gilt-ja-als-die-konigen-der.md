Title: Suzanne Ciani gilt ja als die Königen der...
Date: 2012-05-14 14:05
Author: marvin
Category: Uncategorized
Tags: davidletterman, music, suzanneciani
Slug: suzanne-ciani-gilt-ja-als-die-konigen-der

[Suzanne Ciani](http://en.wikipedia.org/wiki/Suzanne_Ciani) gilt ja als
die Königen der Synthesizer. Sie hat für viele Filme und Musiker
gearbeitet. Unter anderem hat sie auch an Audio-Logos gearbeitet. Sowas
wie der Sound einer Cola Flasche die man öffnet.

Hier ist ein Clip in dem sie zu Gast bei David Letterman ist und ein
wenig über ihre Kunst spricht.

{% youtube fZscRHkLMt0 %}

