Title: Kunst lieben, Kunst hassen
Date: 2015-04-20 11:24
Tags: art, arte, documentary
Slug: kunst-lieben-kunst-hassen


Mal ein Geheimtipp für jeden mit der kleinen Hassliebe Kunst und eine Liebe für die dazugehörige Absurdität des Kunstmarktes. Mir kommt es vor wie ein riesiger Spiegel der Kunstszene. Nicole Zepter arbeitet sich schnippisch durch Galerien, Museen und Kunstmessen. Ziemlichn erfrischend das Ganze. Vor allem weil man am Ende doch Kunst liebt und der Diskurs über den Markt und schlechte Ausstellungen einfach dazu gehört.

Ach Kunst... ich liebe dir...

<iframe frameborder="0" allowfullscreen="true" src="http://www.arte.tv/guide/de/embed/052786-001/medium" style="width: 600px; height: 340px;"></iframe>

<iframe frameborder="0" allowfullscreen="true" src="http://www.arte.tv/guide/de/embed/052786-002/medium" style="width: 600px; height: 340px;"></iframe>

<iframe frameborder="0" allowfullscreen="true" src="http://www.arte.tv/guide/de/embed/052786-003/medium" style="width: 600px; height: 340px;"></iframe>
