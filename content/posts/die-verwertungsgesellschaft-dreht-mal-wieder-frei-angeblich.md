Title: Die  Verwertungsgesellschaft dreht mal wieder frei Angeblich...
Date: 2012-02-28 14:19
Author: marvin
Category: status
Tags: animals, gema, salat, youtube
Slug: die-verwertungsgesellschaft-dreht-mal-wieder-frei-angeblich

Die "Verwertungsgesellschaft" dreht mal wieder frei. Angeblich sei
Vogelgezwitscher eine Urheberrechtsverletzung. Zumindest wird das nun
bei einem Video über wilden Salat auf Youtube
[beanstandet](http://goo.gl/6vnHa). Zum Glück demontieren sie sich immer
wieder selbst...

{% youtube nPBlfeuZuWg %}

