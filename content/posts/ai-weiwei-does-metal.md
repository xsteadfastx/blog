Title: Ai Weiwei does Metal
Date: 2013-05-23 11:58
Author: marvin
Category: Uncategorized
Tags: aiweiwei, art, josephbeuys, music
Slug: ai-weiwei-does-metal

![aiweiwei_dumbass]({static}/images/aiweiwei_dumbass.jpg)

Ich war ja gespannt wie ein Flitzebogen wie nun das Metalalbum Ai
Weiwei's klingen würde. Wie ernst wird er "Metal" nehmen. Dies ist ja
neben Mallorca-Schenkelklopf-Hymnen das Musikgenre das sich am
ernstesten nimmt. Ai Weiwei hat sich kaum über seinen Inhaftierung 2011
geäußert. Angeblich darf er über Details nicht sprechen. Was macht man
dann? Ein Metalalbum aufnehmen. Erster Eindruck? Kaum Anhörbar. Ist auch
egal.

> When you're ready to strike, he mumbles about non-violence.  
>  When you pinch his ear, he says it's no cure for diarrhea.  
>  You say you're a mother-fucker, he claims he's invincible.  
>  You say you're a mother-fucker, he claims he's invincible.  
>  Fuck forgiveness, tolerance be damned, to hell with manners, the
> low-life's invincible.  
>  Fuck forgiveness, tolerance be damned, to hell with manners, the
> low-life's invincible.  
>  Oh dumbass, oh such dumbass! Oh dumbass, oh such dumbass!  
>  Oh dumbass, oh such dumbass! Oh dumbass, oh such dumbass!  
>  Lalalalala, lalalalala Lalalalala, lalalalala  
>  Lalalalala, lalalalala Lalalalala, lalalalala
>
> Stand on the frontline like a dumbass, in a country that puts out like
> a hooker.  
>  The field's full of fuckers, dumbasses are everywhere.  
>  The field's full of fuckers, dumbasses are everywhere.  
>  Fuck forgiveness, tolerance be damned, to hell with manners, the
> low-life's invincible.  
>  You say you're a mother-fucker, he claims he's invincible.  
>  You say you're a mother-fucker, he claims he's invincible.  
>  The field is full of fuckers, dumbasses are everywhere.  
>  The field's full of fuckers, dumbasses are everywhere.

{% youtube 4ACj86DKfWs %}

Künstler nehmen relativ schlechte politische Songs auf? Da war doch
was... Ich bleibe trotzdem Fan.

{% youtube DQ1_ALxGbGk %}

