Title: Mit einem Adler über Paris fliegen
Date: 2014-11-20 11:47
Author: marvin
Category: Uncategorized
Tags: animals, eagles, paris
Slug: mit-einem-adler-uber-paris-fliegen

Und das alles durch eine GoPro-Kamera. Jeden Tag wächst mein Fernweh
nach Paris. Und dann auf einmal wird ein Video hochgeladen in dem ein
Adler über diese wunderbare Stadt fliegt. Und zwar genau über die Stelle
in der wir stundenlang im Gras lagen... ach ja...

{% youtube QwNc0SFIp-o   %}

([via](http://laughingsquid.com/a-white-tailed-eagle-wearing-a-camera-records-a-breathtaking-111-mph-flight-over-paris/))

