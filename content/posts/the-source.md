Title: the SOURCE
Date: 2012-09-24 16:22
Author: marvin
Category: Uncategorized
Tags: biennale, creativity, dougaitken, jackblack, liverpool, mikekelley, tildaswinton
Slug: the-source

![thesourcedemand]({static}/images/thesourcedemand.jpg)

Die Quelle der Kreativität. Woher kommt sie? Wie entsteht sie? [Doug
Aitken](http://en.wikipedia.org/wiki/Doug_Aitken) Installation "the
SOURCE" geht diesen Fragen auf den Grund. Zumindestens befragt er
Künstler, jeglichen Mediums, dazu. Ob Jack White, Mike Kelley, Tilda
Swinton oder auch Thomas Demand. Das ganze wird gerade auf der Liverpool
Biennale ausgestellt. Würde ich super gerne sehen. Hier der Trailer.

{% youtube DxoL2Xnp-9U %}

Ich frage mich in letzter Zeit wo sich diese besagte Quelle für mich
befindet. Vielleicht werde ich es irgendwann rausfinden.

