Title: Die Mutter aller Archive
Date: 2013-02-18 13:43
Author: marvin
Category: Uncategorized
Tags: archiveorg, documentary, internetz
Slug: die-mutter-aller-archive

![archiveorg]({static}/images/archiveorg.jpg)

Es ist der erste Teil einer Dokumentation über
[Archive.org](http://archive.org) erschienen. Archive.org hat es sich
zum Auftrag gemacht das größte digitale Archive zu werden. Zu werden?
Ich glaube man kann wirklich sagen das es das längst ist. Ich könnte
tagelang im Archiv stöbern. Von alten Public-Domain Spielfilmen bis hin
zu Bootleg-Aufnahmen von Death Cab For Cutie und Elliott Smith. Die
Schönheit von Daten bekommt etwas mythologisches wenn man die
Server-Racks in einer umgebauten Kirche sieht. Eine Kathedrale des
Wissens. Gänsehaut überall...

> Archive is a documentary focused on the future of long-term digital
> storage, the history of the Internet and attempts to preserve its
> contents on a massive scale.
>
> Part one features Brewster Kahle, founder of the Internet Archive and
> his colleagues Robert Miller, director of books, and Alexis Rossi,
> director of web collections. On a mission to create universal access
> to all knowledge, the Internet Archive’s staff have built the world's
> largest online library, offering 10 petabytes of archived websites,
> books, movies, music, and television broadcasts.
>
> The video includes a tour of the Internet Archive’s headquarters in
> San Francisco, the book scanning center, and the book storage
> facilities in Richmond, California.

<iframe src="http://archive.org/embed/archive_documentary_internet_archive_sequence" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

([via](http://www.crackajack.de/2013/02/15/the-internet-archive-documentary/))

