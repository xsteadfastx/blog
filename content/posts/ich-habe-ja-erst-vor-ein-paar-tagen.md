Title: Ich habe ja erst vor ein paar Tagen...
Date: 2012-04-03 09:33
Author: marvin
Category: status
Tags: books
Slug: ich-habe-ja-erst-vor-ein-paar-tagen

![tumblr_l9shpniZBR1qa2txho1_500]({static}/images/tumblr_l9shpniZBR1qa2txho1_500.jpg)

Ich habe ja erst vor ein paar Tagen über die [Little Free
Library's](http://xsteadfastx.org/2012/03/30/little-free-library/)
geschrieben. Hier ist ein besonders schönes Exemplar...ach ja... und
dieser "Baum" steht sogar in Berlin. Also das nächste mal wenn ich dort
bin muss ich mir das mal live anschauen gehen.

{% youtube VI7h5fBHPfg %}

