Title: First Person Kollodium-Nassplatte
Date: 2013-09-24 10:52
Author: marvin
Category: Uncategorized
Tags: ianruther, photography
Slug: first-person-kollodium-nassplatte

Fotoentwicklungsprozesse beruhigen mich. Ich liebe die Tage an denen ich
in unsere Küche stehe und einen Film entwickeln kann. Der Prozess an
sich macht es so besonders für mich. Eine zweite, in sich verschlungene,
Obsession der Fotografie. Hier ist ein Video des Prozesses der
Kollodium-Nassplatten-Fotografie. Etwas was ich so gerne mal
ausprobieren würde.

{% vimeo 69916783 %}

Als kleine Zugabe gibt es ein Video von Ian Ruther, der super schöne
Kollodium-Nassplatten-Fotos macht. Hier erklärt er wie er eine Holga
umgebaut hat, um mit ihr und seinem Verfahren zu fotografieren. Jetzt
bin ich ja heiß...

{% vimeo 55837466 %}

