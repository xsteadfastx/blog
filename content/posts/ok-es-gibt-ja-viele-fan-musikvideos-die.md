Title: Ok es gibt ja viele Fan Musikvideos Die...
Date: 2012-02-15 14:10
Author: marvin
Category: status
Tags: billmurray, lostintranslation, m83, music, scarlettjohansson, sofiacoppola
Slug: ok-es-gibt-ja-viele-fan-musikvideos-die

Ok, es gibt ja viele Fan-Musikvideos. Die meisten sind auch wirklich
schäbig wie die Nacht und es überhaupt nicht wert Speicherplatz bei
Youtube zu vernichten. Aber ab und zu kommen kleine Meisterwerke bei
raus. Ich frage mich eigentlich wieso M83 nichts zum Soundtrack von Lost
in Translation beigetragen haben. In diesem Video hier passt alles ganz
wunderbar zusammen. Entschuldigt mich mal...ich geh mich weg-träumen...

{% youtube mGxCfIpWmEA %}

