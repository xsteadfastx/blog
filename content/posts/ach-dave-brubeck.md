Title: Ach Dave Brubeck
Date: 2012-12-06 13:28
Author: marvin
Category: Uncategorized
Tags: davebrubeck, joemorello, music
Slug: ach-dave-brubeck

[![c by Carl van Vechten]({static}/images/Dave_Brubeck_1954.jpg)](http://de.wikipedia.org/w/index.php?title=Datei:Dave_Brubeck_1954.jpg)

Schon als [Joe Morell](http://de.wikipedia.org/wiki/Joe_Morello)o
letztes Jahr gestorben ist, musste ich erstmal schlucken. Doch nun hat
es den Chef des Ensembles dahin gerafft. Dave Brubeck mit 92 Jahren.
Danke für dein Werk!

{% youtube PQLMFNC2Awo %}

Auch nach tausendmal hören, immer noch Gänsehaut.

