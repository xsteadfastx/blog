Title: Memoryhouse - The Kids Were Wrong
Date: 2012-02-28 17:04
Author: marvin
Category: post
Tags: memoryhouse, music, subpop
Slug: memoryhouse-the-kids-were-wrong-2

Ich sage sowas oft...aber kein Album habe ich die letzten Monate so
erwartet wie das neue Album von Memoryhouse. Die ersten Vorabsongs waren
schon purer Zucker. Nun haben sie ein Video für die erste Single "The
Kids Were Wrong" produziert. Hach, was kann ich da nur zu sagen? Die
Vinyl Vorbestellung ist eh schon raus...jetzt kann ich nur mit zittrigen
Händen auf dieses Werk warten...vielleicht vertreibe ich mir die Zeit
mit etwas Tagträumen...

{% youtube zs3xVfCG0Hg %}

