Title: Intelligenz ist die Fähigkeit seine Umgebung zu akzeptieren
Date: 2012-03-02 08:56
Author: marvin
Category: quote
Tags: williamfaulkner
Slug: intelligenz-ist-die-fahigkeit-seine-umgebung-zu-akzeptieren

Intelligenz ist die Fähigkeit, seine Umgebung zu akzeptieren.

<cite>William Faulkner</cite>

