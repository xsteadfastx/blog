Title: Ich muss ehrlich sagen das mir der Oscar...
Date: 2012-02-28 10:15
Author: marvin
Category: status
Tags: elliottsmith, movies, music, oscars
Slug: ich-muss-ehrlich-sagen-das-mir-der-oscar

Ich muss ehrlich sagen das mir der Oscar relativ am Arsch vorbei geht.
Meistens dreht sich alles um schlechte Hollywood Filme und aufgeblasene
Stars. Es gewinnen eh immer die Filme über die im Vorraus am meisten
Berichtet werden. Und das denke ich mir noch nicht einmal aus. Aber es
gibt eine Oscar Performance die mich bis heute fast zu Tränen rührt:
Elliott Smith und sein Soundtrack zu Good Will Hunting. Der Legende nach
hat er um einen Stuhl gebeten damit er sich während des Auftritts
hinsetzen kann. Das wurde ihm verweigert. Es ist so als ob den Gästen
ihr aufgesetzter Glamour wie ein Spiegel vor die Augen gehalten wird.
Übrigens hat er gegen "My Heart Will Go On" verloren. Tragischer hätte
das ganze nicht sein können...

{% youtube _2IcoVQO7yo %}

