Title: Yosemite Range of Light
Date: 2012-09-24 10:50
Author: marvin
Category: Uncategorized
Tags: fernweh, timelapse, yosemite
Slug: yosemite-range-of-light

![yosemite]({static}/images/yosemite.jpg)

Es ist Montag. Sehen wir uns nicht alle, gerade an diesem Tag der Woche,
nach einem Ausweg? Eine Flucht. Und das wirklich schlimme ist, es gibt
so schöne, wunderbare Orte auf dieser Welt. Dieses Timelapse Video von
Shawn Reeder zeigt den Yosemite Park. Zwei Jahre hat er für die
Aufnahmen gebraucht. Und genau diese Aufnahmen versüssen mir den Montag.
Ein persöhnlicher Rat von mir: Panflöten-Musik auf lautlos und Sigur Ros
dafür anmachen. Bitte!

{% vimeo 40802206 %}

