Title: Skateboarden auf Sealand
Date: 2012-07-11 09:08
Author: marvin
Category: Uncategorized
Tags: sealand, skateboard
Slug: skateboarden-auf-sealand

![475837]({static}/images/475837.jpg)

Ich habe ja schonmal über Sealand gebloggt. Ein kleiner selbsternannter
Staat der auf einer alten, schwimmenden Militärplatform vor England.
Anscheinend eignet sich diese kleine Nation ganz wunderbar um zu
skaten...yeah

{% youtube 7pHwkbDx_34   %}

([via](http://www.doobybrain.com/2012/07/11/skating-sealand-the-worlds-smallest-country/))

