Title: Robert Smith remixed Vapour Trail von Ride
Date: 2015-10-06 15:51
Slug: robert-smith-remixed-vapour-trail-von-ride
Tags: robertsmith, thecure, ride, music
Description: Kleiner Remix von Robert Smith

Es gibt ja schon lange keine Neuigkeiten von The Cure. Doch nun hat Robert Smith den Song "Vapour Trail" von Ride geremixt. Super viel verändert hat er nicht. Ein wenig an der Echo Schraube gedreht. Als jemand der das Original schätzt, zereisst es mir nicht das Herz.

{% soundcloud https://soundcloud.com/ridemusic/ride-vapour-trail-trail-mix-mixed-by-robert-smith-2015 %}

([via](http://www.stereogum.com/1835516/ride-vapour-trail-robert-smith-remix/mp3s/))
