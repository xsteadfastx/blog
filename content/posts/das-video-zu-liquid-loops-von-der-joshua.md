Title: Das Video zu Liquid Loops von der Joshua...
Date: 2012-05-08 13:27
Author: marvin
Category: Uncategorized
Tags: joshualightshow, psychedelic
Slug: das-video-zu-liquid-loops-von-der-joshua

Das Video zu Liquid Loops von der Joshua Light Show habe ich ja schonmal
[gepostet](http://xsteadfastx.org/2012/02/23/joshua-light-show/). Was
mir gerade bei meiner Recherche aufgefallen ist: Per
[Kickstarter](http://www.kickstarter.com/projects/joshualightshow/joshua-light-show-liquid-loops-ii)
wurde ein zweites Liquid Loops finanziert.

<iframe frameborder="0" height="360px" src="http://www.kickstarter.com/projects/joshualightshow/joshua-light-show-liquid-loops-ii/widget/video.html" width="480px"></iframe>

Das ganze scheint schon abgedreht zu sein. Es gibt zu mindestens ein
Making-Of auf Youtube...

{% youtube 8rYAf9aeG2I %}

