Title: Thees Uhlmann - Zum Laichen und Sterben ziehen die Lachse den Fluss hinauf
Date: 2011-08-26 11:19
Author: marvin
Category: post
Tags: music, theesuhlmann, tomte
Slug: thees-uhlmann-zum-laichen-und-sterben-ziehen-die-lachse-den-fluss-hinauf

Was ein wunderschöner Song! Ich mochte Thees Uhlmann ja schon immer. Ich
bin kein Fan von Musik in deutscher Sprache aber seine Texte und
Stimmung zieht mich immer in ein emotionales Tief. Ich liebe diese
leichte Melancholie die seine Songs umgibt. Schon bei Tomte riss mich
ein Lied nach dem anderen vom Hocker. Seine neue Solo-Single ist einfach
unfassbar schön...dazu dieses Video nachdem man sich wirklich darum
kümmert wieviel älter man jetzt doch schon ist als man sich fühlt. Ach
ja...Thees...Danke!

{% youtube GwwaYX1oG6g %}

