Title: Say Hello
Date: 2013-12-11 10:20
Author: marvin
Category: Uncategorized
Tags: allegrarosenberg, amandapalmer, doctorwho, music, neilgaiman, youtube
Slug: say-hello

![a2896267751_10]({static}/images/a2896267751_10.jpg)

Und dann kam der Zeitpunkt als Amanda Palmer das Lied covert welches ihr
Ehemann Neil Gaiman zu der Doctor Who Episode "[The Doctors
Wife](https://en.wikipedia.org/wiki/The_Doctor%27s_Wife)" inspirierte.
Das Lied stammt von [Allegra
Rosenberg](https://www.youtube.com/user/stopitsgingertime), die selbst
geschriebene Doctor Who Songs auf der Ukulele schreibt und auf Youtube
veröffentlicht. Wie toll es ist, dass Songs von Fans Neil Gaiman
inspirieren und dies dadurch zurück in die Serie fließt. Und wenn das
dann noch von Amanda Palmer gecovert wird...

{% youtube -Z5hlC5HQ2o %}

{% youtube CbGn6rOVdh4 %}

<iframe style="border: 0; width: 100%; height: 120px;" src="http://bandcamp.com/EmbeddedPlayer/album=283921073/size=medium/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless>[Say
Hello EP by Allegra
Rosenberg](http://gingertime.bandcamp.com/album/say-hello-ep)</iframe>

