Title: Brian Eno und Kevin Shields
Date: 2018-03-07 08:58
Slug: brian-eno-und-kevin-shields
Tags: music, kevinshields, mybloodyvalentine, brianeno

Wie konnte ich diesen Track nicht entdecken? Es muss einfach hier in dieses Blog. Die beiden Meister gemeinsam. Diese Gitarrendecke über dieser minimalistischen Elektronik. Schaue ich es dem Fenster, sehe ich einen dichten Nebel aus Schnee.

{% soundcloud https://soundcloud.com/adultswimsingles/brian-eno-with-kevin-shields %}
