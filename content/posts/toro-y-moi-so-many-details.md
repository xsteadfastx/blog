Title: Toro Y Moi - So Many Details
Date: 2012-10-22 16:51
Author: marvin
Category: Uncategorized
Tags: music, toroymoi
Slug: toro-y-moi-so-many-details

![artworks-000031988769-3swwzd-original]({static}/images/artworks-000031988769-3swwzd-original.jpg)

Und nebenbei fällt einem auf das Toro Y Moi eine Track von seinem neuen
Album "Anything In Return" auf Soundcloud hochgeladen hat. Man wundert
sich wo der Tag hin ist an dem man das erste mal seine Songs gehört hat,
sich seine Kopfhörer aufgesetzt hat um ganz tief einzutauchen. Der Sound
hat sich schon sehr verändert...wird es mir gefallen? Weiß ich
eigentlich noch nicht...

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F63051189&amp;show_artwork=true"></iframe>

