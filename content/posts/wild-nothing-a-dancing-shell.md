Title: Wild Nothing - A Dancing Shell
Date: 2013-05-03 09:34
Author: marvin
Category: Uncategorized
Tags: music, wildnothing
Slug: wild-nothing-a-dancing-shell

![wildnothing-adancingshell]({static}/images/wildnothing-adancingshell.jpg)

Ich feiere diesen Track wirklich ab. Wild Nothing verschmilzt immer mehr
zu einem großartigsten Dreampop-Chill-New-Wave Cocktail. Passend dazu
ein animiertes 80s-Like Video. Hammer!

{% youtube b20SV7v0fzE %}

