Title: Alan Lomax und sein Archiv
Date: 2015-03-26 15:30
Author: marvin
Category: Uncategorized
Tags: alanlomax, bobdylan, music, woodyguthrie
Slug: alan-lomax-und-sein-archiv

![707px-Alan_Lomax]({static}/images/707px-Alan_Lomax.jpg)

In Zeiten des für wenige Cents geklickten One-Hit-Wonders auf iTunes
fragt man sich schon manchmal wie die Kultur der Musik archiviert und
konserviert werden kann. Vor allem wäre so ein Archiv ein kulturelles
Geschenk was seinesgleichen sucht.

Ich glaube ich hörte von [Alan
Lomax](https://de.wikipedia.org/wiki/Alan_Lomax) das erste mal in
Verbindung mit [Woody
Guthrie](https://de.wikipedia.org/wiki/Woody_Guthrie) in der [Bob Dylan
Dokumentation von Martin
Scorsese](https://de.wikipedia.org/wiki/No_Direction_Home_%E2%80%93_Bob_Dylan).
Alan Lomax widmete sich der Aufzeichnung von Interviews und Musik aus
der ganzen Welt aber zu meist amerikanischer Folk-Musik. Er tingelte von
Land zu Land und nahm Menschen und ihre Musik auf. Was mir bis heute
nicht bewusst war: Es gibt sein Archiv im
[Internetz](http://research.culturalequity.org/audio-guide.jsp). Ich bin
dann erstmal weg...

