Title: Schade das die Zeiten vorbei sind in denen...
Date: 2012-03-09 10:13
Author: marvin
Category: status
Tags: art, graffiti, qrcodes, sweza
Slug: schade-das-die-zeiten-vorbei-sind-in-denen

Schade das die Zeiten vorbei sind in denen ich selber noch Graffiti
gemalt habe. Ich habe es geliebt...und liebe es immer noch wenn ich mir
die bunten Wände der Straßen anschaue. Ich versuche meine Augen immer
auf zu halten für neue Sachen. Seien es Sticker, Pieces oder Kacheln. Es
fasziniert mich einfach. Auch wenn Streetart mittlerweile zum Synonym
für den Ausverkauf von Kunst geworden ist. So ähnlich wie jeder mit
einer DSLR ein Fotograf ist...ist jeder mit einem Sticker ein
Streetartist.

Aber was wenn die Stadt denkt das ihre Wände viel besser in grau
aussehen und bunte Farben den Wunsch nach Freiheit auslösen könnte?
Diesen vernichteten Kunstwerken wurde nun ein digitaler Friedhof
gespendet. Mithilfe von QR-Codes, die er auf Kacheln gesprücht, anbringt
kann man die "gelöschte" Streetart auf seinem Handy anschauen. So wird
es zum digitalen Archiv von urbaner Kunst. Danke
[SWEZA](http://sweza.com/)...

{% youtube 9AQO8rShLxo %}

([via](http://www.kraftfuttermischwerk.de/blogg/?p=34587))

