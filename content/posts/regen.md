Title: Regen
Date: 2013-04-19 19:38
Author: marvin
Category: Uncategorized
Tags: regen, springtime
Slug: regen

![IMG_20130419_193744]({static}/images/IMG_20130419_193744.jpg)

Man könnte es fast Frühling nennen. Seit fast einer Woche sind es
draussen erträgliche Temperaturen. Nur das ich davon nicht viel
mitbekomme. Ich sitze Zuhause und brüte eine Grippe aus. Ich lahme vor
mir hin. Selbst das Internetz schenkt mir kaum Freude. Heute verließ ich
für kurze Zeit unsere Wohnung um einzukaufen. Es regnet. Und der Geruch.
Natur die den Frühling in sich aufsaugt. Den Frühling einatmet. Irgendwo
dazwischen laufe ich herum. Ein wenig benebelt von der Grippe. Gerade
lese ich Fahrenheit 451. Eigentlich wäre es Zeit für Literatur die mich
zum träumen bringt. Kenne ich aber nicht...

