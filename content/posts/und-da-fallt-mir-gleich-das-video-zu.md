Title: Und da fällt mir gleich das Video zu...
Date: 2012-05-15 14:15
Author: marvin
Category: Uncategorized
Tags: coldcave, delorean, endlesssummer, music, ocean, surfing, waves
Slug: und-da-fallt-mir-gleich-das-video-zu

Und da fällt mir gleich das Video zu dem Delorean Remix von Cold Caves
Song "Life Magazine" ein. Der Song an sich ist schon der Hammer. Aber
wenn man sich den mit Ausschnitten aus dem Film "Endless Summer 2"
ansieht, kann es kaum etwas besseres geben.

{% youtube bOwUyFQKo8o %}

