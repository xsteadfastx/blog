Title: Wie judenfeindlich ist Deutschland?
Date: 2013-10-30 10:34
Author: marvin
Category: Uncategorized
Tags: antisemitism, germany, judaism, politics
Slug: wie-judenfeindlich-ist-deutschland

![DSC\_0058]({static}/images/3132266905_5e035cb1c8_b.jpg)

Die Antwort ist "erschreckend sehr". Und hier geht es nicht um die
offensichtlichen Anschuldigungen und Brunnenvergifter-Mythen. Wie oft
ich von Menschen höre "Alle Juden sind reich" oder "Die Rothschilds
kontrollieren die Weltbank". Oft wird das ganze in sogenannte
"Israelkritik" gegossen. Ein perfektes Mittel um seinen Antisemitismus
zu tarnen. Es wird Juden in Europa mittlerweile empfohlen ihren Glauben
nicht in der Öffentlichkeit zu zeigen. Kippah und Davidssterne tragen
sind Tabu. Traurige Realität. Ich hatte mich auch gewundert als ich
einmal im Fußballstadion wegen meiner Davidstern-Kette angepöbelt wurde.
What the fuck?

Das Erste hat nun eine Dokumentation zu Antisemitismus in Deutschland
gezeigt. Zum heulen...

{% youtube ZqkY-JIKPM8 %}

