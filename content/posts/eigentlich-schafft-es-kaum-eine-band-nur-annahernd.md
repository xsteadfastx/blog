Title: Eigentlich schafft es kaum eine Band nur annähernd...
Date: 2012-05-22 09:57
Author: marvin
Category: Uncategorized
Tags: music, thehorrors
Slug: eigentlich-schafft-es-kaum-eine-band-nur-annahernd

Eigentlich schafft es kaum eine Band nur annähernd an My Bloody
Valentine ranzukommen. Ich wünschte es wäre anders, aber die einzigen
bei denen es geklappt hat mich so umzureissen waren The Horrors mit
ihrem Album "Primary Colours". Daraus gibt es hier den Song "Mirrors
Image"...ein Knaller...

{% youtube y51iNsccx0Q %}

