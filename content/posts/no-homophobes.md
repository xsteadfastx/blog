Title: No Homophobes
Date: 2012-09-27 11:28
Author: marvin
Category: Uncategorized
Tags: internetz, politics, sexism, twitter
Slug: no-homophobes

![nohomophobes]({static}/images/nohomophobes.jpg)

Man braucht nicht Twitter oder das Internetz um zu sehen, dass homophobe
Begriffe in der Umgangssprache angekommen sind. Da wo viel gesagt wird
werden auch viele beknackte Sachen gesagt. Die Seite
[NoHomophobes.com](http://www.nohomophobes.com/#!/today/) zählt Tweets
mit den Begriffen "Faggot", "No Homo", "So Gay" und "Dyke". Sie will so
aufklären, ins Bewusstsein rufen was die Begriffe bedeuten und dafür
Sensibilisieren. Bin aber immer wenig skeptisch bei Internet-Prangern.
Wer aber Kackscheiße ins Internetz brüllt muss damit rechnen das es auch
gelesen wird...

> This website is designed as a social mirror to show the prevalence of
> casual homophobia in our society. Words and phrases like “faggot,”
> “dyke,” “no homo,” and “so gay” are used casually in everyday
> language, despite promoting the continued alienation, isolation and —
> in some tragic cases — suicide of sexual and gender minority (LGBTQ)
> youth.
>
> We no longer tolerate racist language, we’re getting better at dealing
> with sexist language, but sadly we’re still not actively addressing
> homophobic and transphobic language in our society.  
>  <cite>[NoHomophobes.com](http://www.nohomophobes.com/#!/about/)</cite>

([via](http://www.kraftfuttermischwerk.de/blogg/?p=41724))

