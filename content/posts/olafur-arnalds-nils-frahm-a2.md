Title: Ólafur Arnalds & Nils Frahm - a2
Date: 2012-11-22 13:31
Author: marvin
Category: Uncategorized
Tags: jodiesouthgate, michaelzoidis, music, nilsfrahm, olafurarnalds, psychedelic
Slug: olafur-arnalds-nils-frahm-a2

![a2]({static}/images/a2.jpg)

Ist es mal wieder psychedelic Thursday? Naja...habe ich mir gerade auch
nur ausgedacht. Aber was hier Michael Zoidis und Jodie Southgate, in
bester Joshuas Lightshow Manier, abliefern ist großartig. Sie
visualisieren das Ambientstück von Ólafur Arnalds und Nils Frahm. Ich
sag dann mal: Vollbild, zurück lehnen, wegträumen...

{% vimeo 49131274 %}

([via](http://thomasraukamp.tumblr.com/post/36277070996/a-visual-piece-by-michael-zoidis-and-jodie))

