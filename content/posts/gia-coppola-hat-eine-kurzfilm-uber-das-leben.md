Title: Gia Coppola hat eine Kurzfilm über das Leben...
Date: 2012-06-12 10:23
Author: marvin
Category: Uncategorized
Tags: giacoppola, movies, robertschwartzman
Slug: gia-coppola-hat-eine-kurzfilm-uber-das-leben

![364925]({static}/images/364925.png)

Gia Coppola hat eine Kurzfilm über das Leben als Screenwriter in
Hollywood gemacht. Wie gewöhnlich...großartig! Die Musik ist übrigens
von Robert Schwartzman...

{% youtube bPPzJ6RQehI %}

