Title: Sie fliegen durch Gold
Date: 2017-10-18 13:23
Slug: sie-fliegen-durch-gold
Tags: essay

Der Herbst strahlt die letzte Hitze und Sonne in unsere müden Gemüter und ich beobachte die ersten Vogel Formationen über den Dächern unserer Straße. Ich sehe das große schwarze "V" am am Himmel und beneide diese kleinen Kreaturen um ihren Weg und dem Zurücklassen Unsereins und der Menschlichkeit. In ihrere Winzigkeit sind so so schön, kleine Geschöpfe die durch Gold zu fliegen scheinen. Gold welches ich nur passiv und aus weiter Ferne beobachten kann. Ich werde nie Teil dieser Schönheit sein können. Ich hoffe ihr kommt bald wieder, und lässt mich nie wieder allein.
