Title: Fotos aus der Tristesse
Date: 2015-11-28 08:52
Slug: fotos-aus-der-tristesse
Tags: photography, holga, wolfsburg

![Wolfsburg Tristesse]({static}/images/23354048975_dd7e093f88_b.jpg)

Wolfsburg und ich. Irgendwie habe ich mich immer noch nicht daran gewöhnt wieder hier zu sein. Ich habe mir seit über einem Jahr mal wieder meine Holga genommen und versucht damit umzugehen.

![Wolfsburg Tristesse]({static}/images/22986113799_d39cebc094_b.jpg)

![Wolfsburg Tristesse]({static}/images/23354048325_3fd6712e1e_b.jpg)

![Wolfsburg Tristesse]({static}/images/23271489101_cea8067ab5_b.jpg)

![Wolfsburg Tristesse]({static}/images/23327922806_d0cb859515_b.jpg)

![Wolfsburg Tristesse]({static}/images/23327922536_5d1144ca2f_b.jpg)

![Wolfsburg Tristesse]({static}/images/23058258030_866db42fe6_b.jpg)

![Wolfsburg Tristesse]({static}/images/23327921936_bd5c320ed1_b.jpg)

![Wolfsburg Tristesse]({static}/images/22726907983_640799cd1f_b.jpg)

![Wolfsburg Tristesse]({static}/images/22986111429_967b88579e_b.jpg)

![Wolfsburg Tristesse]({static}/images/23354045705_5230ea1925_b.jpg)
