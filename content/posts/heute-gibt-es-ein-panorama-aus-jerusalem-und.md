Title: Heute gibt es ein Panorama aus Jerusalem und...
Date: 2012-06-03 13:48
Author: marvin
Category: Uncategorized
Tags: israel, jerusalem, panorama, photography
Slug: heute-gibt-es-ein-panorama-aus-jerusalem-und

![Austrian Hospice]({static}/images/7327309412_c5e3f5f310_b.jpg)

Heute gibt es ein Panorama aus Jerusalem...und zwar von dem Dach des
österreichischen Hospitzes. Ein ganz toller Ort in der Altstadt
Jerusalems. Ein kleines grünes Paradies in der hektischen Altstadt. Und
ausserdem kann man ganz großartigen Apfelstrudel essen...
