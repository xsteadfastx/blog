Title: Die Golden Gate Bridge ist 75 Jahre alt...
Date: 2012-05-31 13:10
Author: marvin
Category: Uncategorized
Tags: goldengatebridge, sanfrancisco, timelapse
Slug: die-golden-gate-bridge-ist-75-jahre-alt

Die Golden Gate Bridge ist 75 Jahre alt geworden. Ich bin bis jetzt
leider nur einmal drüber gefahren... hier ist ein Video zu der
"Geburtstagsfeier"...ganz wunderbar gemacht...

{% vimeo 43004940   %}
([via](http://www.doobybrain.com/2012/05/31/the-golden-gate-way/))

