Title: Venedig, die Zweite
Date: 2013-07-29 10:10
Author: marvin
Category: Uncategorized
Tags: art, biennale, photography, rodinal, venice, yashicat3
Slug: venedig-die-zweite

![Und noch mehr Hocker]({static}/images/9379405454_80bda45402_b.jpg)

Ich glaube ich habe mich in Venedig verliebt...

![Ai Weiwei und seine Hocker]({static}/images/9379395296_cf6d86990e_b.jpg)

![Ai Weiwei's Hocker]({static}/images/9379319254_3197d4ec74_b.jpg)

![Russischer Geldregen]({static}/images/9376545899_be391c0ab1_b.jpg)

![Ich vor dem israelischen Pavillon]({static}/images/9379358502_fbe4665f01_b.jpg)

![Biennale]({static}/images/9379374674_1302b5500a_b.jpg)

![Venedig]({static}/images/9379417876_1f35c4ae8f_b.jpg)

![La Biennale]({static}/images/9376639941_3fe489f2ea_b.jpg)

![Pavillon]({static}/images/9376643753_daf8ac8b83_b.jpg)

![Mickey]({static}/images/9376666347_f30762dedd_b.jpg)

![Kanal]({static}/images/9379459882_4d1deea269_b.jpg)

![Sonne über dem Wasser]({static}/images/9376685713_21b16e0f0c_b.jpg)

![Venedig]({static}/images/9379509100_a4baccd2ed_b.jpg)

![Noch mehr Sonne über Venedig]({static}/images/9376731705_a0ec05e3f6_b.jpg)

![Venedigs Tod]({static}/images/9379523742_f0f469949a_b.jpg)

![Maria]({static}/images/9379529138_60b6632a35_b.jpg)

![Regina Pacis Ora Pro Nobis]({static}/images/9379535354_c69a39def9_b.jpg)

![New Zealand Pavillon]({static}/images/9376760269_3c10430a2f_b.jpg)

![World War Z]({static}/images/9379550514_73f5ed98ff_b.jpg)

![Echte Bauarbeiter fahren Boot]({static}/images/9379554452_aeb2b42497_b.jpg)

![Stau auf dem Kanal]({static}/images/9379558058_317b5a02c4_b.jpg)

![Das letzte Bild der Reise]({static}/images/9376823635_6ac7ffff36_b.jpg)

