Title: Psychedelic Monday: Ruslan Khasanov
Date: 2013-07-29 09:55
Author: marvin
Category: Uncategorized
Tags: art, psychedelic, ruslankhasanov
Slug: psychedelic-monday-ruslan-khasanov

![pacificlight]({static}/images/pacificlight.jpg)

[Ruslan Khasanov](http://ruskhasanov.com/) feier ein psychedelisches
Feuerwerk ab. Schönen guten Morgen...

{% vimeo 71035290 %}

([via](http://www.thefoxisblack.com/2013/07/29/ruslan-khasanov-mixes-ink-and-oil-to-create-a-kaleidoscopic-video/))

