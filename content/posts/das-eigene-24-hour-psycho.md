Title: Das eigene 24 Hour Psycho
Date: 2016-01-28 16:01
Slug: das-eigene-24-hour-psycho
Tags: python, hitchcock, douglasgordon, art

{% giphy 30zT7n2imuZVe %}

Es ist schon ein paar Jahre her als ich die [Douglas Gordon](https://de.wikipedia.org/wiki/Douglas_Gordon) Ausstellung im Kunstmuseum Wolfsburg besuchte. Ich laube es war im Jahr 2007. Ein Kunstwerk hat sich besonders in meine Erinnerungen gehalten. [24 Hour Psycho](https://de.wikipedia.org/wiki/24_Hour_Psycho). Eine entschleunigte Version von Hitchcock's Meistwerk Psycho. Eins wird auf jeden Fall klar. Jeder Frame scheint ein komponiertes Bild zu sein, nimmt man einfach die Geschwindigkeit aus dem Film.

Nun zu meiner Geschichte. Ich war eine Woche krankgeschrieben. Ich lag auf dem Sofa, geplagt mit Halsschmerzen und allen möglichen anderen Erkältungssymptomen. Nach ein paar Tagen wollte ich ein wenig programmieren um mich abzulenken. Da kam mir 24 Hour Psycho in den Sinn und das es bestimmt möglich sei es in Python in ein paar Code-Zeilen nachzubauen. Ich präsentiere [24hourvideo](https://github.com/xsteadfastx/24hourvideo). Danke [opencv](http://opencv.org) und [ffmpeg](http://ffmpeg.org). Man füttert es mit einem Video und schon läuft das Video so lange um 24 Stunden zu füllen. Frame für Frame. Mir wurde dadurch mal wieder bewusst das Mathe und ich sich seit der 5 Klasse nicht näher gekommen sind. Ich musste die Millisekunden ausrechnen die zwischen zwei Frames liegt um das ganze auszudehnen.

Vielleicht kann ja einer was damit anfangen.
