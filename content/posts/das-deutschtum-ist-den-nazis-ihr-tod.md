Title: Das Deutschtum ist den Nazis ihr Tod
Date: 2012-08-09 14:27
Author: marvin
Category: Uncategorized
Tags: germany, nuernberg, politics, racism
Slug: das-deutschtum-ist-den-nazis-ihr-tod

![img664]({static}/images/6435565383_51e4581c07_b.jpg)

Ach ja, Nürnberg und die Nazis. Nicht erst seit seit dem, oben
gezeigten, Reichsparteitagsgelände ist die Verbindung zwischen der Stadt
und dem Wahnsinn klar verknüpft. Und wenn man mich fragt fällt es dieser
Stadt wirklich schwer dieses Image oder die Glocke der Geschichte
abzustreifen. Als jemand der fast täglich an dem Reichsparteitaggelände
vorbei geht bemerkt da die kuriosesten Auswüchse des Nazi-Tourismus. Ein
negatives Highlight war das obligatorische Familienfoto vor dem Gelände
mit Hitlergruss der kompletten Bande. Kinder inklusive. Die Stadt
scheint sich schon der Aufbereitung der Historie zu bemühen, aber es
sind die Menschen die der Spiegel des geschichtlichen Verständnisses
sind. Um auch ein positives Beispiel zu nennen: Der breite Protest gegen
einen NPD Infostand in der Stadt. Dieser wurde so belagert das er
weiterziehen musste.

Es gibt aber auch die Fälle in dem man nicht weiß ob es sich um gutes
Nazi-Betrollen handelt oder es die Realität darstellt.

Heute aus der Abteilung... "Das Deutschtum stellt sich selber ein Bein":

Der geplante Neonazi-Treff in Nürnberg Langwasser scheitert daran das es
einem Raum einem zweiten Notausgang fehlt. Und zwar hatten die Gruppen
"Freies Netz Süd" und "Bürgerinitiative Ausländerstopp" die Kellerräume
einer alten Kegelbahn angemietet und wollten da ihren "Treff"
einrichten. Die Räumlichkeiten sollten für Schulungen und
Veranstaltungen genutzt werden. Daraus wird wohl erstmal nichts...und
das alles wegen dem fehlenden Notausgang...

([via](http://www.nordbayern.de/region/nuernberg/kein-treff-fur-nazis-in-nurnberg-1.2266553))

