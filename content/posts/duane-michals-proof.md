Title: Duane Michals - Proof
Date: 2012-09-03 13:30
Author: marvin
Category: Uncategorized
Tags: art, duanemichals, photography
Slug: duane-michals-proof

![tumblr_lp801o03zn1qa7dqxo1_1280]({static}/images/tumblr_lp801o03zn1qa7dqxo1_1280.jpg)

<ins datetime="2012-09-03T11:17:03+00:00">"This photograph is my proof.
There was that afternoon when things were still good between us, and she
embraced me, and we were so happy. It did happen, she did love me. Look
see for yourself!"</ins>

Foto und Text sind oft doch näher beieinander als man denkt. Gerade
begeistern mich [Duane
Michals](http://de.wikipedia.org/wiki/Duane_Michals) Fotografien sehr.
Irgendwas zwischen Melancholie und Leben... es gibt soviel zu
entdecken...

