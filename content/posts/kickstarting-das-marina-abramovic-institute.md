Title: Kickstarting das Marina Abramovic Institute
Date: 2013-08-08 13:15
Author: marvin
Category: Uncategorized
Tags: art, kickstarter, ladygaga, marinaabramovic
Slug: kickstarting-das-marina-abramovic-institute

![ladygaga_abramovic]({static}/images/ladygaga_abramovic.jpg)

Ich und Performancekunst. Wir haben da ein nicht so tolles Verhältnis.
Wenn aber Marina Abramovic ein [Kickstarter
Projekt](http://www.kickstarter.com/projects/422090958/marina-abramovic-institute-the-founders)
gestartet hat um ein Institut aufzubauen und in ihrem Promovideo Lady
Gaga auftreten lässt ist das Grund genug das hier zu posten. Das ganze
ist nicht ganz NSFW...

{% vimeo 71919803 %}

