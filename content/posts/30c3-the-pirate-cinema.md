Title: 30c3: The Pirate Cinema
Date: 2014-01-20 15:53
Author: marvin
Category: Uncategorized
Tags: 30c3, art, bittorrent, internetz, thepiratebay
Slug: 30c3-the-pirate-cinema

Ich hatte ja schon mal über das Pirate Cinema
[gebloggt](http://xsteadfastx.org/2013/06/06/the-pirate-cinema-bittorrent-kunst/).
Zwei Künstler die sich die Top 100 der gefragten Torrents auf Pirate Bay
seeden. Die einzelnen zerstückelten Teile des Transfers werden dann per
Beamer projiziert. Das ganze ist dann sehr glitchig und irgendwie auch
verstörend.

Die Künstler haben einen Talk auf dem 30c3
[gehalten](http://events.ccc.de/congress/2013/Fahrplan/events/5607.html).
Ich mag das Projekt. Nur leider haben sie ihren Code nicht
veröffentlicht. Ich würde mich sehr dafür interessieren.

{% youtube uNHhifNhbEE %}

