Title: Slowdive - Souvlaki Space Station
Date: 2016-04-13 15:49
Slug: slowdive-souvlaki-space-station
Tags: slowdive, music

Ein wenig Shoegaze-Geschichte am Mittwoch Nachmittag. Gestern die Hoffnungen auf das Champions League Halbfinale verspielt und mit leichter Melancholie vom Blickwinkel aus des ersten Jahresviertels heute aufgewacht. Da ist Slowdive ein wenig Medizin.

{% youtube tqkXqHTNPBg %}
