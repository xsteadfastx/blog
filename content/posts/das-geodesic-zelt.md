Title: Das Geodesic Zelt
Date: 2012-08-16 22:13
Author: marvin
Category: Uncategorized
Tags: buckminsterfuller, geodisc
Slug: das-geodesic-zelt

![624169]({static}/images/624169.jpg)

Was ein schönes Ding. Wenn zelten dann in einem Zelt was nach dem
Prinzip von Buckminster Fullers Geodesic Dome aufgebaut ist. Dazu ist es
innerhalb kürzester Zeit aufgebaut. Jetzt brauche ich nur noch ein
kleines Vermögen und ich kann dieser Zelt der deutschen Firma
[Heimplanet](http://heimplanet.com/produkte/the-cave/) bestellen...

{% youtube uPUlhLJhHdM %}

{% youtube YABsA2hlk98 %}

