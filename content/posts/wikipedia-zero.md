Title: Wikipedia Zero
Date: 2013-11-08 12:38
Author: marvin
Category: Uncategorized
Tags: internetz, wikipedia
Slug: wikipedia-zero

![cc-by-sa [Akapoor (WMF)](https://wikimediafoundation.org/wiki/File:Wikipedia_Zero_Logo.jpg)]({static}/images/Wikipedia_Zero_Logo.jpg)

Das die deutsche Wikipedia ein Admin-Kleingarten-Wächter-Problem hat,
ist nichts neues. Trotzdem ist es eins dieser Projekte die das
Informatiosnzeitalter ausmachen. Ein kollektives Wissen, welches
zusammen getragen wird. Und sind wir mal ehrlich, wie großartig ist es
wenn man mit seinen 3G-Kindle noch die Wikipedia lesen kann. Und das
Weltweit. Wikipedia Zero schlägt ein wenig in die gleiche Kerbe. Es geht
darum Mobilfunkanbieter dazu zubringen einen kostenfreien Zugang zu
Wikipedia zu schaffen. Ich finde die Idee klasse. Freies Wissen für
alle! Ich hoffe das Projekt wird Erfolg haben und ein wenig was ins
rollen bringen.

Mehr Infos gibt es
[hier](https://wikimediafoundation.org/wiki/Wikipedia_Zero).

