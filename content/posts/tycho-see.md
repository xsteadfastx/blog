Title: Tycho - See
Date: 2014-05-15 10:44
Author: marvin
Category: Uncategorized
Tags: music, tycho
Slug: tycho-see

Tycho hatte ich hier schon wirklich oft im Blog. Und jedes mal wieder
lohnt es sich Tycho hier zu erwähnen und seine Arbeiten zu teilen.
Gerade an Tagen an denen ich mein Kopf über Code zerbreche ist seine
Musik eine wirkliche Inspiration. Und die Visuals... ein Traum. Gerade
wieder zu bewundern bei seinem neuen Video "See".

{% youtube 5wWBLbQInqk %}

