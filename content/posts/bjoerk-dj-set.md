Title: Björk DJ Set
Date: 2015-11-24 09:17
Slug: bjoerk-dj-set
Tags: bjoerk, music

Björk hat ihr Geburtstags-DJ-Set auf Soundcloud hochgeladen. Dabei geht es noch nicht einmal um ihren eigenen Geburtstag. Sie feierte ihren Fünzigsten vor ein paar Tagen. Es war der Geburtstag von [Tri Angle Records](http://tri-anglerecords.com/). Erst gestern hatte ich ein langes Gespräch über Björk. [Debut](https://de.wikipedia.org/wiki/Debut_%28Bj%C3%B6rk-Album%29) war einer meiner ersten CD's die ich als Kind gekauft hatte. Verstanden hatte ich die Musik nicht und war doch ziemlich fasziniert. Und dies gilt auch noch für heute.

{% soundcloud https://soundcloud.com/bjork/bjork-tri-angle-records-birthday-dj-set %}

([via](https://thecreatorsproject.vice.com/de/blog/das-secret-dj-set-von-bjoerk-war-der-reinste-akustische-wahnsinn-534?utm_source=vicefb&utm_medium=link))
