Title: Ich höre gerade mal wieder den Lost In...
Date: 2012-06-14 12:12
Author: marvin
Category: Uncategorized
Tags: deathinvegas, lostintranslation, movies, music, sofiacoppola
Slug: ich-hore-gerade-mal-wieder-den-lost-in

![tumblr_m39m6lkDQ11rn5d6yo1_500]({static}/images/tumblr_m39m6lkDQ11rn5d6yo1_500.gif)

Ich höre gerade mal wieder den Lost In Translation Soundtrack.
Eigentlich besteht der Soundtrack fast ausschließlich aus Hits...manche
Songs stechen aber hervor. Zum Beispiel Death In Vegas mit dem Song
"Girls". Passend dazu hat jemand ein Musikvideo aus Lost In Translation
Szenen zusammen geschnitten...ich bin Fan...

{% youtube G0zV_aL8aYc %}

