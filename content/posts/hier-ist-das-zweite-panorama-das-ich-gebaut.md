Title: Hier ist das zweite Panorama das ich gebaut...
Date: 2012-05-31 07:29
Author: marvin
Category: Uncategorized
Tags: israel, jerusalem, panorama, photography
Slug: hier-ist-das-zweite-panorama-das-ich-gebaut

![Jerusalem]({static}/images/7302940970_909600888d_b.jpg)

Hier ist das zweite Panorama das ich gebaut habe. Dies ist bis jetzt das
aufwendigste. Es entstand auf der
[Davidszitadelle](http://de.wikipedia.org/wiki/Davidszitadelle). Von
dort oben hat man einen ganz tollen Ausblick über die Altstadt von
Jerusalem.
