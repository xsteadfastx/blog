Title: A Place To Bury Strangers - I Lived My Life To Stand In The Shadow Of Your Heart
Date: 2012-09-21 14:18
Author: marvin
Category: Uncategorized
Tags: aplacetoburystrangers, music, newyork
Slug: a-place-to-bury-strangers-i-lived-my-life-to-stand-in-the-shadow-of-your-heart

![782509]({static}/images/782509.jpg)

Gerüchteweise soll "A Place To Bury Strangers" die lauteste Band New
Yorks sein. Kein Plan. Einer der interessantesten vielleicht. Ganz weit
weg von dem Hipster-Konsum-Geschrammel. Soundwände bis zum erbrechen.
Dazu auch noch dieses tolle Video.

{% youtube OArdM4Cb-ho %}

Als Zugabe noch ein Livevideo der Band mit einem der großartigsten
Bandnamen aller Zeiten.

{% youtube HSsWFa5UUoY %}

