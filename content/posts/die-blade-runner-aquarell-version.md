Title: Die Blade Runner Aquarell Version
Date: 2013-11-21 10:52
Author: marvin
Category: Uncategorized
Tags: andersramsell, art, bladerunner, movies
Slug: die-blade-runner-aquarell-version

![blade_runner_aquarelle]({static}/images/blade_runner_aquarelle.jpg)

Ja ich weiß. Dies wurde die letzten Tage durch jedes Blog getrieben.
Trotzdem bleibt es Blade Runner und ich muss es hier einfach posten.
[Anders Ramsell](http://www.andersramsell.com/) hat fast 13.000 Frames,
aus Blade Runner, mit Aquarell nach gemalt und danach wieder animiert.
Ein Augenschmaus.

{% youtube SLwmlMezS3U %}

