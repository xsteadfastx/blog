Title: Nerds are one of the most dangerous groups in this country
Date: 2013-01-21 13:41
Author: marvin
Category: Uncategorized
Tags: alexjones, nerds, radio
Slug: nerds-are-one-of-the-most-dangerous-groups-in-this-country

![nerdsdanger]({static}/images/nerdsdanger.jpg)

Der Waffenfreund, Verschwörungstheoretiker und Radiohost [Alex
Jones](https://en.wikipedia.org/wiki/Alex_Jones_(radio_host)) fasst das
ganze mal zusammen:

> "They use their brains to hurt people..."

Weltklasse!

{% youtube iQpD9LolO-k %}

([via](http://boingboing.net/2013/01/18/nerds-are-one-of-the-most-da.html))

