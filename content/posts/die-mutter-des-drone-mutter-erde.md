Title: Die Mutter des Drone: Mutter Erde
Date: 2014-01-07 20:54
Author: marvin
Category: Uncategorized
Tags: art, earth, lottegeeven
Slug: die-mutter-des-drone-mutter-erde

So klingt also Mutter Erde in 9 Kilometern Tiefe. Die Künstlerin Lotte
Geeven ist zu einem der tiefsten Löchern der Erde
[gefahren](http://www.geeven.nl/post/67567627667) und hat ein wenig
Fieldrecording vorgenommen. Ambient vom feinsten.

{% vimeo 80266870 %}

([via](http://www.crackajack.de/2014/01/07/the-sound-of-earth/))

