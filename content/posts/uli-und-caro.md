Title: Uli und Caro
Date: 2012-01-09 11:12
Author: marvin
Category: post
Tags: holga, photography, rodinal, wedding
Slug: uli-und-caro

Früher als erwartet gaben sich zwei Freunde von uns das Ja-Wort. Da ich
eigentlich immer gerne Fotos mache, es dann doch nicht mache oder von
dem Ergebnis immer enttäuscht bin war ich skeptisch als ich zu meiner
Holga an jenem morgen griff. Ich hatte richtig vermutet: Ich bin
eigentlich sehr enttäuscht über das Ergebnis. Vielleicht sind die Fotos
auf der zweiten Rolle besser geworden. Doch hier seht ihr die Fotos:

![img713]({static}/images/6666141935_9c574d5567_b.jpg)

![img712]({static}/images/6666143495_9046fba6df_b.jpg)

![img714]({static}/images/6666146213_43ee9b9248_b.jpg)

![img715]({static}/images/6666147601_2730b5d526_b.jpg)

![img709]({static}/images/6666145137_c56ed7d330_b.jpg)

![img711]({static}/images/6666149009_8ecfe7e8c3_b.jpg)

