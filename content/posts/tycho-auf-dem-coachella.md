Title: Tycho auf dem Coachella
Date: 2015-04-15 08:22
Tags: tycho, music, coachella
Slug: tycho-auf-dem-coachella


Ein ganz wunderbares Liveset von [Tycho](http://iso50.com) das er gestern auf dem Coachella Festival performt hat. Die Visuals... unglaublich. Genau das richtige am Morgen.

<iframe src="https://vid.me/e/7TM1" width="640" height="360" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen scrolling="no"></iframe>
