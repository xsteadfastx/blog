Title: Azeda Booth - Kensington
Date: 2013-12-17 11:30
Author: marvin
Category: Uncategorized
Tags: azedabooth, music
Slug: azeda-booth-kensington

![azedaboothkensington]({static}/images/azedaboothkensington.jpg)

Ich weiß gar nicht ob es sich hierbei um ein offizielles Musikvideo von
Azeda Booth handelt. Es untermalt aber perfekt die Musik. Und da ich
gerade ein wenig auf einer melancholischen Blog-Pause reite, gibt es
dieses Video heute. Passt.

{% youtube QOPyN8VFGbs %}

