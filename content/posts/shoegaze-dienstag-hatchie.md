Title: Shoegaze Dienstag: Hatchie
Date: 2017-11-14 10:35
Slug: shoegaze-dienstag-hatchie
Tags: shoegaze, music, hatchie

Alles ein wenig in dem Ton der Cocteau Twins. Bis jetzt nur wenig Tracks auf ihrer [Soundcloud Seite](https://soundcloud.com/hatchiemusic). Ich bin gespannt was da noch so kommt. [Hier](https://www.thefader.com/2017/11/13/hatchie-sure-sugar-and-spice-stream/amp) gibt es noch einen Track.

{% youtube RiZ8NECETHc %}

{% soundcloud https://soundcloud.com/hatchiemusic/try %}
