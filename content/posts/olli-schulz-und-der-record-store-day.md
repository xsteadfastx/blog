Title: Olli Schulz und der Record Store Day
Date: 2015-04-14 13:01
Tags: music, ollischulz, recordstoreday
Slug: olli-schulz-und-der-record-store-day


Diesen Samstag ist wieder [Record Store Day](http://www.recordstoredaygermany.de/). Das bedeutet es gibt um Plattenladen des Vertrauens besondere Aktionen und vor allem tolle Sonderpressungen und andere Gimmicks. Olli Schulz hat das zum Anlass genommen, als offizieller Pate des Record Store Days, ein paar kleine Videos zu drehen.

Ich werde natürlich diesen Samstag mal in den [mono-ton](http://mono-ton.eu/) spitzen.

{% youtube RjHi_Fh__4s %}

{% youtube 5mv96WzOFLw %}

{% youtube XVTbN2vSoP4 %}

{% youtube ECqtiY7-qgo %}

{% youtube INHK0coYIkU %}
