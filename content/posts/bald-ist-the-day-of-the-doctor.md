Title: Bald ist "The Day Of The Doctor"
Date: 2013-11-15 16:38
Author: marvin
Category: Uncategorized
Tags: doctorwho
Slug: bald-ist-the-day-of-the-doctor

[![cc-by-sa randy stewart]({static}/images/2513544827_7b72d29a5a_b.jpg)](https://secure.flickr.com/photos/stewtopia/2513544827/)

Die BBC weiß echt wie man uns Whovians ganz heißt macht. In 8 Tagen ist
"The Day Of The Doctor", die Jubiläumsfolge wird aufgeführt. Ich habe
meine Tickets hier hängen und freue mich ungemein drauf. Um sich die
Tage zu verkürzen hat die BBC ein Prequel zur Folge hochgeladen. Und die
wirft ein paar interessante Fragen auf. Hauptrolle spielt der achte
Doktor und am Ende.. SPOILERS!!!

{% youtube -U3jrS-uhuo %}

Dann ist ein
[Trailer](http://www.doctorwho.tv/whats-new/video/trailer-an-adventure-in-space-and-time)
erschienen zum Fernsehfilm "An Adventure in Space and Time". Er
behandelt wohl die Geschichte der Entstehung Doctor Who's bei der BBC.
Meta und so. Also ich bin Fan und würde mir ja alles reinziehen.

{% youtube dEwikIhEZrE %}

