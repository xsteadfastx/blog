Title: Ride - Dreams Burn Down
Date: 2012-11-20 12:42
Author: marvin
Category: Uncategorized
Tags: music, ride
Slug: ride-dreams-burn-down

![riderbrixton]({static}/images/riderbrixton.jpg)

Gerade ist es so, dass wenn ich mich vor dieses Blog setze und
eigentlich was schreiben möchte, ich es nicht schaffe. Meine
Posting-Fequenz ist gleich null gegangen. Vielleicht liegt es daran,
dass ich gerade mich einfach chronisch überlastet fühle. Und oft bleibt
mir nichts anderes übrig als die Lieder zu posten die mich gerade
berühren. So wie dieser Song von Ride. Shoegaze und Dreampop war
eigentlich nie schöner als mit diesem Song.

{% youtube a8jzl1qZnTg %}

Und als kleine Zugabe: Das ganze Konzert von Ride...

{% youtube hL-bffg1fXo %}

