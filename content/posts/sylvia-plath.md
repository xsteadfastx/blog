Title: Sylvia Plath
Date: 2013-02-12 11:46
Author: marvin
Category: Uncategorized
Tags: books, slyviaplath, suicide, thebelljar
Slug: sylvia-plath

[![cc by psd]({static}/images/5818852739_c4ceffca25_b.jpg)](https://secure.flickr.com/photos/psd/5818852739/)

Nun ist es 50 Jahre her das [Sylvia
Plat](https://de.wikipedia.org/wiki/Sylvia_Plath)h sich ihr Leben nahm.
Vor 10 Jahren bekam ich den Roman "[Die
Glasglocke](https://de.wikipedia.org/wiki/Die_Glasglocke)" zum
Geburtstag geschenkt. Es vergingen noch ein paar Jahre bis ich das erste
mal ihn las. Und denke ich daran zurück schwebt Faszination und
gleichermaßen Traurigkeit über meinem Sein. Die Geschichte über ein
junges Mädchen was, nach einem Praktikum bei einer Modezeitschrift in
New York, an Depressionen leidet und sich versucht am Ende des Buches
das Leben zu nehmen. Der Roman soll viele biographische Elemente Sylvia
Plaths erhalten und vor allem die Stimmungslage vieler Frauen dieser
Zeit wieder spiegeln. Und auch meine Stimmung zu der Zeit konnte das
Buch auffangen. Natürlich ohne die Erfahrungen die die Protagonistin des
Buches zu durchleben. Berührt hat es mich trotzdem. Selbstmorde von
Menschen die mich beeindrucken hinterlassen immer einen faden Geschmack.

{% youtube _tvSDw84ArQ %}

