Title: Melancholia: Depression on Film
Date: 2016-05-23 10:14
Slug: melancholia-depression-on-film
Tags: melancholia, movies

![Ophelia von John Everett Millais]({static}/images/ophelia.jpg)

Melancholia ist einer dieser Lieblingsfilme die ich nicht ein zweites mal schauen konnte. So intensiv habe ich ihn in Erinnerung. Der [Nerdwriter](https://www.youtube.com/channel/UCJkMlOu7faDgqh4PfzbpLdg) hat über die Darstellung von Depression in dem Film ein Video gemacht.

{% youtube FPkANZ9HGWE %}
