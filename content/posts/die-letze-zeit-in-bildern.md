Title: Die letze Zeit in Bildern
Date: 2013-07-16 13:31
Author: marvin
Category: Uncategorized
Tags: adbknuernberg, art, bayreuth, nuernberg, photography, sommerfest, yashicat3
Slug: die-letze-zeit-in-bildern

Wir haben die Familie besucht...

![Sophie \#1]({static}/images/9296995285_0eff4dae90_b.jpg)

![Sophie \#2]({static}/images/9299772734_d8f1e1820d_b.jpg)

![Sophie \#3]({static}/images/9299761184_25ab605a1c_b.jpg)

Dann waren wir auf der Jahresausstellung der Akademie...

![Jahresausstellung ADBK Nürnberg \#1]({static}/images/9296998437_57e3e99970_o_b.jpg)

![Winkekatze]({static}/images/9257548764_22e18cb89a_b.jpg)

Da haben wir auch einen grafischen Tribut an die Vokuhila-Legende [Mike
Werner](https://de.wikipedia.org/wiki/Mike_Werner) gefunden...

![Die vokuhila Legende von Hansa rostock]({static}/images/9257566040_bb8880aa52_b.jpg)

![Noch mehr Legende]({static}/images/9254801759_0c1712785d_b.jpg)

![Jahresausstellung ADBK Nürnberg \#2]({static}/images/9297006181_8f643efd3f_b.jpg)

![Tattoo gegen spende]({static}/images/9254835539_6c5bdbeb2f_b.jpg)

![Elizabeth Taylor]({static}/images/9257861346_8958a2f439_b.jpg)

![Elisabeth Taylor]({static}/images/9255099789_40be045af8_b.jpg)

![Elizabeth Taylor und Michael Jackson]({static}/images/9255113149_9a6b8f9039_b.jpg)

Am Samstung sind wir planlos durch Nürnberg geradelt. Immer dahin wo wir
vorher noch nie waren. Hängengeblieben sind wir am neuen Skatepark...

![Love Skating - Hate Cops]({static}/images/9299803074_70849e87b8_b.jpg)

![Skatepark \#5]({static}/images/9299813232_ac7f55a6a4_b.jpg)

![Skatepark \#4]({static}/images/9297037581_ef2dbfaa49_b.jpg)

![Skatepark \#3]({static}/images/9299843654_4aff599f0d_b.jpg)

![Skatepark \#2]({static}/images/9297064795_6d1b1ae606_b.jpg)

![Skatepark \#1]({static}/images/9297071303_9987b05d67_b.jpg)

Pinkie Pie im Skatepark... fuck yeah...

![Pinkie Pie im Skatepark]({static}/images/9299793674_1410740eea_b.jpg)

![Neues Museum]({static}/images/9275058231_4751698b64_b.jpg)

Ab und zu spielt in der Innenstadt eine Klezmerband... ganz ganz
wunderbar...

![Ich bin eh für mehr Klezmer im öffentlichen Raum]({static}/images/9277843864_da594b16e5_b.jpg)

Und dann war da noch unser Ausflug nach Bayreuth...

![Franz Liszt]({static}/images/9285349142_531b0b9f3d_b.jpg)

![Bayreuth at Hofgarten Bayreuth]({static}/images/9285463550_ce3bc8f648_b.jpg)

