Title: Ceremony - Sick
Date: 2012-11-12 11:11
Author: marvin
Category: Uncategorized
Tags: ceremony, matadorrecords, music
Slug: ceremony-sick

![ceremony]({static}/images/ceremony.jpg)

Guten Morgen am Moshing Monday. Heute für sie: Ceremony mit "Sick". Und
mal wieder verstehe ich den Hass nicht der dieser Band entgegen
schwappt. Anscheinend haben einige "Real Punks" Probleme damit, dass
Ceremony jetzt bei Matador Records sind und tun so als ob sie beim
Teufel unterschrieben hätten. Ich finde fast alle Matador-Releases
wirklich interessant. Vor allem das es die Brücke schlägt zwischen Bands
wie Sonic Youth, Cold Cave, Cat Power und Fucked Up. Also Ruhe auf den
billigen Plätzen und sich mal freuen...

{% vimeo 14735424 %}

