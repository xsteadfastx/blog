Title: On The Road Trailer
Date: 2012-03-23 10:38
Author: marvin
Category: post
Tags: jackkerouac, kirstendunst, kristenstewart, movies, ontheroad, samriley
Slug: on-the-road-trailer

![On-The-ROad-New-Picture-4]({static}/images/On-The-ROad-New-Picture-4.jpg)

Ich kann es kaum fassen. Endlich gibt es den ersten Trailer zu der
Verfilmung eines meiner absoluten Lieblingsbücher. Und vor allem wenn
man die Schauspielerliste sieht...Kirsten Dunst, Sam Riley und Kristen
Stewart. Ich liebe dieses Buch und nach dem Trailer habe ich auch
garkeine Bauchschmerzen das sie es irgendwie verhunzt haben könnten. Ach
ja...ich freue mich so darauf. Ich hoffe das sie dieses Gefühl
wiedergeben können was das Buch vermittelt...so sehr wie es Filme
überhaupt schaffen Bücher wiederzugeben.

{% youtube ttDIcTQpLyQ %}

