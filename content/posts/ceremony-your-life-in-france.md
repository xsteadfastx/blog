Title: Ceremony -  Your Life In France
Date: 2015-04-29 09:17
Tags: ceremony, music
Slug: ceremony-your-life-in-france


So klingt es wenn Hardcore Punks ihre New Wave Einflüsse immer mehr ausbauen bis es genau zu diesem Track kommt. Ästhetisch und musikalisch ein melancholisches Meisterwerk. Es fängt ja schon beim Namen der Band an: Ceremony. Das Original von Joy Division/New Order ist ja schon perfekt. Was eine Band. Und ich mag es das sie sich weiter und weiter entwickeln. Ich bin heiß wie Frittenfett auf das kommende Album.

Danke für diese musikalische Untermalung in dieser melancholischen Zeit.

{% vimeo 126325766 %}

Und als kleine Zugabe noch das erste Video was völlig an mir Vorbei ging:

{% vimeo 124171429 %}
