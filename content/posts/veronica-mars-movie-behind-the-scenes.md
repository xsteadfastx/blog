Title: Veronica Mars Movie: Behind the Scenes
Date: 2013-03-27 12:55
Author: marvin
Category: Uncategorized
Tags: kickstarter, movies, tv, veronicamars
Slug: veronica-mars-movie-behind-the-scenes

Ich bin noch so aufgeregt wie am ersten Tag. Nun ist das
Kickstarter-Projekt, für den Veronica Mars Film, bei fast 4 Millionen
Dollar angekommen. Dazu kommt gerade frisch die Meldung rein das nun
auch Europa supportet wird.

https://twitter.com/RobThomas/status/316875162047897600

Dann habe ich ja doch noch eine Chance an mein T-Shirt zu kommen. Yeah!
Dazu gibt es nun ein kleines Video mit den ersten Reaktionen direkt nach
dem Launch des Kickstarter-Projekts. Ach ja... eigentlich müsste man die
Serie nochmal von vorne schauen. In meinem Fall wäre es mindestens das
fünfte mal.

{% youtube 0uiFIhxXeKI %}

