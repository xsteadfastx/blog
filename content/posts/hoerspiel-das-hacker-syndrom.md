Title: Hörspiel: Das Hacker-Syndrom
Date: 2016-02-10 13:24
Slug: hoerspiel-das-hacker-syndrom
Tags: telecomix, podcasts, stephanurbach, hacker

![Telecomix]({static}/images/telecomix.png)

Hier ein kleiner Podcasttipp zwischendurch. [Das Hacker-Syndrom](http://www.wdr3.de/programm/sendungen/wdr3hoerspiel/hackersyndrom-100.html) von Johannes Nichelmann. Es befasst sich mit [Stephan Urbach](http://herrurbach.de/) und die Zeit des arabischen Frühlings. Das Hörspiel hat mich wirklich sehr berührt.

{% audio http://podcast-ww.wdr.de/medstdp/fsk0/91/916466/wdr3hoerspiel_2016-01-26_dashackersyndromwdr3hoerspiel26012016_wdr3.mp3 %}
