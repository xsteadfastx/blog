Title: The Day of the Doctor
Date: 2013-11-02 11:58
Author: marvin
Category: Uncategorized
Tags: cinema, doctorwho, movies, nuernberg, tv
Slug: the-day-of-the-doctor

![tardisgermanymap]({static}/images/tardisgermanymap.jpg)

Ich werde immer aufgeregter. Der 23. November kommt immer näher. Und bis
heute morgen dachte ich noch das ich die 50 Jahre Jubiläums-Episode von
Doctor Who irgendwie im Internetz schauen muss. Ich lag falsch. Wie ich
herausfand, gibt es Screenings der Folge weltweit in Kinos. Unter
anderem auch in Nürnberg im Cinecitta. Das zeigte mir die blaue
Polizeirufbox auf einer
[Karte](http://www.doctorwho.tv/watch-the-day-of-the-doctor) an. Nun
hängen die ausgedruckten Karten in meinem Büro und ich bin über
glücklich. Als kleinen Heißmacher gibt es jetzt den ersten Teaser zur
Episode... yeah...

{% youtube 7hRy2N2CMhQ %}

