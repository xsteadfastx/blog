Title: Aphex Twin und sein Remote Orchestra
Date: 2013-12-13 13:58
Author: marvin
Category: Uncategorized
Tags: aphextwin, art, midi
Slug: aphex-twin-und-sein-remote-orchestra

![aphextwinorchestra]({static}/images/aphextwinorchestra.jpg)

In letzter Zeit höre ich viel Aphex Twin. Es scheint so als ob ich mein
Jugendtrauma des Konsums des
"[Windowlicker](https://de.wikipedia.org/wiki/Windowlicker)" Videos
überwunden habe. Das Video in dem [Chris
Cunningham](https://de.wikipedia.org/wiki/Chris_Cunningham) das Gesicht
von Aphex Twin Richard James auf andere Köpfe geshoppt hat. Echtes
Albtraum-Material. 2012 hat er ein Orchester von 48 Musikern und einem
24-köpfigen Chor per MIDI "gesteuert". Diese trugen Kopfhörer und
schauten auf eine Leinwand mit den visuellen Signalen für ihre Einsätze,
Noten und Geschwindigkeit. Ok gut... man kann per MIDI sich ein
Orchester zusammen klicken, aber ein Orchester per MIDI zu bedienen ist
schon weltklasse. Das ganze fand am 10. September 2012 in Wroclaw, Polen
[statt](http://pitchfork.com/news/44732-watch-aphex-twin-conduct-a-48-piece-orchestra-by-remote-control-in-poland/).

{% youtube uu5jH2BgPBM %}

{% youtube Dph-bkJL_wM %}

