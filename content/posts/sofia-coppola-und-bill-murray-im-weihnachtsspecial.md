Title: Sofia Coppola und Bill Murray im Weihnachtsspecial
Date: 2015-06-03 11:30
Slug: sofia-coppola-und-bill-murray-im-weihnachtsspecial
Tags: sofiacoppola, billmurray, netflix


{% youtube f46ivuCahK4 %}

Unfassbar. Sofial Coppola und Bill Murray drehen für Netflix ein Weihnachtsspecial mit dem Namen "A Very Murray Christmas". Alles was es bis jetzt gibt ist ein kleiner Teaser. Das reicht aber um mich anzufixen. Alles was aus ihrer Feder stammt kann einfach nicht schlecht werden. Ach ja, und Bill Murray... das gleiche gilt natürlich auch für ihn.

([via](http://kottke.org/15/05/a-very-murray-christmas))
