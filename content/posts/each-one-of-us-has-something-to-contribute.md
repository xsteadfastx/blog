Title: Each one of us has something to contribute...
Date: 2012-02-22 13:38
Author: marvin
Category: quote
Tags: buckminsterfuller
Slug: each-one-of-us-has-something-to-contribute

Each one of us has something to contribute. This really depends on each
one doing their own thinking, but not following any kind of rule that I
can give out, any command. We're all on the frontier, we're all in a
great mystery — incredibly mysterious…

<cite>Buckminster Fuller</cite>

