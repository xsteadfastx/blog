Title: Beirut - The Rip Tide
Date: 2012-06-28 11:52
Author: marvin
Category: Uncategorized
Tags: beirut, music, psychedelic
Slug: beirut-the-rip-tide

![beirut-rip-tide-3]({static}/images/beirut-rip-tide-3.jpg)

Wenn ich den Namen Rip Tide lese muss ich eigentlich zuerst an meinen
allerliebsten Plattenladen in Braunschweig denken. Aber hier geht es um
das neue Musikvideo von Beirut. Auch wenn mich die Musik gerade nicht so
vom Hocker haut...das Video umso mehr. Segeln, Meer und psychedelische
Farbeinlagen ala Joshuas Light Show.

{% youtube sX7fd8uQles   %}

([via](http://www.thefoxisblack.com/2012/06/27/beiruts-newest-video-for-the-rip-tide-takes-us-on-an-inky-voyage-across-the-sea/))

