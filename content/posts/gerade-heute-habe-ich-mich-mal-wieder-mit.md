Title: Gerade heute habe ich mich mal wieder mit...
Date: 2012-03-23 13:12
Author: marvin
Category: status
Tags: fernweh, fieldrecordings, newyork
Slug: gerade-heute-habe-ich-mich-mal-wieder-mit

Gerade heute habe ich mich mal wieder mit Fieldrecordings beschäftigt.
Christine und ich haben ja einige Recordings in Jerusalem gemacht
letztes Jahr. Wir überlegen bis heute was wir damit machen wollen. Nun
gibt es eine Idee die ich gerne ausbauen möchte...

Bis dahin habe ich dieses ganz wunderbare Video gefunden welches den
Gang durch eine U-Bahn Station in New York zeigt. Mehr braucht man auch
nicht. Fernweh und so...

{% vimeo 38788768 %}

