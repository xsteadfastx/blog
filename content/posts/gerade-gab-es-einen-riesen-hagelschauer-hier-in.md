Title: Gerade gab es einen riesen Hagelschauer hier in...
Date: 2012-05-15 13:46
Author: marvin
Category: Uncategorized
Tags: goldpanda, music
Slug: gerade-gab-es-einen-riesen-hagelschauer-hier-in

![artworks-000022917759-x7ix1p-original]({static}/images/artworks-000022917759-x7ix1p-original.jpg)

Gerade gab es einen riesen Hagelschauer hier in Nürnberg. Ich liebe
Donner und Gewitter. Nun sitze ich hier und höre diese beiden Tracks von
[Gold Panda](http://soundcloud.com/gold-panda) an. Alles stimmt...

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F1957430&amp;auto_play=false&amp;show_artwork=true&amp;color=3366cc"></iframe>

