Title: Tatort und ACAB
Date: 2013-01-28 15:39
Author: marvin
Category: Uncategorized
Tags: acab, tatort, tv
Slug: tatort-und-acab

[![by dokumentage]({static}/images/tatort.jpeg)](https://twitter.com/dokumentage/status/295641906724085761)

Nicht nur das der Tatort gestern besonders großartig war, jemand hat
auch eine nette Botschaft als Requisite hinterlassen. "All Cats Are
Beautiful" sag ich da nur. Zusätzlich zu diesem Schmankerl lohnt es sich
auch sonst die Folge "Melinda" zu schauen. Und nach dem letzten Dialog
der Episode muss man den neuen Kommissar einfach ins Herz aufnehmen:

"Tue Recht und tue Gutes dabei?"

"Von wem ist das?"

"Vom Internet."

{% youtube upEoXK3h_dk %}

