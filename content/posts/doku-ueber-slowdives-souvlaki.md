Title: Doku über Slowdive's Souvlaki
Date: 2015-06-16 10:38
Slug: doku-ueber-slowdives-souvlaki
Tags: pitchfork, documentary, slowdive, music


Pitchfork hat eine fast einstündige Dokumentation über das Album "Souvlaki" von Slowdive veröffentlich. Ich freue mich ja das Slowdive gerade wieder entdeckt werden. Auch wenn ich "My Bloody Valentine" verehre, bleibt Slowdive meine liebste Shoegaze Band. Ich kann mich an einen ganzen Winter erinnert an dem ich den Song "Avalyn 1" immer und immer wieder gehört habe. Ach ja...

{% youtube Sjr6esFXJl4 %}
