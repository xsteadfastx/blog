Title: Dürer zurück in seine Stadt
Date: 2012-02-06 13:24
Author: marvin
Category: post
Tags: art, football, nuernberg
Slug: durer-zuruck-in-seine-stadt

Was sehen meine müden Augen da? Die Ultras Nürnberg beteiligen sich mit
einem Transparent an der aktuellen Diskussion um das berühmte
"Selbstbildnis im Pelzrock" von Albrecht Dürer. Dieses hängt in der
alten Pinakothek in München. Und diese weigert sich das Bild dem
Germanischen Nationalmuseum in Nürnberg auszuleihen für ihre großen
Dürer-Retrospektive. Dieses weckt den stätigen Konflikt zwischen Franken
und Bayern. Ich als jemand der Wahl-Franke ist, kann das ganze nur
ansatzweise verstehen. Das dieser Lokalpatriotismus in die tiefsten
Kreise zieht zeigt das Transparent was letzten Freitag beim Spiel 1. FC
Nürnberg gegen Borussia Dortmund gezeigt wurden ist.

![190]({static}/images/190.jpg)

