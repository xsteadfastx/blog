Title: Dreamland
Date: 2013-08-19 16:08
Author: marvin
Category: Uncategorized
Tags: bobburnquist, skateboard
Slug: dreamland

[![cc-by-sa Fernando Mafra]({static}/images/514px-Bob_Burnquist.jpg)](https://en.wikipedia.org/wiki/File:Bob_Burnquist.jpg)

So sieht es also aus wenn der Skater Bob Burnquist sich sein Traumland
baut. Ich bin sprachlos. Hat nicht jeder von uns so ein Traumland?

{% youtube tSnfO15cAHE %}

