Title: Sarah Brendel Tour Portrait
Date: 2018-01-23 12:15
Slug: sarah-brendel-tour-portrait
Tags: sarahbrendel, documentary

Wir hatten ja ein kleines [Konzert]({static}/posts/sarah-brendel-am-21102017.md) vor ein paar Monaten. Sarah wurde von dem wunderbaren [Tim Markgraf](http://www.timmmarkgraf.de/) und seiner Kamera begleitet.

{% youtube 3iw3pIH9JfA %}
