Title: Cold Cave - A Little Death To Laugh
Date: 2013-04-05 11:19
Author: marvin
Category: Uncategorized
Tags: coldcave, deathwishinc
Slug: cold-cave-a-little-death-to-laugh-2

![alittledeathtolove]({static}/images/alittledeathtolove.jpg)

Ich habe das Gefühl, dass Cold Cave immer besser und besser werden.
Vielleicht auch ein tick düsterer. Ich mag es. Nun gibt es, langer Zeit
mal wieder, ein neues Musikvideo.

{% youtube ELRvdyMwVQw %}

Als kleine Zugabe, hier der Song "Oceans With No End" von der EP die
bald bei Deathwish Inc. rauskommt.

{% youtube vfRr0k2rUjk %}

