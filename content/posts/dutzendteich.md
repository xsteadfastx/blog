Title: Dutzendteich
Date: 2013-05-07 11:59
Author: marvin
Category: Uncategorized
Tags: nuernberg, photography
Slug: dutzendteich

![Dutzendteich]({static}/images/8711385324_db8a5d8071_b.jpg)

Oft wenn ich wieder nach Hause komme, aus dem Auto aussteige und die
Sonne gerade versucht sich hinter den Horizont zu schieben, merke ich
wie sehr ich es hier mag. Oft verachten wir die Orte an denen wir leben.
Ich tue das nicht...

![Dutzendteich]({static}/images/8710268469_9b16bf517a_b.jpg)

![On a wire]({static}/images/8711395478_4d8c5c899d_b.jpg)

