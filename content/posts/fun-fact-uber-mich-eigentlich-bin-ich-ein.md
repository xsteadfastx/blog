Title: Fun Fact über mich eigentlich bin ich ein...
Date: 2012-06-06 08:09
Author: marvin
Category: Uncategorized
Tags: london, queen, timelapse
Slug: fun-fact-uber-mich-eigentlich-bin-ich-ein

![343351]({static}/images/343351.png)

Fun Fact über mich...eigentlich bin ich ein verkappter Monarchist bin.
Naja nicht wirklich. Ich schaue halt gerne Dokumentationen über das
englische Königshaus. Leider habe ich die Parade auf der Themse letzten
Sonntag verpasst. [Ben Begley]({% vimeo user5305973) hat ein %}
schönes Time-Lapse Video davon gemacht und ins Internetz geladen.

{% vimeo 43386950   %}

([via](http://www.doobybrain.com/2012/06/05/the-queens-diamond-jubilee-celebration-on-the-thames-river-2012/))

