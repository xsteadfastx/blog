Title: The Source Family
Date: 2013-05-16 12:45
Author: marvin
Category: Uncategorized
Tags: documentary, hippie, thesource
Slug: the-source-family

![TheSourceFamily_Poster_ALT31]({static}/images/TheSourceFamily_Poster_ALT31.jpg)

"Couln't be more hippie" beschreibt "The Source" wohl am besten. Eine
"Sekte" entstanden aus einem vegetarischen Restaurant in Los Angeles die
sich um ihren religiösen Führer, mit dem großartigen Namen "[Father
Yod](https://en.wikipedia.org/wiki/Father_Yod)", sammelten. Der scheint
auf jeden Fall richtig einen an der Waffel gehabt zu haben. Natürlich
gab es auch eine psychedelische Band mit dem Namen "Ya Ho Wa 13". Diese
gibt es als Best-Of auf dem Soundtrack zu dem nun erscheinenden
[Dokumentarfilm](http://thesourcedoc.com/) über "The Source". Als Stream
unter dem Trailer...

{% vimeo 58953915 %}

https://soundcloud.com/drag-city/sets/the-source-family-soundtrack

([via](http://boingboing.net/2013/05/15/free-stream-the-source-family.html))

