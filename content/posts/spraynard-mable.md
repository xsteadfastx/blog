Title: Spraynard - Mable
Date: 2015-08-24 16:34
Slug: spraynard-mable
Tags: spraynard, music, jadetree
Description: Spraynards tolles neues Album

![Spraynard - Mable]({static}/images/spraynard-mable.jpg)

Schön das [Jade Tree](http://www.jadetree.com/) in letzter Zeit wieder Sachen released. Unter anderem dieses tolle Werk von der Band Spraynard. Und es gibt sie doch wieder... diese tollen 90er "Emo-Punk"-Bands. Es fällt mir schwer das Genre besser zu beschreiben. Ihr wisst was ich meine. Also einfach mal Geld in ihre Richtung werfen bitte.

<iframe style="border: 0; width: 400px; height: 472px;" src="https://bandcamp.com/EmbeddedPlayer/album=3207803976/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="http://spraynard.bandcamp.com/album/mable">Mable by Spraynard</a></iframe>
