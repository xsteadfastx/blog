Title: Dog Bite - Tranquilizers
Date: 2014-01-14 15:41
Author: marvin
Category: Uncategorized
Tags: dogbite, music
Slug: dog-bite-tranquilizers

![artworks-000067822111-tffetz-t500x500]({static}/images/artworks-000067822111-tffetz-t500x500.jpg)

Eigentlich braucht man immer etwas Shoegaze/Dreampop um durch den Tag zu
kommen. Das erste Album von Dog Bite mochte ich ja schon sehr. Und das
neue Album "Tranquilizers" schwimmt auch so vor sich hin. Also Augen zu
machen und ganz fest an den Frühling glauben.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/20038562&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>

([via](http://www.dazeddigital.com/music/article/18430/1/dog-bite-tranquilizers?utm_source=Link&utm_medium=Link&utm_campaign=RSSFeed&utm_term=dog-bite-tranquilizers))

