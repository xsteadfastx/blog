Title: altweibersommer
Date: 2011-09-29 11:10
Author: marvin
Category: post
Tags: holga, photography, rodinal
Slug: altweibersommer

Ich genieße diesen Herbstanfang. Jeder Sonnenstrahl gehört aufgesogen.
Das sind hier ein paar Holga Fotos der letzten Woche. Wir haben und
wurden besucht. Wir haben wirklich tolle Freunde.

![img459]({static}/images/6194339963_bb542e993d_b.jpg)

![img460]({static}/images/6194341809_487bd07653_b.jpg)

![img463]({static}/images/6194343479_9e909389a2_b.jpg)

![img465]({static}/images/6194861200_ef627481f8_b.jpg)

![img466]({static}/images/6194862224_7737c710fd_b.jpg)

![img468]({static}/images/6194346551_bbccf2900b_b.jpg)

