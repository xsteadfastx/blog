Title: Kerouacs Artwork Skizze
Date: 2012-08-14 13:20
Author: marvin
Category: Uncategorized
Tags: books, jackkerouac, ontheroad
Slug: kerouacs-artwork-skizze

![Kerouacs-Cover-Design]({static}/images/Kerouacs-Cover-Design.jpeg)

Und weil wir gerade bei Jack Kerouac und "On The Road" waren gibt es nun
eine kleine Zugabe. Eine Skizze von ihm selber wie das Artwork für das
Buch aussehen sollte. Nice!

([via](http://twentytwowords.com/2012/08/13/jack-kerouacs-design-for-the-cover-of-on-the-road/))

