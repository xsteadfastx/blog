Title: Wir verblonshen waren Teil der  blauen Nacht...
Date: 2012-05-21 14:07
Author: marvin
Category: Uncategorized
Tags: alfredhitchcock, blauenacht, nuernberg, photography, rearwindow, verblonshen
Slug: wir-verblonshen-waren-teil-der-blauen-nacht

![Rear Window]({static}/images/7241200288_441a815a7f_b.jpg)

Wir ([verblonshen](http://verblonshen.org)) waren Teil der "blauen
Nacht" in Nürnberg. Wir haben den Rathaus Innenhof, unter dem Titel
"[Rear Window](http://verblonshen.org/2012/05/10/blaue-nacht/)",
bespielt. Hitchcock und so... hier ein paar Fotos von den Vorbereitungen
und der fertigen Ausstellung. Hier eine kleine Auswahl...alle Fotos gibt
es
[hier](http://www.flickr.com/photos/marvinxsteadfast/sets/72157629829597786/with/7241188728/)...

![Rear Window]({static}/images/7241210864_d9df3141dd_b.jpg)

![Rear Window]({static}/images/7241208596_4f3692dde8_b.jpg)

![Rear Window]({static}/images/7241206928_9b116aede6_b.jpg)

![Rear Window]({static}/images/7241204724_03c75c0584_b.jpg)

![Rear Window]({static}/images/7241201538_356e0b3b24_b.jpg)

![Rear Window]({static}/images/7241198460_8751a9bcaf_b.jpg)

![Rear Window]({static}/images/7241196204_b1c52bd255_b.jpg)

![Rear Window]({static}/images/7241195838_1ff2189bcd_b.jpg)

![Rear Window]({static}/images/7241194054_6957aa07b3_b.jpg)

![Rear Window]({static}/images/7241193360_3b1cb4606f_b.jpg)

![Rear Window]({static}/images/7241192080_1e24afdef3_b.jpg)

![Rear Window]({static}/images/7241191612_a195440576_b.jpg)

![Rear Window]({static}/images/7241190564_d66e49ba71_b.jpg)

![Rear Window]({static}/images/7241190030_cdfc366eb4_b.jpg)

