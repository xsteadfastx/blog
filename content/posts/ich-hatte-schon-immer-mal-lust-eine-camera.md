Title: Ich hatte schon immer mal Lust eine Camera...
Date: 2012-03-15 12:22
Author: marvin
Category: status
Tags: art, cameraobscura, paris, photography
Slug: ich-hatte-schon-immer-mal-lust-eine-camera

![tumblr_lzps453JAA1roezubo1_r2_1280]({static}/images/tumblr_lzps453JAA1roezubo1_r2_1280.jpg)

Ich hatte schon immer mal Lust eine Camera Obscura zu bauen. Das ist
natürlich am beeindruckendsten wenn die Projektionsfläche groß genug
ist. Durch das Pinhole bekommt man ein super scharfes Bild der sich
draussen befindenden Stadt...wunderschön...

Hier wurde das von [Stenop.es](http://stenop.es/) realisiert. Sie haben
ganze Zimmer abgedunktelt, die Fenster verhüllt und ein Loch in die
Abdeckung gemacht. Dadurch fiel das Licht in die Wohnung mit einem
umgedrehten Bild der Stadt die sich vor dem Fenster abspielt.

{% vimeo 37102493 %}

