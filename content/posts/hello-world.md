Title: Hello World!
Slug: hello-world
Date: 2014-01-28 15:00
Tags: helloworld


Hello World! Dies ist ein kleines Blog in dem ich den technischen Wahnsinn niederschreiben möchte. Eigentlich wollte ich das ganze immer in mein normales [Blog](http://xsteadfastx.org) kippen. Habe mich aber dann doch dagegen entschieden. Dies ist dann das Ergebnis aus einer meiner ersten Python-Experimente. Das Blog wird mit [Pelican](http://docs.getpelican.com) erzeugt. Yeah. Na mal schauen...
