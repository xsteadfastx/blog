Title: Obey This Film
Date: 2014-06-04 09:12
Author: marvin
Category: Uncategorized
Tags: art, brettnovak, documentary, shepardfairey
Slug: obey-this-film

Hier eine kleine Dokumentation von [Brett
Novak](http://www.brettnovak.com/) über Shepard Fairey. Ich bin ja Fan.

{% youtube rcSBr4ZKmrQ   %}

([via](http://laughingsquid.com/obey-this-film-a-short-documentary-about-street-artist-shepard-fairey/))

