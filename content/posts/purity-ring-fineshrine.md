Title: Purity Ring - Fineshrine
Date: 2012-07-26 11:05
Author: marvin
Category: Uncategorized
Tags: music, purityring
Slug: purity-ring-fineshrine

![537273]({static}/images/537273.jpg)

Auch wenn das Video ein wenig WTF ist, ist "Fineshrine" eins meiner
liebsten Tracks auf dem Purity Ring Album "Shrines". Am Anfang war mir
das Album ein wenig zu düster... aber gerade mit dem Sommer hat sich die
Stimmung gelichtet...

{% vimeo 45677714 %}

