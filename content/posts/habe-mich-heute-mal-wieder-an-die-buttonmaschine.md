Title: Habe mich heute mal wieder an die Buttonmaschine...
Date: 2012-03-09 16:50
Author: marvin
Category: status
Tags: buttons, verblonshen
Slug: habe-mich-heute-mal-wieder-an-die-buttonmaschine

Habe mich heute mal wieder an die Buttonmaschine gesetzt und ein paar
Buttons für [Verblonshen.org](http://verblonshen.org) gemacht. Hier das
Ergebnis:

![DSC_0085_NEF]({static}/images/DSC_0085_NEF.jpg)

