Title: Die Lange Nacht über Hermann Melville
Date: 2017-06-06 11:00
Slug: die-lange-nacht-uber-hermann-melville
Tags: deutschlandfunk, book, documentary


![Moby-Dick]({static}/images/moby_dick.jpg)


Wie einmal mehr durch Zufall das Radio zum Zähneputzen angeschaltet und war mitten in der [langen Nacht über Hermann Melville](http://www.deutschlandfunk.de/lange-nacht-ueber-den-amerikanischen-schriftsteller-herman.704.de.html?dram:article_id=385764). Erst gestern las ich über Bob Dylans Rede zu seinem Literatur-Nobelpreis und die Vergleiche die er zwischen Musik und Literatur zog anhand von Moby-Dick (habe ich unten eingebunden). Wieder einmal muss ich mich beim Deutschlandfunk für seine tollen Produktionen bedanken. 

{% youtube 3Zf04vnVPfM %}
