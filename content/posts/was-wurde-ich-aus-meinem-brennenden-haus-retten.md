Title: Was würde ich aus meinem brennenden Haus retten?
Date: 2012-07-04 08:59
Author: marvin
Category: Uncategorized
Tags: blog, theburninghouse
Slug: was-wurde-ich-aus-meinem-brennenden-haus-retten

![Ben-R]({static}/images/Ben-R.jpg)

Die frage stellt das [Burning House](http://theburninghouse.com/). Es
ist ein Blog indem Fotos von Leuten veröffentlicht werden mit den
"Gegenständen" die sie aus ihrem brennenden Haus retten würden. Also
wahrscheinlich die wichtigsten Sachen die sie besitzen.

Da ich mir eh stundenlang "whats in my bag"-Fotostrecken anschauen
kann...kann ich ja nur begeistert sein. Es sind immer diese kleinen
Ideen und Blogs die ich so toll finde...i love it...

Das Foto zeigt übrigens nicht meine Sachen sondern die von Ben R...

