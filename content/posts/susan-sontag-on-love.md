Title: Susan Sontag on Love
Date: 2012-08-15 10:05
Author: marvin
Category: Uncategorized
Tags: art, susansontag
Slug: susan-sontag-on-love

![sontagonlove_brainpickings]({static}/images/sontagonlove_brainpickings.jpg)

Maria Popova von
[Brainpickings](http://www.brainpickings.org/index.php/2012/08/03/susan-sontag-on-love/)
hat Passagen über Liebe aus Susan Sontags Tagebücher gesammelt und von
[Wendy MacNaughton](http://wendymacnaughton.blogspot.de/) illustrieren
lassen. Wunderbar!

