Title: this israel
Date: 2011-12-02 10:44
Author: marvin
Category: post
Tags: israel, jerusalem
Slug: this-israel

{% vimeo 30861381 %}

Ich war jetzt fünf Jahre in Folge in Israel. Als ich das erste mal vor 5
1/2 Jahren dort war schwor ich mir auf dem Heimweg dieses Land einmal
pro Jahr zu besuchen. Eigentlich war mir sofort klar das es fast
unmöglich sei. Aus irgendeinem Grund war dies aber nicht so. Ich habe es
geschafft jedes Jahr meinen Fuß auf den heiligen Boden zu setzen. Meist
mit Freunden oder mit Familie. Es ist mit jedes mal eine Freude neue
Leute mit mir mitzunehmen und ihnen die Orte zu zeigen dir mir am
meisten bedeuten. Das letzte mal war ich Anfang Januar in Israel. Es war
das Highlight...ich war mit meiner jetzigen Frau dort. Das ist nun fast
ein Jahr her...langsam überkommt mich die Melancholie weil ich nicht
sagen kann wann es das nächste mal sein wird...

