Title: Moby und das Schlafen
Date: 2016-09-19 12:09
Slug: moby-und-das-schlafen
Tags: moby, music

![Moby]({static}/images/moby-calm_sleep.jpg)

Moby hat ein 4 Stündiges Ambient Werk freigegeben. Anscheinend hat Moby seit langem ein Problem mit dem schlafen. Diese Tracks sind für ihn eine Art Versuch zu Ruhe zu kommen.

> over the last couple of years i’ve been making really really really quiet music to listen to when i do yoga or sleep or meditate or panic. i ended up with 4  hours of music and have decided to give it away. you can download it for free here: [moby.com/la1](moby.com/la1) or stream it for free on soundcloud. or you can get it on spotify, apple music & itunes & deezer & tidal. it’s really quiet: no drums, no vocals, just very slow calm pretty chords and sounds and things for sleeping and yoga and etc. and feel free to share it or give it away or whatever, it’s not protected

{% soundcloud https://soundcloud.com/moby/sets/long-ambients1-calm-sleep?lf=c1ce5c176a7fdd95952f3462b27b4c9f %}

([via](http://www.openculture.com/2016/06/moby-lets-you-download-4-hours-of-ambient-music-to-help-you-sleep.html))
