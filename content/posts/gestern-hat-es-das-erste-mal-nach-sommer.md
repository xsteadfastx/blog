Title: Gestern hat es das erste mal nach Sommer...
Date: 2012-06-19 09:57
Author: marvin
Category: Uncategorized
Tags: nuernberg, photography
Slug: gestern-hat-es-das-erste-mal-nach-sommer

![christine.]({static}/images/7400125030_1e77af1ef9_b.jpg)

Gestern hat es das erste mal nach Sommer gerochen. Es war fast sogar zu
heiß. So heiß das ich Abends noch nicht mal laufen gehen konnte.
Spazieren an dem Dutzendteich ist aber auch ganz fein. Meine Kamera war
auch dabei. Und es wurde sogar eins der sehr seltenen Bildern von mir
gemacht...

![marvin.]({static}/images/7400124366_97c6503a16_b.jpg)

