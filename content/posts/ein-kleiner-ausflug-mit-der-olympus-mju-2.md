Title: Ein kleiner Ausflug mit der Olympus MJU 2
Date: 2014-01-09 11:11
Author: marvin
Category: Uncategorized
Tags: olympusmju2, photography, rodinal
Slug: ein-kleiner-ausflug-mit-der-olympus-mju-2

![Schlacke I]({static}/images/11834465264_526b212ba6_b.jpg)

Es war Silvester als ich durch Zufall an einem Fotoladen vorbei kam. Ich
dachte ich frage einfach mal nach einer gebrauchten Olympus MJU. Und
Überraschung, er hatte eine. Ich war sogar ein wenig aufgeregt. Danach
gleich mal einen Testfilm durchgezogen und entwickelt. Und schon beim
aufhängen fielen mir die Lightleaks auf. Und ich musste echt hin und her
überlegen ob ich sie zurückgebe oder damit lebe das auf manchen
Negativen Lightleaks sind. Im Endeffekt habe ich sie gegen eine Leica
Mini II getauscht. Und wenn ich mir ein paar der Bilder anschaue, frage
ich mich ob ich nicht doch lieber mit den Lightleaks leben hätte
sollen...

![Schlacke III]({static}/images/11834422983_2cbfb0ec22_b.jpg)

![Schlacke II]({static}/images/11834401953_7afacf9c4f_b.jpg)

![All Glory To Hypno-Snacks]({static}/images/11834493163_3902afbaf7_b.jpg)

![Happy 2014!]({static}/images/11834640604_227d533c66_b.jpg)

