Title: 29c3: Open Source Schlüssel und Schlösser
Date: 2013-01-11 11:24
Author: marvin
Category: Uncategorized
Tags: ccc, lockpicking
Slug: 29c3-open-source-schlussel-und-schlosser

![schluessel]({static}/images/schluessel.jpg)

Ich habe mich dazu entschieden mal ein paar Videos vom letzten Chaos
Communication Congress zu posten. Ich war zwar schon seit über 10 Jahren
nicht mehr auf dem Congress, verfolge ihn aber jedes Jahr über die
Streams und die nachträglich veröffentlichten Aufnahmen.

Heute einen Beitrag der
[Lockpicker](http://events.ccc.de/congress/2012/Fahrplan/events/5308.de.html):

> Was bedeutet das Zeitalter offener Designs für die Sicherheit von
> Schlössern? Zum Beispiel solchen, die auf eine geringe Verbreitung
> eines Schlüssels setzen? Ein Beispiel sind die sogenannten
> Hochsicherheitsversionen von Polizeihandschellen. Der Talk zeigt was
> (und wie) sich in diesem Bereich mit Lasercuttern und 3D Druckern
> erreichen lässt - sowie welche komplexeren Angriffsziele noch warten.
> Als Ausweg aus der Problematik kopierbarer Schlüssel gelten digitale
> Schlösser, aber sie kranken anders an offenen Quellen: sie haben
> keine! Im Rahmen eines Open Source Lock Projektes haben wir uns daher
> ein reflashbares Vorhängeschloss angesehen, doch noch ehe wir den
> Programmieradapter angeschlossen hatten fanden wir eine Schwachstelle
> der Hardware... Leider kein Einzelfall!

{% youtube 3JK3TO_crc8 %}

