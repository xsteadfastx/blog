Title: Hinter dem Namen  XXYYXX steckt der 16...
Date: 2012-06-08 10:15
Author: marvin
Category: Uncategorized
Tags: music, xxyyxx
Slug: hinter-dem-namen-xxyyxx-steckt-der-16

![469484_433709406642696_130263128_o]({static}/images/469484_433709406642696_130263128_o.jpg)

Hinter dem Namen "XXYYXX" steckt der 16 jährige Marcel Everett. Er macht
düstere, dahin plätschernde Bass-Lines die auf Wellen daher
reiten...oder sowas halt. Ich mag es...

{% youtube lG5aSZBAuPs %}

Sein Album gibt es auch für 1\$+ auf Bandcamp...zuschlagen...

<iframe width="300" height="410" style="position: relative; display: block; width: 300px; height: 410px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=2354567944/size=grande3/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[XXYYXX
by XXYYXX](http://xxyyxx.bandcamp.com/album/xxyyxx)</iframe>

