Title: Man braucht zwei Jahre um sprechen zu lernen...
Date: 2012-05-25 13:00
Author: marvin
Category: Uncategorized
Tags: ernesthemingway
Slug: man-braucht-zwei-jahre-um-sprechen-zu-lernen

Man braucht zwei Jahre um sprechen zu lernen und fünfzig, um schweigen
zu lernen.

<cite>Ernest Hemingway</cite>

