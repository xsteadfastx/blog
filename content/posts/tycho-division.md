Title: Tycho - Division
Date: 2016-08-17 09:47
Slug: tycho-division
Tags: tycho, music

![Division]({static}/images/tycho-division.jpg)

Das Richtige für einen Morgen an den man durch den herbstlichen Morgennebel geradelt ist, auch wenn es eigentlich die Mitte des Sommers sein sollte. Ein neuer Tycho Track. Toll!

{% soundcloud https://soundcloud.com/tycho/tycho-division %}
