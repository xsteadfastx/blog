Title: Memoryhouse - Dream Shake
Date: 2015-10-29 10:58
Slug: memoryhouse---dream-shake
Tags: memoryhouse, music

![Cover]({static}/images/memoryhouse_dream_shake.jpg)

Erst vor kurzem habe ich mich gewundert was aus Memoryhouse geworden ist. Und siehe da, es gibt sie noch. Ein Lied über den vergangenen Sommer.

{% soundcloud https://soundcloud.com/mmryhs/dream-shake %}
