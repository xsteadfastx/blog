Title: Tauben sind doch auch nur die besseren Fotografen
Date: 2012-10-08 13:06
Author: marvin
Category: Uncategorized
Tags: animals, photography
Slug: tauben-sind-doch-auch-nur-die-besseren-fotografen

![799px-Three_pigeons_with_cameras]({static}/images/799px-Three_pigeons_with_cameras.jpg)

Der Tausendsassa [Dr. Julius
Neubronner](http://de.wikipedia.org/wiki/Julius_Neubronner) kam auf die
Idee eine Kamera an Tauben zu schnallen. Noch bevor wir Nerds unseren
Katzen Katzen Kameras und GPS-Empfänger um den Hals gehängt haben.
Schönes Ding...

![Pigeon_wingtips]({static}/images/Pigeon_wingtips.jpg)  
([via](http://www.kraftfuttermischwerk.de/blogg/?p=42181))

