Title: Moose Blood - Gum
Date: 2015-10-06 09:42
Slug: moose-blood-gum
Tags: music, mooseblood, emo
Description: Emo Revival und so

![i'll keep you in mind, from time to time]({static}/images/moose_blood_time_to_time.jpg)

Und schon wieder eine kleine Neuentdeckung für mich im großen "Emo Revival". Ich habe mitlerweile die Theorie das es kein Revival ist sondern ich einfach nur super verpennt habe. Über [Moose Blood](http://mooseblooduk.com/) habe ich in einem super netten [Artikel auf Washed Up Emo](https://medium.com/@washedupemo/the-day-i-realized-my-chemical-romance-wasn-t-emo-5a5b7590a03f) gelesen. Da bekommt man gleich Phantom-Herzschmerz.

{% vimeo 127422795 %}
