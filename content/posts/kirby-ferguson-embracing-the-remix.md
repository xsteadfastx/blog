Title: Kirby Ferguson - Embracing The Remix
Date: 2012-09-05 11:42
Author: marvin
Category: Uncategorized
Tags: apple, kirbyferguson, remix, samsung, stevejobs, ted
Slug: kirby-ferguson-embracing-the-remix

![705157]({static}/images/705157.jpg)

Diesen TED-Talk hatte ich schon lange auf der Liste der zu verbloggenen
Dinge. Heute habe ich ihn mir nochmal angeschaut und gemerkt wie wichtig
es ist, dass mehr Menschen diese 10 Minuten sich anschauen. Es geht um
die Remix-Kultur oder besser gesagt: Ohne Remix keine Kultur. Eins
meiner liebsten Themen. Das es immer noch große Institutionen gibt die
denken das man Kultur, und natürlich andere Errungenschaften, aus sich
selber schöpft. Und wenn man ehrlich ist nimmt niemand Bob Dylan es
übel, dass er konsumierte, sich inspirieren lies und selber erschaffen
hat. Und der Krieg zwischen Apple und Samsung ist die Spitze des
Eisberges. Moneyquote von Steve Jobs:

> 1996: "We always have been shameless about stealing great ideas"  
>  2010: "I'm going﻿ to destroy Android Because it's a stolen product!"  
>  <cite>Steve Jobs</cite>

Es tötet Fortschritt...Kultur und Kunst. So einfach ist das...  
10 Minuten die sich lohnen.

{% youtube zd-dqUuvLk4 %}

