Title: Das Leben an der Zugstrecke
Date: 2012-01-11 14:50
Author: marvin
Category: post
Tags: nuernberg, thoughts, trains, travel
Slug: das-leben-an-der-zugstrecke

![CameraZOOM-20120111155208632]({static}/images/CameraZOOM-20120111155208632.jpg)  
Als wir uns diese Wohnung am Dutzendteich in Nürnberg angesehen haben
war uns klar das sie direkt an einer Zugstrecke lag. Der Vermieter
versicherte uns das er auch extra die besten schallgeschützen Fenster
eingebaut hat. Mir war nicht bewusst wie es sein wird direkt an so einer
Zugstrecke zu wohnen. Ich bin in Wolfsburg in einem sehr ruhigen Vorort
aufgewachsen. Ich kenne nichts anderes als ein Auto was ab und zu vorbei
fährt. Das was ich nach einem halben Jahr hier in dieser Wohnung gelernt
habe: Ich liebe es...ich liebe jeden einzelnen Zug der vorbei fährt.
Wenn man aus den Fenstern schaut und sich fragt wo dieser Zug hinfährt,
oder woher er kommt. Was passieren würde wenn wir jetzt in diesem Zug
sitzen würden. Romantische Hobbo Geschichten spielen sich jedes mal in
meinem Kopf ab. Es sind wie die Adern der Reise die durch Europa sich
ziehen und mit jedem Klappern der Züge wünsche ich mir mehr diese Welt
zu bereisen...

