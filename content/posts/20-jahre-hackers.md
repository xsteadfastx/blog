Title: 20 Jahre Hackers
Date: 2015-09-18 10:58
Slug: 20-jahre-hackers
Tags: hackers, movie
Description: Nun schon 20 Jahre Jubiläum vom Film Hackers

{% giphy xebOoxouppcGs %}

Unfassbare 20 Jahre ist es nun her das der Film [Hackers](https://de.wikipedia.org/wiki/Hackers_%E2%80%93_Im_Netz_des_FBI) erschienen ist. Es gibt ja das Phänomen Hacker-Film. Sehr wunderbar besprochen übrigens im dem [CRE202](http://cre.fm/cre202-hackerfilme) Podcast. Über Hackers scheiden sich die Geister. Natürlich ist alles sehr grafisch und überdreht. Aber für mich war es eine geniale Darstellung des Themas, welches seit meiner Kindheit ein absoluter Mittelpunkt in meinem Leben ist. Ich erinnere mich daran wie wir auf dem [Chaos Communication Congress 18c3](https://events.ccc.de/congress/2001/) ihn Nachts im großen Saal anschauten. Und der Soundtrack ist auch bombastisch. Höre ich ziemlich oft beim Programmieren. Hier noch ein toller Artikel von [csmonitor](http://passcode.csmonitor.com/hackers). Also Netflix anschmeißen und ihn nochmal schauen. Ach ja... HACK THE PLANET!!111!!einself

{% giphy 14kdiJUblbWBXy %}

{% youtube vCobCU9FfzI %}
