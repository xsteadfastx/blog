Title: Blackout Poems, die Zweite 
Slug: blackout-poems-die-zweite
Date: 2014-02-02 12:25
Tags: art, bild, random


![Post von Wagner]({static}/images/blackout-gold-deutschland.jpg)

Das Projekt macht seine Fortschritte. Ich habe den [Code](https://github.com/xsteadfastx/blackout_poems) soweit angepasst, dass er nur einmal pro Tag schaut ob es neue "Post von Wagner" gibt. Sonst holt er sich den Text aus einem pickle-file. Eine elegantere Methode fällt mir gerade nicht ein. Was ich noch beschlossen habe: Die nicht geschwärzten Worte werden nicht nur mit einem schwarzen Background versehen, alle Buchstaben werden jetzt auch durch "X"e ersetzt. Das ganze ist natürtlich auch ein kleines Selbstschutz. Schade eigentlich. 

Ich habe mal eine kleine Testinstallation unter [http://blackoutpoems.verblonshen.org](http://blackoutpoems.verblonshen.org) gemacht. Ich muss nochmal schauen wieso er ein paar Worte "zusammen klebt". Vielleicht nochmal regex überprüfen.  
