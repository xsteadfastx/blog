Title: Rosetta Code
Date: 2013-12-26 19:45
Author: marvin
Category: Uncategorized
Tags: coding, rosettastone
Slug: rosetta-code

[![cc-by-sa Hans Hillewaert]({static}/images/875px-Rosetta_Stone.jpg)](https://commons.wikimedia.org/wiki/File:Rosetta_Stone.JPG)

[Der Stein von Rosette](https://de.wikipedia.org/wiki/Stein_von_Rosette)
ist ein Stein auf dem in drei verschiedenen Sprachen das Priesterdekret,
als Ehrung des ägyptischen Königs Ptolemaios V. sowie seiner Frau und
deren Ahnen, eingemeiselt wurde. Dadurch wurde es möglich ägyptische
Hieroglyphen zu übersetzen. Dieses Prinzip wird nun auf
Programmiersprachen angewandt. Ein Wiki mit den Namen [Rosetta
Code](http://rosettacode.org/wiki/Rosetta_Code) in dem
Programmieraufgaben in den verschiedenen Sprachen gelöst werden. In
letzter Zeit programmiere ich wieder mehr. Natürlich auf unterstem
Niveau aber mit viel Spaß und viel mehr Verzweiflung. Ich stehe einfach
auf solche Projekte.

