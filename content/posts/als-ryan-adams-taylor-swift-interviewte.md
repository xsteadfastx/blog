Title: Als Ryan Adams Taylor Swift interviewte
Date: 2015-10-21 15:30
Slug: als-ryan-adams-taylor-swift-interviewte
Tags: ryanadams, taylorswift, music

{% giphy 895uU9amktvgc %}

Ryan Adams Coveralbum "1989" ist für mich keine große Überraschung. Er nahm das Pop Album der letzten 10 Jahre und verwandelte es in eine melancholisches Americana-Indie Meisterwerk. Ich bin ja seit der ersten Stunde ein Verfechter von Taylor Swift. Ja ich mag Pop Musik wenn sie so gut gemacht ist wie auf 1989. Hier interviewt Ryan Adams Taylor Swift. Und das ist von Beiden durchaus charmant. Mehr von den Beiden bitte.

{% youtube g3WlNjExg0E %}
