Title: Gibt es ein beklemmenderes Gefühl als der Moment...
Date: 2012-02-20 11:30
Author: marvin
Category: status
Tags: thoughts
Slug: gibt-es-ein-beklemmenderes-gefuhl-als-der-moment

Gibt es ein beklemmenderes Gefühl als der Moment an dem man sein, gerade
runtergefallenes, elektronisches Spielzeug aufheben muss? Es graust
einen die ganze Zeit davor...dann passiert es...was macht man nun? Es
muss ja auch mal passieren. Also rein statistisch. Man steckt den
Akkudeckel wieder drauf und macht einen ersten optischen Test...ach was
solls...sowas macht mich halt fertig...

