Title: Sufjan Stevens - Year of the Rabbit
Date: 2012-09-24 12:10
Author: marvin
Category: Uncategorized
Tags: ballett, music, sufjanstevens, yearoftherabbit
Slug: sufjan-stevens-year-of-the-rabbit

![sufjanballet]({static}/images/sufjanballet.jpg)

Was macht der gute Sufjan Stevens eigentlich nicht? Jetzt hat er ein
Ballett-Stück mit dem Namen "Year Of The Rabbit" vertont. Dies ist das
Promovideo für das Stück. Wunderbar!

{% youtube aKms2nc9JPo   %}
([via](http://www.itsnicethat.com/articles/year-of-the-rabbit))

