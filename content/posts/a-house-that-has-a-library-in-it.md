Title: A house that has a library in it...
Date: 2012-04-25 11:43
Author: marvin
Category: Uncategorized
Tags: books, plato
Slug: a-house-that-has-a-library-in-it

A house that has a library in it has a soul.

<cite>Plato</cite>

