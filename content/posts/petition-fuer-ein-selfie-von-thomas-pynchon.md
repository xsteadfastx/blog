Title: Petition für ein Selfie von Thomas Pynchon
Date: 2015-11-10 12:27
Slug: petition-fuer-ein-selfie-von-thomas-pynchon
Tags: thomaspynchon, books

![Thomas Pynchon]({static}/images/simpson_pynchon.jpg)

Aus der Abteilung "Petitionen die die Welt braucht". Eine [Petition](https://www.change.org/p/barack-obama-thomas-pynchon-must-post-a-selfie-on-instagram) an Barack Obama der sich darum kümmern soll, dass Thomas Pynchon einen Instagram Account eröffnent und Selfies posten soll. Wird sicher passiert... nicht.

> We the people request that Thomas Pynchon make an Instagram account and post no fewer than one selfie, ensuring that his legacy will live on in this digital age.
>
> The easiest way to accomplish this goal is for President Barack Obama to mandate the creation of a Pynchon instagram account, potentially titled "GravitysSelfie" or merely "Pynchon49" via executive order. An alternative method could potentially involve Pynchon willingly complying to appease his legion of loyal followers, hereby known as "Pynchies."
>
> Note: We implore Mr. Pynchon not to abuse social media by posting pictures of his food.
