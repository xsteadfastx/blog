Title: Purity Ring - Obedear
Date: 2012-04-24 10:20
Author: marvin
Category: Uncategorized
Tags: 4ad, music, purityring
Slug: purity-ring-obedear

Nachdem Memoryhouse bei Sub Pop Records gelandet sind freut es mich das
Purity Ring jetzt bei 4AD unter Vertrag sind. Die ganzen neuen Dreampop
und Chillwave Sachen finde ich eh super interessant und die Purity Ring
Songs die ich bis jetzt kannte um so mehr.

Zur Ankündigung des Albums haben sie einen Song den man umsonst
[runterladen](http://purityringsongs.com/) kann. Großartig!

{% youtube ETbGpGJNVLM %}

