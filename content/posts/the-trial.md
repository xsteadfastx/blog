Title: the trial
Date: 2011-12-16 12:02
Author: marvin
Category: post
Tags: franzkafka, literature, movies, orsonwelles
Slug: the-trial

Im Literaturverfilmungskurs nehmen wir gerade Orson Welles "The Trial"
vor. Beim googlen bin ich darauf gestossen das der Film ja unter Public
Domain steht. Leider gibt es ihn nicht auf archive.org. Aber natürlich
hat Youtube ihn. Ich glaube ich werde ihn mir nochmal anschauen. Leider
habe ich bis heute noch nicht das Buch gelesen...aber der Film gefällt
mir sehr.

{% youtube -jpvLGyN0vE %}

