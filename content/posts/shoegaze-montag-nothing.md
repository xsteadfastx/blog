Title: Shoegaze Montag: Nothing
Date: 2015-11-02 10:25
Slug: shoegaze-montag-nothing
Tags: music, nothing, shoegaze

![guilty of everything]({static}/images/nothing_guilty_of_everything.jpg)

Wie klingt es eigentlich wenn ein paar Hardcore Leute Shoegaze machen. [Nothing](http://www.bandofnothing.com/) wurde von Domenic Palermo gegründet. Seinerzeit in der Band Horror Show. Ich kann mich noch daran erinnern wie ich die damals abgefeiert habe. Nun also die musikalische Konsequenz mit diesen kaum zu überwindenden Soundwänden.

{% youtube 72ZGnPo-E4k %}

<iframe style="border: 0; width: 350px; height: 753px;" src="https://bandcamp.com/EmbeddedPlayer/album=2289343264/size=large/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="http://wearenothing.bandcamp.com/album/guilty-of-everything">Guilty of Everything by nothing</a></iframe>
