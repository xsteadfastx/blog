Title: Doku über das Album "The Lonesome Crowded West" von Modest Mouse
Date: 2013-04-12 13:12
Author: marvin
Category: Uncategorized
Tags: documentary, modestmouse, music
Slug: doku-uber-das-album-the-lonesome-crowded-west-von-modest-mouse

![Modest_Mouse-The_Lonesome_Crowded_West-Frontal]({static}/images/Modest_Mouse-The_Lonesome_Crowded_West-Frontal.jpg)

Pitchfork dreht ja sehr feine Dokumentationen über wichtige Alben. Dies
ist mir gestern über den Bildschirm geflackert. Modest Mouse großartiges
Album "The Lonsesome Crowded West". Modest Mouse sind eh gnadenlos
unterbewertet...

{% youtube G33AcZzZ0pM %}

