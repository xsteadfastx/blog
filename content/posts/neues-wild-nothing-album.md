Title: Neues Wild Nothing Album
Date: 2015-11-24 14:42
Slug: neues-wild-nothing-album
Tags: wildnothing, music

Erst vor ein paar Tagen habe ich mich gefragt was Wild Nothing wohl so treibt. Er war die letzten Jahre immer der Soundtrack für die Winterzeit. Und siehe da, pünktlich ist er mit einem neuen Album da. Hier schonmal zwei erste Tracks: "TV Queen" und "To Know You".

{% youtube 2XVwA4XCDeM %}

([via](http://pitchfork.com/news/62246-wild-nothing-announces-new-album-life-of-pause-shares-tv-queen-and-to-know-you/))
