Title: Kirsten Dunst - Aspirational
Date: 2014-09-24 09:31
Author: marvin
Category: Uncategorized
Tags: kirstendunst, matthewfrost, movies
Slug: kirsten-dunst-aspirational

Matthew Frost ist vor ein paar Monaten mit einem tollen Kurzfilm
(Werbung für ein Fashion Label) aufgefallen. In dem geht es um die
Modeblogger-Hipster-Filme die so kursieren. Viel Vintage und viel
Gepose. Nun hat er wieder einen kurzen Film gemacht. Diesmal mit Kirsten
Dunst.

{% vimeo 106807552 %}

{% vimeo 58933055 %}

