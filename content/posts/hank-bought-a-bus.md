Title: Hank bought a bus
Date: 2013-08-20 10:19
Author: marvin
Category: Uncategorized
Tags: fernweh
Slug: hank-bought-a-bus

Der Architekturstudent Hank Butitta hat einen alten Schulbus gekauft und
ihn in eine richtig schöne Wohnung umgebaut. Nun fährt er mit diesem Bus
durch Amerika und [bloggt](http://www.hankboughtabus.com/) darüber. Also
wenn das nicht ein Füllhorn für Fernweh ist, dann was? Ich bin
begeistert...

{% youtube yAEm382PeS4 %}

([via](http://www.thisiscolossal.com/2013/08/hank-bought-a-bus/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+colossal+%28Colossal%29))

