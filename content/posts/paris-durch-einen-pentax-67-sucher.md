Title: Paris durch einen Pentax 67 Sucher
Date: 2014-09-12 12:39
Author: marvin
Category: Uncategorized
Tags: paris, photography
Slug: paris-durch-einen-pentax-67-sucher

Ich liebe Fotografie, ich liebe Lichtschachtsucher, ich liebe Paris. Und
vor allem liebe ich Paris durch einen Lichtschachtsucher. Wir durften
diesen Sommer eine Woche in Paris verbringen. Was soll ich sagen? Seit
dem vergeht eigentlich kein Tag an dem ich nicht an diese Woche zurück
denke. Ich glaube es ist Liebe. Ich habe versucht viel zu fotografieren.
Paris durch Fotos. Nun haben Mathieu Maury und Antoine Pai dieses Video
erstellt in dem sie eine Videokamera über den Sucher einer Pentax 67
geknuppert haben. Pure Liebe!

{% vimeo 104088954   %}

([via](http://petapixel.com/2014/08/23/stunning-video-paris-captured-viewfinder-old-pentax-67/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+PetaPixel+%28PetaPixel%29))

