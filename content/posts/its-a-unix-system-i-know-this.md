Title: "It's a UNIX system! I know this!"
Date: 2014-03-03 15:58
Author: marvin
Category: Uncategorized
Tags: jurassicpark, movies, unix
Slug: its-a-unix-system-i-know-this

![itsaunixsystem]({static}/images/itsaunixsystem.jpg)

Ja ich stehe total auf Jurassic Park. Und schon als kleiner Knirps hatte
ich viele Sympathien für denn dicken "Nerd", der in der Jurassic Park
Kommandozentrale mit seinen vielen Monitoren und einem Unix-artigen
System, rum saß. Auch wenn er nicht gerade zu den Guten gehörte und als
Erster die Ohren anlegte. Der Ausspruch "It's a UNIX system! I know
this!", während Lex durch einen 3D-Dateimanager browsed (den gab es
übrigens [wirklich](https://en.wikipedia.org/wiki/Fsn)), ist ein riesen
Meme geworden.

Nun hat jemand dieses System, für den Browser, nach gebaut. Ich bin
sowas von Fan. [Viel Spaß](http://www.jurassicsystems.com/).

{% youtube dFUlAQZB9Ng %}

{% youtube RfiQYRn7fBg %}

