Title: Small Black - No Stranger
Date: 2013-08-05 12:38
Author: marvin
Category: Uncategorized
Tags: beforesunrise, music, smallblack
Slug: small-black-no-stranger

![smallblack_nostranger]({static}/images/smallblack_nostranger.jpg)

Ich wollte dieses Video schon seit einiger Zeit posten. Irgendwie kam
ich nicht dazu. Nachdem ich es jetzt gefühlte hundertmal gesehen habe,
erhöhte sich der Wille es doch hier abzuladen. "No Stranger" von Small
Black ist durchzogen von erstklassigen "[Before
Sunrise](https://de.wikipedia.org/wiki/Before_Sunrise)"-Momenten. Einen
Film den ich sehr liebe. Es ist Sommer. Also sollten wir unseren
romantischen Tagträumen, gegenüber der Alltagswut, doch den Vortritt
lassen.

{% vimeo 70916009 %}

