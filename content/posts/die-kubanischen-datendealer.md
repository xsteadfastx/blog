Title: Die kubanischen Datendealer
Date: 2015-09-25 13:15
Slug: die-kubanischen-datendealer
Tags: internet, cuba, documentary
Description: Dokumentation über kubanische Datendealer

Eine kleine Dokumentation von [Vox](https://www.vox.com/2015/9/21/9352095/netflix-cuba-paquete-internet) über kubanische Datenshops und Dealer. Wenn man ein Land ist welches von dem Internet fast komplett abgeschnitten ist (und ich rede nicht vom bayrischen Land), müssen sich die Piraten auf anderen Wegen austauschen. USB-Sticks und Festplatten stricken ein klassiches Turnshuh-Netzwerk.

{% youtube fTTno8D-b2E %}

([via](http://www.nerdcore.de/2015/09/24/inside-a-cuban-digital-pirate-market/))
