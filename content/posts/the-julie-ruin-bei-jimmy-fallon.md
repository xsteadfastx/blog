Title: the Julie Ruin bei Jimmy Fallon
Date: 2013-09-09 10:29
Author: marvin
Category: Uncategorized
Tags: jimmyfallon, kathleenhanna, letigre, music, thejulieruin
Slug: the-julie-ruin-bei-jimmy-fallon

Yeah. Endlich mal was neues von Kathleen Hanna. Sie hat ihr altes
Soloprojekt ausgepackt und mit Musikern bestückt. Le Tigre hat mich, bis
auf ein paar Ausnahmen, nie so richtig gekickt. The Julie Ruin machen
das schon eher. Na ich bin gespannt...

<iframe width="560" height="315" src="http://www.nbc.com/assets/video/widget/widget.html?vid=n40323" frameborder="0"></iframe>

