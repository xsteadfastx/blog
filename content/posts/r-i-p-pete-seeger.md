Title: R.I.P. Pete Seeger
Date: 2014-01-28 11:51
Author: marvin
Category: Uncategorized
Tags: folk, johnnycash, music, peteseeger
Slug: r-i-p-pete-seeger

![885px-Pete_Seeger_NYWTS]({static}/images/885px-Pete_Seeger_NYWTS.jpg)

Ach Pete, wir werden dich vermissen. Neben allen was du beeinflusst und
erschaffen hast, muss ich immer wieder an deinen Auftritt bei der Johnny
Cash Show denken. Eine Gänsehaut befällt mich da. Wie sehr die wirklich
Protestlieder mir gerade fehlen. Folk ist eigentlich nur noch ein
Kleidungsstil geworden. Ach Pete... wir werden dich vermissen...

{% youtube NHbTWJ9tjnw %}

{% youtube Eea-vGaBUXU %}

