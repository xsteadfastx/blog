Title: Ephraim Moses Lilien
Date: 2012-08-21 13:49
Author: marvin
Category: Uncategorized
Tags: art, ephraimmoseslilien, zionism
Slug: ephraim-moses-lilien

![6378084619_2777d54518_o]({static}/images/6378084619_2777d54518_o.jpg)

Ich bin gerade auf die grafischen Arbeiten von [Ephraim Moses
Lilien](http://de.wikipedia.org/wiki/E._M._Lilien) gestossen. Ein
Grafiker aus Galizien (die Heimat meiner Familie). Er gestaltete Bücher
und später auch viele Sachen für die Zionistische Bewegung. Berühmt
wurde er durch seine Gestaltung des Buches "[Lieder des
Ghetto](http://openlibrary.org/books/OL14020053M/Lieder_des_Ghetto.)" in
der deutschen Ausgabe. Was für tolle Arbeiten...

![6378157371_fcf1bc55a9_o]({static}/images/6378157371_fcf1bc55a9_o.jpg)

![6378162655_a27fe41bd2_o]({static}/images/6378162655_a27fe41bd2_o.jpg)

![6378177307_36ffdb6019_o]({static}/images/6378177307_36ffdb6019_o.jpg)

![6378132673_04c0483cab_o]({static}/images/6378132673_04c0483cab_o.jpg)

([via](http://www.johncoulthart.com/feuilleton/2012/08/21/ephraim-moses-liliens-lieder-des-ghetto/))

