Title: andreas elsholz - summer time
Date: 2012-03-07 13:19
Author: marvin
Category: post
Tags: andreaselsholz, dsds, music, summertime
Slug: andreas-elsholz-summer-time

Eine absolute Perle der deutschen Musik-Kultur: Andreas Elsholz mit
seinem Hit "Summer Time". Das Ding ist eine billige Kopie von DJ Jazzy
Jeff & The Fresh Prince aka Will Smith mit ultra schlechte Reimen wie:

> Wie ein Profi gleite ich über die Wellen. Die Leute fangen an, sich
> hinzustellen.

Absolutes Gold!

{% youtube 9TSxIvRCe0k %}

