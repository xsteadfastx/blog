Title: Man könnte es eine Liebesgeschichte nennen Erika Steinbach...
Date: 2012-03-16 10:32
Author: marvin
Category: status
Tags: erikasteinbach, politics, twitter
Slug: man-konnte-es-eine-liebesgeschichte-nennen-erika-steinbach

Man könnte es eine Liebesgeschichte nennen: Erika Steinbach, meine
liebste aller Geschichtsverdreher, und ich. Zumindestens auf
Twitter...und genau deswegen mag ich sie so sehr:

> Also liebe Piraten keine Angst vor ACTA. Besser selbst geistiges
> Eigentum schaffen. Lasst die Köpfe rauchen
>
> — Erika Steinbach (@SteinbachErika) [March 16,
> 2012](https://twitter.com/SteinbachErika/status/180581763167428609)

<p>
<script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> @[xsteadfastx](https://twitter.com/xsteadfastx) Nicht lachen sondern
> denken !
>
> — Erika Steinbach (@SteinbachErika) [March 16,
> 2012](https://twitter.com/SteinbachErika/status/180583094682791937)

<p>
<script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Und das ganze konnte ich dann nur mit extra viel Flausch ertragen:

> you made my day :-\* RT
> @[SteinbachErika](https://twitter.com/SteinbachErika):
> @[xsteadfastx](https://twitter.com/xsteadfastx) Nicht lachen sondern
> denken !
>
> — xsteadfastx (@xsteadfastx) [March 16,
> 2012](https://twitter.com/xsteadfastx/status/180583562003755009)

<p>
<script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Es ist halt immer sehr putzig wenn solche Politiktrolle versuchen
Politik zumachen und das im Internetz. Immer für ein paar Lacher gut das
ganze...

