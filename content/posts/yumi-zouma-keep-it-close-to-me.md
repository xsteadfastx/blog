Title: Yumi Zouma - Keep It Close To Me
Date: 2016-09-02 08:30
Slug: yumi-zouma-keep-it-close-to-me
Tags: music, yumizouma

Eigentlich denkt man sich, dass Dreampop langsam den Shark gejumped hat. Aber dann freut man sich einfach über dahintröpfelnde Beats mit Synthiteppich. Heute ist so ein Tag und da passt sich, dass Yumi Zouma gestern ihr erstes Video online gestellt hat.

{% youtube gZusfpGiIkQ %}
