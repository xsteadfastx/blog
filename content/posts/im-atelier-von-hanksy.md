Title: Im Atelier von Hanksy
Date: 2014-09-11 10:14
Author: marvin
Category: Uncategorized
Tags: art, banksy, hanksy
Slug: im-atelier-von-hanksy

Hanksy ist sowas wie die popkulturelle Antwort auf Banksy. Und wenn
Remixing bei Street-Art nicht passt... dann weiß ich auch nicht. Hier
ein kleiner Rundgang durch sein Atelier.

{% youtube C8Nl8yPYlvw   %}
([via](http://thehundreds.com/in-the-studio-hanksy/))

