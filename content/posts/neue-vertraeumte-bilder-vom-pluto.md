Title: Neue verträumte Bilder vom Pluto
Date: 2015-09-21 16:16
Slug: neue-vertraeumte-bilder-vom-pluto
Tags: pluto, nasa, newhorizons
Description: Neue Bilder von New Horizons

![Pluto]({static}/images/pluto_1.png)

NASA hat wieder einen neuen Schwung Bilder vom Pluto veröffentlicht. Dank der [New Horizons](https://www.nasa.gov/mission_pages/newhorizons/images/index.html) Sonde. Ach wie toll...

![Pluto]({static}/images/pluto_2.png)

![Pluto]({static}/images/pluto_3.png)

![Pluto]({static}/images/pluto_4.png)
