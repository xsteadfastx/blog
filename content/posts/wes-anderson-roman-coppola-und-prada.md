Title: Wes Anderson, Roman Coppola und Prada
Date: 2013-03-27 13:25
Author: marvin
Category: Uncategorized
Tags: giacoppola, movies, prada, romancoppola, sofiacoppola, wesanderson
Slug: wes-anderson-roman-coppola-und-prada

![prada-anderson]({static}/images/prada-anderson.jpg)

Eigentlich sind diese kleinen Werbesports nicht der Rede wert. Aber
dadurch das sie von Wes Anderson und Roman Coppola gedreht wurden,
müssen sie doch ihren Weg in mein Blog finden. Sofia Coppola und ihre
Schwester Gia drehen ja öfters Werbe und Kurzfilme für die Modebranche.
Manche sind wirklich gut und bestechen natürlich auch wieder durch die
Song-Auswahl. Naja.

{% youtube u9Le-lYPQHg %}

{% youtube TvmLxGb7iPI %}

{% youtube GTyWdtLzax4 %}

([via](http://iwatchstuff.com/2013/03/wes-anderson-and-roman-coppola-made-some.php))

