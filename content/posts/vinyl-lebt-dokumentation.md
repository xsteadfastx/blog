Title: Vinyl lebt! Dokumentation.
Date: 2015-07-06 17:27
Slug: vinyl-lebt-dokumentation
Tags: music, vinyl, documentary
Description: Dokumentation über die Rückker der Schallplatte


Gibt es eigentlich schönere Orte als Plattenläden? Ich habe eine tief emotionale Verbindung zu den Plattenläden die ich regelmäßig versuche zu besuchen. Ich freue mich jetzt schon das ich bald wieder in der Nähe vom [RipTide Cafe](http://www.cafe-riptide.de/) wohne. Es verging keine Woche in der ich nicht durch die Regale browste. Hier gibt es eine kleine ZDF Dokumentation über Vinyl. Stereotyp Hipsters included.

{% youtube sd3AYW8aX6c %}
