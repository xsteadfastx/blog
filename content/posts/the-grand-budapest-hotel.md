Title: The Grand Budapest Hotel
Date: 2013-10-18 18:11
Author: marvin
Category: Uncategorized
Tags: movies, wesanderson
Slug: the-grand-budapest-hotel

![Grand_Hotel]({static}/images/Grand_Hotel.jpg)

Kurz muss ich mich aus meinem Krankenbett melden um diesen Trailer zu
verbloggen. Und dieser hat gerade ein Lächeln auf mein Gesicht
geschneidert. Wes Anderson macht die Filme immer so wie Wes Anderson.
Ich möchte von ihm gar nicht mehr überrascht werden. Er soll einfach das
machen was er immer macht, und ich bin glücklich. Die Szenerie, die
Farben und der Cast. Perfekt.

{% youtube 1Fg5iWmQjwk %}

