Title: Blake Schwarzenbach und ein schöner Solo-Track
Date: 2015-09-15 11:57
Slug: blake-schwarzenbach-und-ein-schoener-solo-track
Tags: jawbreakr, blakeschwarzenbach, music
Description: Solo Track vom Jawbreaker Sänger

Blake Schwarzenbach, Teil der legendären Bands "Jawbreaker" und "Jets To Brazil" hat einen sehr tollen und athmosphärischen Song veröffentlicht. Klingt wie Emo aus seiner besten Zeit. Und das meine ich gänzlich positiv.

{% soundcloud https://soundcloud.com/nothingmorenothingless/blake-schwarzenbach-sanity-is-waiting %}

([via](http://www.brooklynvegan.com/archives/2015/07/jawbreakers_bla.html))
