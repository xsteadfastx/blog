Title: Bob Dylan und Allen Ginsberg am Grab von Jack Kerouac
Date: 2012-08-17 15:02
Author: marvin
Category: Uncategorized
Tags: allenginsberg, bobdylan, jackkerouac
Slug: bob-dylan-und-allen-ginsberg-am-grab-von-jack-kerouac

![Bob-Dylan-and-Allen-Ginsberg-at-Jack-Kerouacs-Grave-e1345094580496]({static}/images/Bob-Dylan-and-Allen-Ginsberg-at-Jack-Kerouacs-Grave-e1345094580496.jpeg)

Wie die beiden großen amerikanischen Poeten das Grab des großen Jack
Kerouac besuchen und aus seinem Werk Mexico City Blues vorlesen...

{% youtube cU3XJDm8R0w %}

([via](http://www.openculture.com/2012/08/bob_dylan_and_allen_ginsberg_visit_the_grave_of_jack_kerouac_1979.html))

