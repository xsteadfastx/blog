Title: libellen besuch
Date: 2011-10-20 10:48
Author: marvin
Category: post
Tags: animals, nikonf801, photography, rodinal, wolfsburg
Slug: libellen-besuch

Als wir letztes Wochenende in Wolfsburg waren sassen wir kurz vor der
Abfahrt noch am Teich meiner Eltern in der Sonne. Wir waren begeistert
von den ganzen Libellen die sich um das Wasser tummelten. Eine hat es
uns besonders angetan und fing sofort an sich auf Christine zu setzen.
Zum Glück konnte ich schnell meine Kamera holen und ein paar Fotos
schießen.

![img555]({static}/images/6263392718_108cb4f5a8_b.jpg)

![img562]({static}/images/6262869341_3caa16f7bb_b.jpg)

![img565]({static}/images/6263395224_36403f2d02_b.jpg)

![img569]({static}/images/6263395732_2ea1cfffcd_b.jpg)

