Title: Johnny Spreeblick interviewt Olli Schulz
Date: 2012-11-22 11:37
Author: marvin
Category: Uncategorized
Tags: fluxfm, johnnyhaeusler, ollischulz, spreeblick
Slug: johnny-spreeblick-interviewt-olli-schulz

[![cc by Franz Richter]({static}/images/980px-Olli_Schulz_0040.jpg)](http://de.wikipedia.org/w/index.php?title=Datei:Olli_Schulz_0040.JPG&filetimestamp=20120819204641)

Johnny Häusler von [Spreeblick](http://www.spreeblick.com/) hat den
legendären Olli Schulz für seine Sendung auf FluxFM
[interviewt](http://www.fluxfm.de/programm/fluxfm-spreeblick-mit-olli-schulz/).
Die Sendung von Johnny gefällt mir eh sehr gut. Endlich mal interessante
Interviews. Die Stimmung zwischen den Gesprächspartnern scheint wirklich
zu stimmen. So hört man einen etwas anderen Olli Schulz als zum Beispiel
bei Markus Lanz. Wie gesagt... ich bin ja Fan seit "Brichst du mir das
Herz, brech ich dir die Beine".

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F68001424&amp;show_artwork=true"></iframe>

