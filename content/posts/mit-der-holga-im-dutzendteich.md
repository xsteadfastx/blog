Title: Mit der Holga im Dutzendteich
Date: 2014-12-05 09:55
Author: marvin
Category: Uncategorized
Tags: holga, nuernberg, photography
Slug: mit-der-holga-im-dutzendteich

![Dutzendteich]({static}/images/15734450840_d57bd7eece_b.jpg)

Irgendwie liebe ich diese kleine Plastik-Kamera. Ich weiß es gibt Zeiten
in der sie nicht gerade meine erste Wahl ist um zu fotografieren. Hole
ich sie dann doch raus bin ich sehr zufrieden. Ich weiß nicht wieso,
doch der Herbst und vor allem der Winter ist für mich Holga-Zeit.
Vielleicht sind es die Grautöne der Natur und die Unschärfe der dumpfen,
melancholischen Stimmung.

![Dutzendteich]({static}/images/15734448590_22d7e961a0_b.jpg)

![Dutzendteich]({static}/images/15921737865_fd22f46864_b.jpg)

![Dutzendteich]({static}/images/15734500270_4c3d29288b_b.jpg)

![Dutzendteich]({static}/images/15734347368_e95ffbc705_b.jpg)

![Dutzendteich]({static}/images/15736076697_7a2fe91a68_b.jpg)

