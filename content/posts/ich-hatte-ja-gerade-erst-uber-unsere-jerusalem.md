Title: Ich hatte ja gerade erst über unsere Jerusalem...
Date: 2012-04-24 08:01
Author: marvin
Category: Uncategorized
Tags: fernweh, google, israel, jerusalem, streetview
Slug: ich-hatte-ja-gerade-erst-uber-unsere-jerusalem

Ich hatte ja gerade erst über unsere Jerusalem Field Recordings
gebloggt. Kurz danach lese ich das es jetzt Google Street View in der
Jerusalemer Altstadt gibt. Wie großartig ist das denn? Ab den ersten
virtuellen Schritten hat es ein ganzes Faß an Fernweh in mir aufgemacht.
Wie wunderbar das ganze auch ist...so sehr vermisse ich die Stadt...

Ich fasse es ja immer noch nicht das sie das Projekt in Deutschland
eingestellt haben...und das nur wegen ein paar BILD-Zeitungslesenden
Fortschrittsbremsen.

[Hier](http://goo.gl/DkW20) geht es los...

{% youtube qrNhV_JK0VI %}

