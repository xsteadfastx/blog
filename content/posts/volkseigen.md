Title: Volkseigen
Date: 2012-07-19 08:35
Author: marvin
Category: Uncategorized
Tags: documentary, germany, manuelgehrke
Slug: volkseigen

![508523]({static}/images/508523.jpg)

Mein Freund Manuel Gehrke hat eine kurze Dokumentation über den Zerfall
von drei Firmen in der ehemaligen DDR nach der Wende gedreht. Neben den
Interviews gibt es schöne, melancholische Bilder...

{% vimeo 45974513 %}

