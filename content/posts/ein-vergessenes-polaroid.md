Title: Ein vergessenes Polaroid
Date: 2012-10-24 17:06
Author: marvin
Category: Uncategorized
Tags: photography, polaroid
Slug: ein-vergessenes-polaroid

![wpid-IMG_20121024_1703271]({static}/images/wpid-IMG_20121024_1703271.jpg)

Ich muss ja zugeben das ich von Polaroid mittlerweile nicht mehr soviel
halte. Ich bin enttäuscht. Ich weiß das alles wieder neu entwickelt
werden muss. Die Chemie, die Verfahren. Ich weiß das alles seine Zeit
dauert. Umgerechnet habe ich mehr Geld für Polaroid Filme und Kameras in
den Sand gesetzt als ich das zugeben möchte. Drei von vier Bildern
werden nichts bzw. kommen sie nicht aus der Kamera. Das "Phänomen" habe
ich bei allen meinen vier Kameras und bei verschiedenen Filmen. Mir ist
das einfach zu teuer und die Enttäuschung zu groß. Es gibt Momente an
denen ich eine meiner Kameras raus hole und dem ganzen eine Chance
geben. Ab und zu freue ich mich dann darüber, dass ich wohl doch noch
einen letzten Funken Hoffnung in mir habe. Zum Beispiel wenn ich mich
über ein Ergebnis freue. Zuletzt bei diesem Foto.

