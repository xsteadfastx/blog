Title: Wir haben so viel Bücher zuhause das wir...
Date: 2012-04-25 12:36
Author: marvin
Category: Uncategorized
Tags: books, paris, shakespeareandcompany
Slug: wir-haben-so-viel-bucher-zuhause-das-wir

Wir haben so viel Bücher zuhause das wir ein Abkommen fassen mussten:
Jedes neue Buch was wir kaufen muss auch gelesen werden. Das ist die
letzte Notbremse...wir stecken voll mit Büchern und was ist das schlimme
daran? Ich liebe es. Ich liebe von Büchern umgeben zu sein.

Bücherläden und Bibliotheken strahlen etwas aus von dem ich gerne ein
Teil wäre. Zum Beispiel dieser Buchladen hier in Paris: Shakespeare and
Company. Genau da würde ich jetzt gerne sein...

{% youtube 1k1VV1j_k08 %}

