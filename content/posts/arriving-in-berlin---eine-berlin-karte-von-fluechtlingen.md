Title: Arriving in Berlin - Eine Berlin Karte von Flüchtlingen
Date: 2015-10-27 10:42
Slug: arriving-in-berlin---eine-berlin-karte-von-fluechtlingen
Tags: refugees, openstreetmap, berlin

[![Arriving in Berlin]({static}/images/arriving_in_berlin.jpg)](http://arriving-in-berlin.de/)

[Arriving in Berlin](http://arriving-in-berlin.de/) ist eine Karte auf der Basis von [OpenStreetMap](http://www.openstreetmap.org/) die von Flüchtlingen gestaltet werden. Das bedeutet, dass für Flüchtlinge wichtige Punkte markiert werden.

> Why maps? Because maps are a visual tool for sharing information with others. Because they can be produced by many people and combined together to tell stories about complex relationships. Because maps are never finished and only tell part of a story that can constantly be expanded upon. Because power exists in space, struggle exists in space and we exist in space. Because we cannot know where we are going if we do not know where we are from.

Dies ganze ist ein Projekt vom [Haus Leo](http://www.berliner-stadtmission.de/haus-leo) und dem [Haus der Kulturen der Welt](http://www.hkw.de/). Super Projekt!
