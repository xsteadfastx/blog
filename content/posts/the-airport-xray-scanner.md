Title: the airport xray-scanner
Date: 2011-08-05 12:47
Author: marvin
Category: post
Tags: holga, photography, rodinal, wolfsburg
Slug: the-airport-xray-scanner

the last time i visited israel i took a handful of 120 film with me. it
was ilford hp5 film. i read alot about taking film through the airport
security and most people said its no problem as long it dont get scanned
too often through the xray-scanner. as lazy as i am i shot like...one
film over the whole almost two week trip. so i had like 3-4 films which
got dragged through the scanner several times and wasnt even ready to
get developed. after i shot some films and developed them i saw some
strange stuff on the negatives...it was like the right side of the
negative was overexposed...and i couldnt even explain it. ok...i was
using a holga...but still...never saw something like this on my
negatives. at least...i really like the effect somehow...here are some
resaults...

![Holga 20110310]({static}/images/5514528836_9f64d39b49_b.jpg)

![Holga 20110310]({static}/images/5514528738_ca4be56ef8_b.jpg)

