Title: Chillwave ist ja gerade der heiße Scheiß Ich...
Date: 2012-05-16 10:05
Author: marvin
Category: Uncategorized
Tags: anne, music
Slug: chillwave-ist-ja-gerade-der-heise-scheis-ich

![dreampunx]({static}/images/dreampunx.jpg)

Chillwave ist ja gerade der heiße Scheiß. Ich stehe ja auch drauf und
so... aber die Dream Pop / Shoegaze Elemente sind doch oft zu wenig. Man
hat gar den Eindruck das viele Chillwave-Künstler diese Referenz gar
nicht kennen. Es gibt kaum eine aktuelle Band die annähernd so super wie
Slowdive ist.

Da freue ich mich dann besonders wenn so Bands wie ANNE mit ihrem Album
"Dream Punx" das ganze in eine Träumerei katapultieren... leider trauer
ich das ich nicht an die Vinyl hier in Deutschland komme... meh

Das ganze Album kann man sich bei "[Brooklyn
Vegan](http://www.brooklynvegan.com/archives/2011/11/anne_release_dr.html#more)"
anhören.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F1352049&amp;show_artwork=true&amp;secret_token=s-o7Qnh"></iframe>

{% youtube PLL2nYgp3es %}

{% youtube 4PJogkDV-mw %}

{% youtube mmy2P4XOFvU %}

