Title: Mutter Erde in einem Timelapse Video
Date: 2013-04-06 13:59
Author: marvin
Category: Uncategorized
Tags: nasa, space, timelapse
Slug: mutter-erde-in-einem-timelapse-video

![earth]({static}/images/earth.jpg)

Wie faszinierend der Blick auf die Erde ist. Vor allem wenn die
Aufnahmen nachträglich aufgemotzt wurden um sie in HD genießen zu
können. Danke Bruce W. Berry Jr.!

> All Time-lapse sequences were taken by the astronaunts onboard the
> International Space Station (ISS) (Thanks guys for making this
> available to the public for use!) All footage has been color graded,
> denoised, deflickered, slowed down and stabilized by myself. Clips
> were then complied and converted to 1080 HD at 24 frames/sec.  
>  Some interesting tidbits about the ISS. It orbits the planet about
> once every 90 mins and is about 350 Km/217 miles. The yellow/greenish
> line that you see over the earth is Airgolw.

{% vimeo 61487989 %}

([via](http://www.petapixel.com/2013/04/04/time-lapse-shows-what-earth-looks-like-to-astronauts-on-the-iss/))

