Title: alfred hitchcocks cameo compilation
Date: 2011-09-14 09:12
Author: marvin
Category: post
Tags: alfredhitchcock, movies
Slug: alfred-hitchcocks-cameo-compilation

Es ist ja bekannt das Alfred Hitchcock in vielen seiner Filme Cameo
Rollen hatte. Am Anfang weil das Geld für Statisten fehlte und später
weil es schon zur Pflicht geworden ist.

Hier ist eine nette Compilation mit vielen seiner Cameo Auftritten...

{% youtube OW6Rdiqlg2E %}

