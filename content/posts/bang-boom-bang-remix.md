Title: Bang Boom Bang Remix
Date: 2012-07-30 15:46
Author: marvin
Category: Uncategorized
Tags: bangboombang, movies, remix
Slug: bang-boom-bang-remix

![551570]({static}/images/551570.jpg)

Es gab Tage an denen mein Arbeitskollege und ich nur mit Zitaten aus
Bang Boom Bang kommuniziert haben. Neben Macho Man eigentlich der beste
deutsche Film. Und das trotz Til Schweigers Gastauftritt. Auf Spiegel
Offline gibt es nun ein Remix Video zu diesem großartigen Film... wie
kann man eigentlich leben ohne dieses Meisterwerk gesehen zu haben?

{% vimeo 46588921   %}

([via](http://www.spiegeloffline.de/2012/07/29/bang-boom-bang-remix-eine-hommage-an-einen-der-besten-deutschen-filme-aller-zeiten/))

