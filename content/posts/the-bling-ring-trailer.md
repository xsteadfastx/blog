Title: The Bling Ring Trailer
Date: 2013-03-11 13:46
Author: marvin
Category: Uncategorized
Tags: movies, sofiacoppola, theblingring
Slug: the-bling-ring-trailer

![Bling_ring]({static}/images/Bling_ring.jpg)

Da ist er endlich: Der Trailer zu dem neuen Sofia Coppola Film "The
Bling Ring". Der Film handelt über eine Gruppe von Teenagern die Häuser
von Prominenten einbrechen und sich selber den "Bling Ring" nennen. Das
ganze scheint so in der Art auch [wirklich passiert zu
sein](https://en.wikipedia.org/wiki/Bling_Ring). Ich bin ein wenig
skeptisch bei der Story, vertraue Sofia Coppola aber das sie daraus ein
optisches und akustisches Athmospheren-Feuerwerk abfeuert. Der Song aus
dem Trailer (Sleigh Bells - Crown on the Ground) ist schon mal super.

{% youtube BST3CCnP6uE %}

