Title: always trouble with harry
Date: 2011-08-22 11:05
Author: marvin
Category: post
Tags: alfredhitchcock, alwaystroublewithharry, movies
Slug: always-trouble-with-harry

![harry]({static}/images/troubleharry.png)

i kind of get addicted to Hitchcock movies. it's not just the stories
and the way he saw things...its his perfect views and aesthetics. he was
a nerd for sure...and if i see scenes like this i seriously get infected
by his style. my wife and me started this project...to watch all
Hitchcock movies...and for sure this is a big project. but i cant wait
to see them all.

we got tons of books about Hitchcock's art from the library. there is a
lot of stuff to discover.

