Title: No Joy
Date: 2012-06-26 09:35
Author: marvin
Category: Uncategorized
Tags: music, nojoy
Slug: no-joy

![330892_10150419557767457_1540356265_o]({static}/images/330892_10150419557767457_1540356265_o.jpg)

Auch wenn viele Musik gerade so daher wellenreitet gibt es nicht
wirklich gute Shoegaze Bands. Viele springen auf den Zug von Chillwave
auf...Shoegaze bleibt zurück. Und wenn ich dann mal eine Band finde, die
dann auch noch so super ist wie No Joy, dann gibt es an Feierei kein
halten mehr. Es handelt sich um das Duo Jasamine White-Gluz und Laura
Lloyd aus Kanada. Ich weiß das der Vergleich mit My Bloody Valentine
zwangsläufig mit Shoegaze kommt...aber in diesem Fall muss ich ihn
anbringen... ich freue mich darüber...

<iframe width="400" height="100" style="position: relative; display: block; width: 400px; height: 100px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=233492792/size=venti/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[Negaverse
by No Joy](http://nojoy.bandcamp.com/album/negaverse)</iframe>

{% youtube 3XE2Yl04Q4w %}

{% youtube 1JMYJPLHOcg %}

