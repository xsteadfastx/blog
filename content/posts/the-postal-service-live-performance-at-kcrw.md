Title: The Postal Service live performance at KCRW
Date: 2013-02-27 11:03
Author: marvin
Category: Uncategorized
Tags: jennylewis, music, thepostalservice
Slug: the-postal-service-live-performance-at-kcrw

![postalserviceradio]({static}/images/postalserviceradio.jpg)

Über das kommende The Postal Service Comeback kann man ja denken was man
will. Aber ihr Album ist immer noch einer der besten Soundtracks für
sommerliche Nächte. Wenn ich so darüber nachdenke habe ich bis jetzt
noch kein Livevideo von The Postal Service gesehen. Hier ist ein sehr
rares, und durch miesester Qualität bestechendes, Video von einem
Auftritt bei KCRW. Inklusive der wunderbaren Jenny Lewis natürlich...

{% youtube bRndmSOrN0Q %}

