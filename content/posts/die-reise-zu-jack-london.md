Title: Die Reise zu Jack London
Date: 2017-11-30 10:02
Slug: die-reise-zu-jack-london
Tags: jacklondon, books, documentary

Hier ein kleine Dokumentation zu Jack Londons 100 jährigen Todestag. Das [literaturcafe.de](http://www.literaturcafe.de/video-die-reise-zu-jack-london-auf-spurensuche-im-yukon-kanada/) ist aus diesem Grund seinen Spuren nach Alaska gefolgt.

{% youtube HCoXniwvpaI %}
