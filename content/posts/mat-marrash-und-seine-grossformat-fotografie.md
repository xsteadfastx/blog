Title: Mat Marrash und seine Großformat Fotografie
Date: 2016-06-07 12:47
Slug: mat-marrash-und-seine-grossformat-fotografie
Tags: documentary, photography, matmarrash

Kleine Dokumentation über den Fotografen [Mat Marrash](http://matmarrash.com/) über Großformat Fotografie. Und ich verstehe ihn total. Auch wenn ich keine Großformat-Kamera besitze. Meine digitale Kamera habe ich seit fast 2 Jahren nicht mehr benutzt.

{% youtube f0_Nax7PBtM %}
