Title: Godspeed - Written Word
Date: 2012-11-29 14:26
Author: marvin
Category: Uncategorized
Tags: emo, godspeed, straightedge, turningpoint
Slug: godspeed-written-word

![tfr014.turningpoint]({static}/images/tfr014.turningpoint.jpg)

Wenn ich darüber nachdenke muss ich sich damals ähnlich angefühlt haben.
Der Zeitpunkt als ich das erste mal diesen Song gehört habe. In meinen
Erinnerungen lag auch der erste Schnee. Ich war verhüllt in etwas was
sich wie Melancholie anfühlt. Und dann ist da dieser Song von der Band
die mal Turning Point war. Eine Straight Edge Band die alle Konsequenzen
weiterführten, sich in Godspeed umbenannten und eine wahnsinnige
Emo-Platte aufnahmen.

{% youtube H2Gx4Z7Ya2Y %}

