Title: You don't love someone for their looks or...
Date: 2012-04-10 09:41
Author: marvin
Category: Uncategorized
Tags: oscarwilde
Slug: you-dont-love-someone-for-their-looks-or

You don't love someone for their looks,  
or their clothes, or for their fancy car,  
but because they sing a song only you can hear.

<cite>Oscar Wilde</cite>

