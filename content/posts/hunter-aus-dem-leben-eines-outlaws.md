Title: Hunter - Aus dem Leben eines Outlaws
Date: 2015-04-21 09:10
Tags: huntersthompson, radio, documentary, wdr
Slug: hunter-aus-dem-leben-eines-outlaws


![Gonzo Fist]({static}/images/1000px-Gonzo.svg_.png)

Das WDR hat ein Hörspiel über [Hunter S. Thompson](https://de.wikipedia.org/wiki/Hunter_S._Thompson) veröffentlicht. Und eigentlich lohnt es sich immer etwas über den Erfinder des Gonzo Journalismus zu hören oder zu lesen. Neben seiner Rollings Stone Artikel habe ich auch [Fear and Loathing in Las Vegas](https://de.wikipedia.org/wiki/Fear_and_Loathing_in_Las_Vegas) gelesen. Und die Verfilmung kennt eh jeder und findet ihn großartig. Bald nehme ich mir mal seinen Hells Angels Bericht vor.

[Hier](http://podcast-ww.wdr.de/medstdp/fsk0/67/678146/wdrhoerspielspeicher_2015-04-14_hunterausdemlebeneinesoutlaws_wdr3.mp3) geht es zur MP3...

([via](http://www.nerdcore.de/2015/04/16/podcasts-hunter-s-thompson-techno-und-prof-trinkaus-annoying-things/))
