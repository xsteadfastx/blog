Title: Erster Test mit der Holga 120 WPC
Date: 2015-01-08 14:56
Author: marvin
Category: Uncategorized
Tags: holga, photography, rodinal
Slug: erster-test-mit-der-holga-120-wpc

Es musste einfach sein: Ich habe mir eine Holga 120 WPC gekauft. Das
ganze ist eine Pinhole Holga Kamera die sehr große Negative produziert.
Die Bilder die ich auf Flickr gesehen habe hatten mich sofort überzeugt.
Ich habe noch keine Efahrung mit Pinhole Kameras und war bereit ein
wenig damit rumzuprobieren. Alles nicht so einfach aber ich habe auf
jeden Fall Lust öfters mal mein Stativ mit zunehmen und länger zu
belichten als 1/60s...

![Holga 120 WPC]({static}/images/15566333504_2576982280_b.jpg)

![Holga 120 WPC]({static}/images/16187946812_1bf1427796_b.jpg)

![Holga 120 WPC]({static}/images/16162891806_d452a3fa85_b.jpg)

![Holga 120 WPC]({static}/images/16187931592_5b665fc096_b.jpg)

