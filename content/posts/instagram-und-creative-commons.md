Title: Instagram und Creative Commons
Date: 2012-09-07 11:59
Author: marvin
Category: Uncategorized
Tags: creativecommons, instagram, photography
Slug: instagram-und-creative-commons

![Toskana]({static}/images/7948557084_9c11941223_b.jpg)

Ja es ist verteufelt...und ja vielleicht auch teilweise zu Recht:
Instagram. Ich benutze es super gerne. Nicht nur weil ich die Filter
sehr gerne mag, sondern auch weil es endlich super einfach ist Bilder
auf Twitter und auf Facebook zu schieben. Leider fehlt es, wie fast bei
jedem Service im Internetz, eine Möglichkeit das von mir Veröffentlichte
unter einer Creative Commons Lizenz zu posten. Das übernimmt nun die
Website [i-am-cc.org](http://i-am-cc.org/instagram/marvinxsteadfast). Es
verbindet sich mit der API von Instagram und läßt eine Lizenz für die
letzten und zukünftigen Bilder aussuchen. Dieses Vorgehen gilt für die
nächsten drei Monate. Dann muss diese Entscheidung erneuert werden. Bis
jetzt kann man noch nicht in den Fotos suchen die unter der CC-Lizenz
stehen, die Features sollen aber bald alle kommen. So kann man, neben
Flickr, auch bei Instagram passende Bilder für sein Blog suchen und
benutzen. Creative Commons ist eh ein Projekt was man tatkräftig
unterstützen sollte. Also...anmelden...und die Freiheit genießen...

([via](http://www.petapixel.com/2012/08/24/i-am-cc-allows-instagram-users-to-share-under-a-creative-commons-license))

