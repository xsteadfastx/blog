Title: Ordos 100
Date: 2012-03-01 15:35
Author: marvin
Category: post
Tags: aiweiwei, art, china, documentary, ordos100
Slug: ordos-100

Ordos 100 ist ein Kunstprojekt aus China an dem viele internationale
Architekten sich beteiligen um eine Stadt mitten in das große Nichts der
Mongolei zu bauen. Das Projekt ist schon am entstehen und nun ist eine
Dokumentation auf Youtube aufgetaucht die es sich lohnt anzuschauen. Vor
allem weil Ai WeiWei Ordos 100 kuratiert. Das ganze ist nicht ganz
unumstritten. Vor allem das Engagement von Architekten in China. Was ich
mich frage ist...wieso ist es eigentlich nicht umstritten wenn westliche
Firmen ohne Ende Geld in ein menschenverachtenes Regime stecken. Ich
glaube weiterhin daran das Kunst was verändern kann...und ich meine
nicht die Wohnung schön zu dekorieren...

Mehr zu Ordos 100 gibt es
[hier](http://www.arte.tv/de/2105446,CmC=2105206.html) und
[hier](http://aiweiwei.blog.hausderkunst.de/?p=1824).

{% youtube vvEh_IoMuEg %}

