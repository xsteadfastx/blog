Title:  Loveless von My Bloody Valentine zählt zu...
Date: 2012-04-02 10:52
Author: marvin
Category: status
Tags: 8bit, music, mybloodyvalentine
Slug: loveless-von-my-bloody-valentine-zahlt-zu

"Loveless" von My Bloody Valentine zählt zu meinen absoluten
Lieblingsplatten. Wie sehr ich mich gefreut habe über das Re-Release auf
Vinyl. Und was noch verrückter klingt als das Original ist die 8Bit
Version von "Only Shadow"...

{% youtube ZhP_Usc9LKs %}

