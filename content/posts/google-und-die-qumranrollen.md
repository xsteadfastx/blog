Title: google und die qumranrollen
Date: 2011-09-26 15:27
Author: marvin
Category: post
Tags: google, israel, jerusalem, judaism
Slug: google-und-die-qumranrollen

![greatisaiahrolls]({static}/images/greatisaiahrolls.png)

Google hat zusammen mit dem Israel Museum in Jerusalem die berühmten
Schriftrollen aus Qumran aufwendig
[digitalisiert](http://dss.collections.imj.org.il/) und online gestellt.
Es ist möglich durch die Rollen zu blättern und bekommt per "mouse-over"
die englische Übersetzung der Stellen angezeigt.

Diese Rollen sind mit das Kostbarste was der Staat Israel besitzt. Es
wir noch die Arbeit von Generationen gebraucht um die Rollen vollständig
zu restaurieren und aufzuarbeiten. Viele Informatiker sitzen an dem
Problem viele kleine Schriftroll-Schnipsel einzuscannen und per Software
zusammen zusetzen. Desweiteren kämpft man mit Fehlern die die ersten
Archäologen gemacht haben. Zum Beispiel der Einsatz von Tesafilm zum
fixieren der einzelnen Stücke. Es ist auf jeden Fall spannend zu wissen
das sich in diesen ganzen Jahrtausenden die Schriften nicht verändert
haben.

Ich war jetzt schon viel oder fünf mal im Israel Museum und es
beeindruckt mich immer sehr wie sie es schaffen gerade biblische
Archäologie mit Kunst zu verbinden. Ich kann das Museum wirklich
empfehlen. Und das schon wegen der James Turrel Installation.

 

