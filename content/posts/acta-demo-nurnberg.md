Title: ACTA Demo Nürnberg
Date: 2012-02-13 13:00
Author: marvin
Category: post
Tags: acta, nuernberg, piraten
Slug: acta-demo-nurnberg

Am Samstag war es also soweit. Stop Acta Demo in Nürnberg. Ich bin schon
lange nicht mehr auf Demonstrationen gewesen. Ich kann gar nicht greifen
wieso das so ist. Nun war das Thema sehr akut und ich musste meinen
Unmut rauslassen. Das von der EU unterschriebene ACTA-Abkommen zur
Vergewaltigung von Inspiration. Laut Presse waren es ca. 1500 Menschen
die vor der Lorenzkirche demonstrierten. Neben vielen witzigen Meme
Schildern gab es aber auch viele Schaulustige die einfach nur genervt
haben. Vor allem besoffenes Gesocks welches sich in der Menge getummelt
hat. Leute...euch braucht kein Mensch! Saufen sollt ihr woanders. Eine
zweite Sache die ein großes Peinlichkeitsgefühl in mir aufstiegen ließ
war die Sprachanlage. Ok, die 4500 Facebook Zusagen sollte man nicht
ernst nehmen...aber das davon 1500 Menschen kommen war jetzt auch nicht
ganz abwegig. Geboten wurden zwei kleine Monitorboxen die zur
Beschallung des ganzen Lorenzkirchenplatzes reichen sollten. Hat nicht
geklappt. Ich stand in der Mitte und konnte kein einziges Wort
verstehen, und das lag nicht an einer lauten Menschenmenge. Trotzdem bin
ich froh das es einige geschafft haben an einem eisigen Tag sich für
mehrere Stunden in die Innenstadt zu stellen um gegen ACTA zu
demonstrieren...also alles gut...

![CameraZOOM-20120211155359125]({static}/images/CameraZOOM-20120211155359125.jpg)

