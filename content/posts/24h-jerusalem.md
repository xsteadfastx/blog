Title: 24h Jerusalem
Date: 2014-02-26 14:32
Author: marvin
Category: Uncategorized
Tags: arte, documentary, jerusalem, tv
Slug: 24h-jerusalem

![dsc\_0137.jpg]({static}/images/4166708116_104c9c0473_b.jpg)

Mir fällt es oft sehr schwer über Jerusalem zu schreiben. Zu viel denke
ich über diese Stadt nach, zu sehr sehne ich mich danach wieder dort hin
zu reisen. ARTE und der Bayrische Rundfunk haben am 12. April 2014 das
Projekt "24h Jerusalem". Es wird versucht einen kompletten Tag in
Jerusalem filmisch darzustellen und den in 24 Stunden zu senden. Ich bin
auf jeden Fall gespannt. Mal schauen wieviel "deutsche Israelkritik"
untergebracht wird.

Mehr Infos [hier](http://www.24hjerusalem.tv/).

