Title: AIR im Sydney Opera House
Date: 2017-06-27 11:39
Slug: air-im-sydney-opera-house
Tags: air, music, sydney

Holgi hatte es mal so oder so ähnlich ausgedrückt: "Jeder hat Moon Safari Zuhause ist aber scheiße". Dies trifft null auf mich zu. Und gerade wenn man diesen großen Freundeskreis Phoenix / Daft Punk / AIR betrachtet, sehe ich die Musik aller Beteiligten in einem größeren Kontext und lasse mich damit gerne beschallen. Ich sehe da Sommer und Sonne und Ruhige Tage des Dahinplätscherns. 

Hier ein Video von ihrem Auftritt im Sydney Opera House.

{% youtube ZP9SvddHdO0 %}

[via](http://www.kraftfuttermischwerk.de/blogg/air-live-at-sydney-opera-house-mai-2017/)
