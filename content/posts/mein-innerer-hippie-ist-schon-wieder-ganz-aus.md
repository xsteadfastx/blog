Title: Mein innerer Hippie ist schon wieder ganz aus...
Date: 2012-03-30 12:02
Author: marvin
Category: status
Tags: festivalofcolors, psychedelic
Slug: mein-innerer-hippie-ist-schon-wieder-ganz-aus

![festivalofcolors]({static}/images/festivalofcolors.jpg)

Mein innerer Hippie ist schon wieder ganz aus dem Häuschen. Das
[Festival of Colors](http://de.wikipedia.org/wiki/Holi) ist ein
indischer Feiertag der sich natürlich auch nach Amerika durchgegraben
hat. Und wenn man die Bilder sieht weiß man wieso. Das muss ein Spaß
sein...

{% youtube Hh-o5g4tLVE %}

