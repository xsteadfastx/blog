Title: Lizard Kisses - Close
Date: 2012-09-12 12:29
Author: marvin
Category: Uncategorized
Tags: lizardkisses, music
Slug: lizard-kisses-close

![269126554-1]({static}/images/269126554-1.jpg)

Gerade dieses wunderbare Video zu diesem wunderbaren Musiktitel
gefunden. Naja, ich glaube der Herbst-Blues ist da...

{% vimeo 48576188 %}

<iframe width="400" height="100" style="position: relative; display: block; width: 400px; height: 100px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/track=1681515404/size=venti/bgcol=FFFFFF/linkcol=4285BB/" allowtransparency="true" frameborder="0">[Close
by Lizard Kisses](http://lizardkisses.bandcamp.com/track/close)</iframe>

