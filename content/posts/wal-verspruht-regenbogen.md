Title: Wal versprüht Regenbogen
Date: 2012-06-22 07:00
Author: marvin
Category: Uncategorized
Tags: animals, rainbow, whale
Slug: wal-verspruht-regenbogen

![404942]({static}/images/404942.png)

Manche Sachen sind einfach zu wunderbar. Zum Beispiel wenn sich das von
einem Wal gesprühte Wasser zu einem Regenbogen verwandelt...

{% youtube 6BD7uGxDCfI   %}

([via](http://www.doobybrain.com/2012/06/22/whale-blows-a-rainbow-in-nova-scotia/))

