Title: Rückwärts durch die Straßen von Jerusalem
Date: 2013-03-19 13:11
Author: marvin
Category: Uncategorized
Tags: israel, jerusalem
Slug: ruckwarts-durch-die-strasen-von-jerusalem

Ein Video was mein Fernweh auf die absolute Spitze schiebt. Jetzt war
ich doch schon eine längere Zeit nicht mehr in Israel und vor allem
nicht in mein geliebtes Jerusalem. Ich verkürze mir die Zeit mit Videos
wie diesem hier...

{% youtube X6jprOZ29wY   %}

([via](http://www.petapixel.com/2013/03/18/a-mind-bending-reversed-stroll-through-downtown-jerusalem-shot-in-one-take/))

