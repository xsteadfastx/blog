Title: Germany Germany - With You
Date: 2012-07-06 08:36
Author: marvin
Category: Uncategorized
Tags: germanygermany, music
Slug: germany-germany-with-you

![artworks-000026082620-xla3fb-original]({static}/images/artworks-000026082620-xla3fb-original.jpg)

[Germany Germany](http://grmnygrmny.com/) hat jetzt die erste Single von
seinem neuen Album veröffentlicht... "With You" heißt der A-Seiten
Track. Der zweite Song ist eine Cover Version von "Real Hero". Gerade
auch dieses Cover hat es in sich. Ich bin total hin und weg. Beide
Tracks hat er mit der Sängerin Kotomi produziert.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F2186999&amp;show_artwork=true"></iframe>

