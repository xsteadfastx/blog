Title: Shoegaze Freitag: Wildhoney
Date: 2015-06-12 10:45
Slug: shoegaze-freitag-wildhoney
Tags: music, shoegaze, wildhoney


Gerade ist soviel los und die Musik von Wildhoney trifft mich irgendwie. Irgendwas zwischen Shoegaze und Pop. Ach was solls... einfach toll.

{% youtube Dgv4zLDFjLc %}

<iframe style="border: 0; width: 400px; height: 241px;" src="https://bandcamp.com/EmbeddedPlayer/album=2989186570/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="http://wildhoneysound.bandcamp.com/album/seventeen-forever">Seventeen Forever by Wildhoney</a></iframe>

<iframe style="border: 0; width: 400px; height: 472px;" src="https://bandcamp.com/EmbeddedPlayer/album=362901220/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="http://derangedrecords.bandcamp.com/album/sleep-through-it-2">Sleep Through It by WILDHONEY</a></iframe>
