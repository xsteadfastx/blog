Title: Apollo Missions
Date: 2015-10-28 15:28
Slug: apollo-missions
Tags: nasa

Ich hatte ja schon von dem Apollo Fotoarchive [geschrieben]({static}/posts/die-nasa-hat-ihr-apollo-fotoarchiv-online-gestellt.md). Viele dieser Aufnahmen sind Fotoreihen die man wunderbar Stop-Motion artig aneinander kleben kann. Dies dachte sich auch [harrisonicus](https://vimeo.com/user4617609).

> I was looking through the Project Apollo Archive (flickr.com/photos/projectapolloarchive/) and at one point, I began clicking through a series of pics quickly and it looked like stop motion animation. So, I decided to see what that would look like without me having to click through it. Enjoy!

{% vimeo 141812811 %}
