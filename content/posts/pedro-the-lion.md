Title: Pedro the Lion
Date: 2013-02-26 11:55
Author: marvin
Category: Uncategorized
Tags: emo, music, pedrothelion
Slug: pedro-the-lion

[![cc-by Christian Kock]({static}/images/93993275_96e6959d17_b.jpg)](https://secure.flickr.com/photos/konzerte/93993275/)

Fast mein ganzes Leben hatte ich diesen Traum in meinem Kopf. Weglaufen.
Vor zehn Jahre war es am schlimmsten. Ich kam gerade aus Kalifornien
wieder und sah dort all das Leben wie ich es aus den 80er Teeniefilmen
kannte und liebte. Eine völlige Illusion. Trotzdem war es immer eine
unrealistische Option in meinen Gedanken. Natürlich würde ich surfen,
eine neue Band gründen. Die hätte wie Pedro the Lion klingen müssen.
Genauso...

{% youtube BLWDuW3Zzvo %}

