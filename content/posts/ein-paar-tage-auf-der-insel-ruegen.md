Title: Ein paar Tage auf der Insel Rügen
Date: 2016-01-05 19:33
Slug: ein-paar-tage-auf-der-insel-ruegen
Tags: ruegen, photography

![Rügen]({static}/images/23826092909_d246ef38de_b.jpg)

Wir haben ein paar Tage auf der Insel Rügen verbracht. Für mich so etwas wie eine zweite Heimat. Es war kalt und genauso schön.

![Rügen]({static}/images/24111298331_f5fe23340c_b.jpg)

![Rügen]({static}/images/24167806056_0bff7f9814_b.jpg)

![Rügen]({static}/images/24167802266_8d1a750ea6_b.jpg)

![Rügen]({static}/images/24167714876_da802fa583_b.jpg)

![Rügen]({static}/images/24193854875_9b06e0ed4e_b.jpg)

![Rügen]({static}/images/23567037163_9f4127684d_b.jpg)

![Rügen]({static}/images/23898195490_2b531f05d1_b.jpg)

![Rügen]({static}/images/24193793945_d506746482_b.jpg)

![Rügen]({static}/images/23825988679_69041956b5_b.jpg)

![Rügen]({static}/images/24111199271_7986c100e2_b.jpg)

![Rügen]({static}/images/24111200701_0d365415bd_b.jpg)

![Rügen]({static}/images/24193797835_663a684ac0_b.jpg)

![Rügen]({static}/images/23825992579_9e9af7fe46_b.jpg)
