Title: Gerettete Filme aus dem zweiten Weltkrieg
Date: 2015-01-20 15:49
Author: marvin
Category: Uncategorized
Tags: photography
Slug: gerettete-filme-aus-dem-zweiten-weltkrieg

Vor einigen Jahren fand mein Vater einen unentwickelten Rollfilm in
einer Kameratasche seiner Kamerasammlung. Irgendwann wollte ich diesen
sehr alt-wirkenden Film entwickeln. Ich setzte auf Rodinal, den
Entwickler meiner Wahl. Ich dachte das es bestimmt funktioniert. Es ist
ja ein Entwickler der auf einer alten Rezeptur basiert. Raus kam nur
Matsch. Die Emulsion hatte sich halb gelöst und es war ein Geschmiere
auf dem man nichts mehr erkennen konnte.

Anscheinend hat es bei diesem Projekt gut geklappt. Es ist das [Rescued
Film Project](http://www.rescuedfilm.com/). Dort werden alte
unentwickelte Filme entwickelt und gescannt. In diesem Beispiel über 30
Rollen Filme aus dem zweiten Weltkrieg. Eine tolle Idee.

Einziger Kritikpunkt bleibt das Copyright. Dies beansprucht das Projekt
für sich selber. Da sie nicht der Urheber sind und der Fotograf nicht
bekannt ist, finde ich es fragwürdig. Naja... trotzdem schön

{% vimeo 116735360   %}

([via](http://www.kraftfuttermischwerk.de/blogg/31-erst-jetzt-entwickelte-filmrollen-amerikanischer-soldaten-aus-dem-2-weltkrieg/))

