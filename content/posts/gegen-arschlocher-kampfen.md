Title: Gegen Arschlöcher kämpfen
Date: 2012-09-17 13:55
Author: marvin
Category: Uncategorized
Tags: boehseonkelz, ollischulz, rocheundboehmermann, tv, zdfkultur
Slug: gegen-arschlocher-kampfen

![760215]({static}/images/760215.jpg)

Gestern hat mein persönlicher Held Olli Schulz bei Roche & Böhmermann
mal so richtig Dampf abgelassen. Ich konnte meinen Ohren kaum glauben
und bin so froh das sich jemand die Mühe gemacht hat diesen Ausschnitt
bei Youtube hochzuladen. Es gibt Sachen die müssen einfach gesagt
werden...

{% youtube i_KO1h3bRKs %}

