Title: Baths - Miasma Sky
Date: 2013-03-20 11:07
Author: marvin
Category: Uncategorized
Tags: baths, music
Slug: baths-miasma-sky

![artworks-000042238035-sov3w6-t500x500]({static}/images/artworks-000042238035-sov3w6-t500x500.jpg)

Schaut mal aus dem Fenster. Es ist grau und wo es nicht grau ist liegt
der weiße Beweis das der Frühling nicht auftauchen mag. Braucht also
jemand einen Soundtrack für diese Tage? Vielleicht die neue Single von
Baths. Ich freue mich schon sehr auf sein neues Album.

<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/81970529&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>

([via](http://www.brooklynvegan.com/archives/2013/03/baths_releasing.html))

