Title: Wenn wir bedenken dass wir alle verrückt sind...
Date: 2012-05-09 12:01
Author: marvin
Category: Uncategorized
Tags: marktwain
Slug: wenn-wir-bedenken-dass-wir-alle-verruckt-sind

Wenn wir bedenken, dass wir alle verrückt sind, ist das Leben erklärt.

<cite>Mark Twain</cite>

