Title: Wie die Disketten von Andy Warhol aufgearbeitet wurden
Date: 2014-05-13 11:15
Author: marvin
Category: Uncategorized
Tags: andywarhol, art, atari
Slug: wie-die-disketten-von-andy-warhol-aufgearbeitet-wurden

Vor einiger Zeit ging durchs Netz, dass Disketten von Andy Warhol
gefunden und "restauriert"
[wurden](http://boingboing.net/2014/04/24/lost-warhol-originals-extracte.html).
Andy Warhol hatte im Zuge einer Promokampagne einen Amiga 1000 gebutzt
um Bilder am Computer zu zeichnen. An diesem Beispiel wird die gesamte
Tragik von digitalen Formaten klar. Disketten zerfallen und Formate sind
nicht mehr lesbar. Zum Glück gibt es findige Nerds die uns diese
Kunstwerke sichern konnten.

{% vimeo 92583299 %}

