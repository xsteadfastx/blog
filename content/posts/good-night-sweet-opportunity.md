Title: good night sweet opportunity
Date: 2019-02-14 08:25
Slug: good-night-sweet-opportunity
Tags: nasa, mars

![opportunity]({static}/images/51910968_1974626822587283_4948178619961704448_n.jpg)

[Die NASA hat gestern versucht](https://www.heise.de/newsticker/meldung/Mars-Rover-Opportunity-Letzter-Kontaktversuch-der-NASA-4307493.html) das letzte mal Kontakt zu dem Mars-Rover Opportunity aufzunehmen. Diese sind spektakulär gescheitert, da der Rover nun schon viel länger auf dem Mars umher geistert als die geplanten 90 Tage. Diese erreichten Ziele lenken einen doch schon ziemlich erfolgreich von den Problemen unseres Planeten ab und wenn man stark glaubt, geben sie sogar ein wenig Hoffnung.

Also good night sweet prince...
