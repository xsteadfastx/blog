Title: Inside Biosphere 2
Date: 2018-01-19 11:33
Slug: inside-biosphere-2
Tags: biosphere, documentary

Ich lese gerade "Terranauten" von T.C. Boyle. Diesen Roman schrieb er auf der Basis von den Ereignissen der [Biosphere 2](https://de.wikipedia.org/wiki/Biosph%C3%A4re_2) und baut darauf ein völlig neues Experiment auf. Das Thema Biosphere 2 taucht immer mal wieder in mein Interesse ab. Irgendwie faszinierend, eine zweite Erde, grandios gescheitert. Technisch wie auch menschlich. Was würde ich geben, sie mal zu besuchen.

{% youtube -yAcD3wuY2Q %}
