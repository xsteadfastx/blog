Title: 3 RPM Record
Date: 2012-03-29 09:40
Author: marvin
Category: post
Tags: jackwhite, vinyl
Slug: 3-rpm-record

Jack White hat zum Jubiläum seines Labels "Third Man Records" eine
Platte rausgebracht auf der alle bisher erschienenen Singles gepresst
sind. Wie kann das denn gehen? Man muss die Platte auf 3 RPM
hören...also unhörbar...auch wenn es einige Leute auf Youtube probieren
diese Platte in der richtigen Geschwindigkeit abzuspielen...viel Spaß...

{% youtube pZWgdg2jayw   %}

([via](http://www.rollingstone.com/music/news/jack-whites-third-man-creates-3rpm-record-20120328))

