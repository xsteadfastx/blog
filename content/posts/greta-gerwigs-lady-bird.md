Title: Greta Gerwig's Lady Bird
Date: 2017-09-07 16:15
Slug: greta-gerwigs-lady-bird
Tags: movies, gretagerwig

![Ladybird]({static}/images/ladybird.jpg)

Jemand sagte mir, nur einige Tage her, dass es schwer sei die Filme mit Greta Gerwig aus einander zu halten. Was in vielen Fällen nicht so nett gemeint ist, macht das Schaffen von Greta Gerwig nur noch besser. Sei es als Schauspielern, als Schauspielern ihrer eigenen Filme oder nur als Re­gis­sein und Drehbuchautorin. Ok, im letzten Fall warte ich noch auf eine Bestätigung meiner steilen These. Dies wird der Film "Lady Bird" nicht enttäuschen. Dies bilde ich mir zumindestens ein, nach dem gesehenen Trailer.

Wie kommt es eigentlich, dass es oft so einfach scheint sich mit Heranwachsenen zu identifizieren. Es sind vielleicht die Probleme die man selber gefühlt hat, heute noch immer, als Schatten, fühlt und nun vielleicht sogar andere Lösungen und Wege vor Augen hätte. Nun ist es egal... man ist erwachsen und weint den Emotionen von damals hinterher.

{% youtube cNi_HC839Wo %}
