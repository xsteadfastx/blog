Title: Beat Generation: Kerouac Ginsberg Burroughs
Date: 2013-09-19 14:05
Author: marvin
Category: Uncategorized
Tags: allenginsberg, arte, beatculture, books, documentary, jackkerouac, williamsburroughs
Slug: beat-generation-kerouac-ginsberg-burroughs

![Kerouac_by_Palumbo]({static}/images/Kerouac_by_Palumbo.jpg)

Auf ARTE lief gestern eine Dokumentation über die drei großen Autoren
der "Beat Generation". Ich weiß nicht wieso, die Zeit mit ihrer
Literatur und Kunst ist faszinierend. Diese Zeit des spürbaren Aufbruchs
in eine neue Zeit. Das Brechen von Konventionen. Und dann sind da die
endlos wirkenden Reisen durch Amerika und Europa. Unterwegs sein und
schreiben.

<p>
<script type="text/javascript" src="http://www.arte.tv/playerv2/embed.php?json_url=http://arte.tv/papi/tvguide/videos/stream/player/D/047586-000_PLUS7-D/ALL/ALL.json⟨=de_DE&amp;config=arte_tvguide"></script>
</p>

