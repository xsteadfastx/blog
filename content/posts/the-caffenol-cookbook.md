Title: The Caffenol Cookbook
Date: 2012-11-26 12:13
Author: marvin
Category: Uncategorized
Tags: caffenol, photography
Slug: the-caffenol-cookbook

[![cc by MrFredFedora]({static}/images/6077697151_26018b584b_b.jpg)](http://www.flickr.com/photos/fredfedora/6077697151/)

Die Hoffnung stirbt zu letzt. Es passieren bei analogen Fotografieren
immer wieder großartige Sachen. Sachen die die Plastik-Photoshop-Gang
einfach oft nicht nachvollziehen kann. Und aus genau dieser Abteilung
gibt es heute das Caffenol-Cookbook. Ja richtig! Entwickeln mit Kaffee.
Als jemand der sehr gerne analog fotografiert und nach jeder Flasche
Rodinal Angst hat, das es die letzte sein wird, freut man sich über die
ganzen Möglichkeiten die das Entwickeln bietet. Caffenol habe ich schon
lange auf meiner List mit Sachen die ich ausprobieren möchte. Schön das
es jetzt dieses Ebook mit Beispielen und Anleitung gibt.

![cover]({static}/images/cover.jpg)

<http://www.caffenol-cookbook.com/>

([via](http://caffenol.blogspot.de/2012/11/the-caffenol-cookbook-and-bible.html))

