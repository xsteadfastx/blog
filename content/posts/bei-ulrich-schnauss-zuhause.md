Title: Bei Ulrich Schnauss Zuhause
Date: 2014-11-20 11:44
Author: marvin
Category: Uncategorized
Tags: music, ulrichschnauss
Slug: bei-ulrich-schnauss-zuhause

Das Wetter draussen zwingt einen quasi die Musik von Ulrich Schnauss
aufzulegen. Aber wie sieht es eigentlich bei ihm Zuhause im Studio aus?
Wie großartig ist seine Synthie-Sammlung?

{% vimeo 22386724 %}

