Title: im herbst
Date: 2011-12-01 10:57
Author: marvin
Category: post
Tags: holga, nuernberg, photography, rodinal
Slug: im-herbst

Ich bin ziemlich begeistert was die Holga leisten kann wenn man einfach
mal einen Blitz mit ihr benutzt. Das ist das erste mal das ich auf TRI-X
400 schieße. Ich glaube ich mag den Film ein wenig mehr als den Ilford
HP5...

![img655]({static}/images/6435554013_05091761f6_b.jpg)

![img656]({static}/images/6435556465_48a86f7e38_b.jpg)

![img657]({static}/images/6435558395_91b5450c5d_b.jpg)

![img662]({static}/images/6435560805_dbc1ecd503_b.jpg)

![img663]({static}/images/6435563069_473ee67d7b_b.jpg)

![img664]({static}/images/6435565383_51e4581c07_b.jpg)

