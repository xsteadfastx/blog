Title: Tinte in Wasser in 4K
Date: 2013-02-05 13:58
Author: marvin
Category: Uncategorized
Tags: 4k, ink, psychedelic, water
Slug: tinte-in-wasser-in-4k

![4kfarbeinwasser]({static}/images/4kfarbeinwasser.jpg)

Wie schön farbige Tinte in Wasser aussieht wenn man es in 4K-Auflösung
aufnimmt und in psychedelischer Slowmo abspielt... i like it.

{% youtube k_okcNVZqqI   %}
([via](http://www.kraftfuttermischwerk.de/blogg/?p=48488))

