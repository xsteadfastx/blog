Title: No Joy - Lunar Phobia
Date: 2013-03-20 16:05
Author: marvin
Category: Uncategorized
Tags: music, nojoy
Slug: no-joy-lunar-phobia

[![cc-sa Jason Persse]({static}/images/5116716118_87459a896a_b.jpg)](https://secure.flickr.com/photos/jasonpersse/5116716118/in/photostream/)

Nachdem das Jahr von dem neuen My Bloody Valentine Album eingeleitet
wurde, kann es nur ein großartiges Shoegaze/Dreampop Fest werden. Dabei
dachte ich das sich das Genre tot gespielt hat. Pustekuchen! Ich freue
mich auf das neue No Joy Album, um so mehr nachdem ich diese erste
Single gehört habe.

{% youtube X1sWbcgBe1g %}

([via](http://pitchfork.com/reviews/tracks/14963-no-joy-lunar-phobia/))

