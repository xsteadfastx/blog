Title: Azeda Booth - Tubtrek
Date: 2013-01-09 11:11
Author: marvin
Category: Uncategorized
Tags: 23seconds, azedabooth, creativecommons, music, netlabels
Slug: azeda-booth-tubtrek

[![1.
![1.-cover]({static}/images/1.-cover.jpg)

Hier mal ein ganz tolles Release des [23 Seconds
Netlabels](http://www.23seconds.org/031.htm). Azeda Booth hat dort ihre
letzten Aufnahmen rausgebracht. Und das unter der Creative Commons
Lizenz. Die Tracks sind super. Irgendwas zwischen Glitch, Electronic und
Shoegaze. Das Album kann man [hier](http://www.23seconds.org/031.htm)
runterladen... wegträumen und so.

{% youtube kd7CjlTJNDE %}

{% youtube QOPyN8VFGbs %}

