Title: All the pie and coffee
Date: 2013-07-09 10:48
Author: marvin
Category: Uncategorized
Tags: davidlynch, tv, twinpeaks
Slug: all-the-pie-and-coffee

[![c by Slacktory]({static}/images/Twin-Peaks-coffee-sip-graph-585x600.jpg)](http://slacktory.com/2013/07/twin-peaks-all-the-pie-and-coffee/)

Ich würde es fast die Mutter aller Supercuts nennen. Oder den Supercut
auf den ich seit der Supercut-Welle gewartet habe. Eine Zusammenschnitt
aller Kaffee-, und Kuchenszenen. Danke [Bryan
Menegus](http://gifthorsedentistry.tumblr.com/).

Als kleine Zugabe gibt es eine Infografik von Slacktory über den
Kaffeekonsum in der Serie.

{% youtube 12QQV3lyYj0 %}

([via](http://slacktory.com/2013/07/twin-peaks-all-the-pie-and-coffee/))

